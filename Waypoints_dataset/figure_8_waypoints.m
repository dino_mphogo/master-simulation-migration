% Lets try figure 8
fig_8_origin = [426 37];
x_start = fig_8_origin(1,1);
y_start = fig_8_origin(1,2);
diagonal_length = 300;
sides_length = 150;
figure_eight_points = [x_start y_start constant_altitude; x_start+diagonal_length*cos((30*pi)/180) y_start+sides_length constant_altitude; x_start+diagonal_length*cos((30*pi)/180) y_start constant_altitude; x_start y_start+sides_length constant_altitude; x_start y_start constant_altitude];
%current_way = [x_start y_start];
%next_way = [x_start+side_length y_start+diagonal_length*sin(acos(side_length/diagonal_length))];
%dist = sqrt((current_way(1,1)-next_way(1,1))^2 + (current_way(1,2)-next_way(1,2))^2);
csvwrite('figure_8_waypoints.csv',figure_eight_points);
figure
grid on
hold on
plot3(x_start,y_start, constant_altitude,'kX');
plot3(figure_eight_points(size(figure_eight_points,1),1),figure_eight_points(size(figure_eight_points,1),2),constant_altitude, 'kO');
plot3(figure_eight_points(:,1),figure_eight_points(:,2),figure_eight_points(:,3),'k-');
xlabel('North Direction');
ylabel('East Direction');
hold off
legend('UAV origin','UAV Destination','UAV pathway');
hold off	
