function [uav_6_waypoints_, uav_launch_angle] = uav_6_waypoints()
start_point = [43 594 5500];
x_0 = start_point(1);
y_0 = start_point(2);
z_0 = start_point(3);
waypoint_0 = [x_0 y_0 z_0];
angles = [(15/180)*pi (30/180)*pi (45/180)*pi];
rand_angle = uint64(1 + (3-1).*rand(1,1)); % rnadom angle index
uav_launch_angle = angles(rand_angle); % picks the random waypoint angle
x_delt = 494.54; % distance away from x_0 the x axis origin
side_length = 9074;
x_1 = x_0+x_delt;
y_1 = y_0; % new altitude
z_1 = z_0+x_delt*tan(uav_launch_angle);
waypoint_1 = [x_1 y_1 z_1];

x_2 = x_1+side_length;
waypoint_2 = [x_2 y_1 z_1];

x_3 = x_2+x_delt;
z_3 = z_0;
y_3 = y_0;
waypoint_3 = [x_3 y_3 z_3];
uav_6_waypoints_ = [waypoint_3; waypoint_2; waypoint_1;waypoint_0];
%uav_6_waypoints_ = [waypoint_0; waypoint_1; waypoint_2; waypoint_3];
