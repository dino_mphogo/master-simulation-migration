function [] = plot_uav_data(uav_id,uav_waypoints,uav_path_position_data)
figure
subplot(2,2,1);
grid on
hold on
plot3(uav_waypoints(:,1),uav_waypoints(:,2),uav_waypoints(:,3),'ro');
text(uav_waypoints(:,1),uav_waypoints(:,2),uav_waypoints(:,3),'\leftarrow  UAV waypoint');

plot3(uav_path_position_data.data(:,1),uav_path_position_data.data(:,2),uav_path_position_data.data(:,3),'r-');
xlabel('North axis(x)');
ylabel('East axis(y)');
zlabel('Down axis(z)');
legend('UAV 1 waypoints','UAV 1 pathway');
hold off


subplot(2,2,2);
grid on
hold on
plot(1:size(uav_path_position_data.data(:,1),1),uav_path_position_data.data(:,1),'r-');
v_x = uav_path_position_data.data(2:1:size(uav_path_position_data.data(:,1),1),1) - uav_path_position_data.data(1:1:size(uav_path_position_data.data(:,1),1)-1,1);
plot(1:size(v_x,1),v_x,'b-');
ylabel('X axis command response');
xlabel('simulation time step');
legend('X-axis position command response','X-axis velocity command response');
hold off

subplot(2,2,3);
grid on
hold on
plot(1:size(uav_path_position_data.data(:,2),2),uav_path_position_data.data(:,2),'r-');
v_y = uav_path_position_data.data(2:1:size(uav_path_position_data.data(:,2),1),2) - uav_path_position_data.data(1:1:size(uav_path_position_data.data(:,2),1)-1,2);
plot(1:size(v_y,1),v_y,'b-');
ylabel('Y axis command response');
xlabel('simulation time step');
legend('Y-axis position command response','Y-axis velocity command response');
hold off

subplot(2,2,4);
grid on
hold on
plot(1:size(uav_path_position_data.data(:,3),1),uav_path_position_data.data(:,3),'r-');
v_z = uav_path_position_data.data(2:1:size(uav_path_position_data.data(:,3),1),3) - uav_path_position_data.data(1:1:size(uav_path_position_data.data(:,3),1)-1,3);
plot(1:size(v_z,1),v_z,'b-');
ylabel('Z axis command response');
xlabel('simulation time step');
legend('Z-axis position command response','Z-axis velocity command response');
hold off 
