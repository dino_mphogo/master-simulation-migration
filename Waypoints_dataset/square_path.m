% lets start with a square
constant_altitude = 4304;
start_corner = [64 83];
y_start = start_corner(1,1);
x_start = start_corner(1,2);
side_length = 200;
square_points = [];

for x = x_start:side_length:x_start+side_length
	square_points(size(square_points,1)+1,:) = [x y_start constant_altitude];
end
x_last = square_points(size(square_points,1),1);
for y = y_start:side_length:side_length+y_start
	square_points(size(square_points,1)+1,:) = [x_last y constant_altitude];
end
y_last = square_points(size(square_points,1),2);
for x = x_last:-side_length:x_last-side_length 
	square_points(size(square_points,1)+1,:) = [x y_last constant_altitude];
end
x_last = square_points(size(square_points,1),1);
for y = y_last:-side_length:y_last-side_length
	square_points(size(square_points,1)+1,:) = [x_last y constant_altitude];
end
csvwrite('square_waypoints.csv',square_points);
figure
grid on
hold on
plot3(x_start,y_start, constant_altitude,'bX');
plot3(square_points(size(square_points,1),1),square_points(size(square_points,1),2),constant_altitude, 'bO');
plot3(square_points(:,1),square_points(:,2),square_points(:,3),'b-');
xlabel('North Direction');
ylabel('East Direction');
hold off
legend('UAV origin','UAV Destination','UAV pathway');
hold off
