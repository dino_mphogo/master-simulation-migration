uav_1_waypoints = csvread('uav_1_waypoints.csv');
uav_2_waypoints = csvread('uav_2_waypoints.csv');
uav_3_waypoints = csvread('uav_3_waypoints.csv');
uav_4_waypoints = csvread('uav_4_waypoints.csv');
uav_5_waypoints = csvread('uav_5_waypoints.csv');
uav_6_waypoints = csvread('uav_6_waypoints.csv');
uav_7_waypoints = csvread('uav_6_waypoints.csv');
uav_8_waypoints = csvread('uav_6_waypoints.csv');
uav_9_waypoints = csvread('uav_6_waypoints.csv');
uav_10_waypoints = csvread('uav_6_waypoints.csv');
load uav_1_pathway.mat
load uav_2_pathway.mat
load uav_3_pathway.mat
load uav_4_pathway.mat
load uav_5_pathway.mat
load uav_6_pathway.mat
load uav_7_pathway.mat
load uav_8_pathway.mat
load uav_9_pathway.mat
load uav_10_pathway.mat
%plot_uav_data(1,uav_1_waypoints,uav_1_pathway_data);
figure
hold on
plot3(uav_1_waypoints(:,1),uav_1_waypoints(:,2),uav_1_waypoints(:,3),'ro', uav_2_waypoints(:,1),uav_2_waypoints(:,2),uav_2_waypoints(:,3), 'go',uav_3_waypoints(:,1),uav_3_waypoints(:,2),uav_3_waypoints(:,3), 'bo', uav_4_waypoints(:,1),uav_4_waypoints(:,2),uav_4_waypoints(:,3), 'ko',uav_5_waypoints(:,1),uav_5_waypoints(:,2),uav_5_waypoints(:,3), 'co',uav_6_waypoints(:,1),uav_6_waypoints(:,2),uav_6_waypoints(:,3), 'mo',uav_7_waypoints(:,1),uav_7_waypoints(:,2),uav_7_waypoints(:,3), 'co',uav_8_waypoints(:,1),uav_8_waypoints(:,2),uav_8_waypoints(:,3), 'yo',uav_9_waypoints(:,1),uav_9_waypoints(:,2),uav_9_waypoints(:,3), 'mx',uav_10_waypoints(:,1),uav_10_waypoints(:,2),uav_10_waypoints(:,3), 'rx');

text(uav_1_waypoints(1,1),uav_1_waypoints(1,2),uav_1_waypoints(1,3),[' \leftarrow endpoint waypoint Multi-UAV [x,y,z] = ',mat2str(uav_1_waypoints(1,1:3))]);
text(uav_1_waypoints(end,1),uav_1_waypoints(end,2),uav_1_waypoints(end,3),[' \leftarrow Endpoint waypoint Multi-UAV [x,y,z] = ',mat2str(uav_1_waypoints(end,1:3))]);


grid on
plot3(uav_1_pathway_data.data(:,1),uav_1_pathway_data.data(:,2),uav_1_pathway_data.data(:,3), 'r-', uav_2_pathway_data.data(:,1),uav_2_pathway_data.data(:,2),uav_2_pathway_data.data(:,3),'g-',uav_3_pathway_data.data(:,1),uav_3_pathway_data.data(:,2),uav_3_pathway_data.data(:,3),'b-',uav_4_pathway_data.data(:,1),uav_4_pathway_data.data(:,2),uav_4_pathway_data.data(:,3),'k-',uav_5_pathway_data.data(:,1),uav_5_pathway_data.data(:,2),uav_5_pathway_data.data(:,3),'c-',uav_6_pathway_data.data(:,1),uav_6_pathway_data.data(:,2),uav_6_pathway_data.data(:,3),'k.',uav_7_pathway_data.data(:,1),uav_7_pathway_data.data(:,2),uav_7_pathway_data.data(:,3),'c.',uav_8_pathway_data.data(:,1),uav_8_pathway_data.data(:,2),uav_8_pathway_data.data(:,3),'b.',uav_9_pathway_data.data(:,1),uav_9_pathway_data.data(:,2),uav_9_pathway_data.data(:,3),'g.',uav_10_pathway_data.data(:,1),uav_10_pathway_data.data(:,2),uav_10_pathway_data.data(:,3),'y-');

hold off
xlabel('North');
ylabel('East');
zlabel('Down')
legend('UAV 1 waypoints', 'UAV 2 waypoints', 'UAV 3 waypoints', 'UAV 4 waypoints', 'UAV 5 waypoints', 'UAV 6 waypoints',  'UAV 7 waypoints',  'UAV 8 waypoints',  'UAV 9 waypoints',  'UAV 10 waypoints' ,'UAV 1 positions(NED)', 'UAV 2 position(NED)', 'UAV 3 position(NED)','UAV 4 position(NED)', 'UAV 5 position(NED)','UAV 6 position(NED)','UAV 7 position(NED)','UAV 8 position(NED)','UAV 9 position(NED)','UAV 10 position(NED)');
