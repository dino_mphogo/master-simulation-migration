 % lets plot a circle waypoints

center = [200 -100];
a = center(1,1);
b = center(1,2);
radius = 200;
epsilon = 0.035;
circle_points = [];
circle_points(size(circle_points,1)+1,:) = [a-radius b constant_altitude];

for x = a-radius:10:a
	for y = b:-10:b-radius
		if sqrt((x-a)^2 + (y-b)^2) > radius-epsilon && sqrt((x-a)^2 + (y-b)^2) < radius+epsilon
			circle_points(size(circle_points,1)+1,:) = [x y constant_altitude];
		end  
	end
end
for x = a:10:a+radius
	for y = b-radius:10:b
		if sqrt((x-a)^2 + (y-b)^2) > radius-epsilon && sqrt((x-a)^2 + (y-b)^2) < radius+epsilon 
			circle_points(size(circle_points,1)+1,:) = [x y constant_altitude];
		end  
	end
end
for x = a+radius:-10:a
	for y = b:10:b+radius
		if sqrt((x-a)^2 + (y-b)^2) > radius-epsilon && sqrt((x-a)^2 + (y-b)^2) < radius+epsilon 
			circle_points(size(circle_points,1)+1,:) = [x y constant_altitude];
		end  
	end
end
for x = a:-10:a-radius
	for y = b+radius:-10:b
		if sqrt((x-a)^2 + (y-b)^2) > radius-epsilon && sqrt((x-a)^2 + (y-b)^2) < radius+epsilon 
			circle_points(size(circle_points,1)+1,:) = [x y constant_altitude];
		end  
	end
end

csvwrite('circle_waypoints.csv',circle_points);
figure
grid on
hold on
xlabel('North Direction');
ylabel('East Direction');
plot3(a,b,constant_altitude,'rO');
plot3(circle_points(size(circle_points,1),1),circle_points(size(circle_points,1),2),constant_altitude,'rX');
plot3(circle_points(:,1),circle_points(:,2), circle_points(:,3),'r-');
legend('UAV origin','UAV Destination','UAV pathway');
hold off
