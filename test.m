Radius = 10;
AngularVelocity = 5; % in deg / s
AngleStep = 0.1
Angles = AngleStep : AngleStep : 2*pi;
CircleX = [Radius]; % empty array
CircleY = [0]; % empty array

%Initial zero-angle plot whose data we'll keep updating in the for loop:
a = plot([CircleX,CircleX], [CircleY,CircleY], 'r:');
hold on;
b = plot(CircleX, CircleY, 'o', 'markeredgecolor', 'k', 'markerfacecolor','g');
axis([-Radius, +Radius, -Radius, +Radius]); % make sure the axis is fixed
axis equal; % make x and y pixels of equal size so it "looks" a circle!
hold off;

for t = Angles
  CircleX(end+1) = Radius * cos (t); % append point at end of CircleX array
  CircleY(end+1) = Radius * sin (t); % append point at end of Circley array
  set(a,'xdata',CircleX,'ydata',CircleY); % update plot 'a' data
  set(b,'xdata', CircleX(end), 'ydata', CircleY(end)); % update plot 'b' data
  drawnow; % ensure intermediate frames are shown!
  pause(AngleStep/AngularVelocity) % pause the right amount of time!
end
