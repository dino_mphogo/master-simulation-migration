% prepare environment 



 
% insert enemy aircraft 



[x,y] = meshgrid(0:1:800,0:1:800);
sea_surf = peaks(800) + repmat(peaks(400),2,2) + repmat(peaks(200),4,4);
sea_surf(:,801) = 0;
sea_surf(801,:) = 0;
sea_surf = 0.7*sea_surf; 


data = zeros(100,6);
data(:,2) = linspace(100,100);
data(:,3) = linspace(250,250);
 new_object('aircraft_1.mat',data,... 
'model','f-16.mat','scale',4,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1); 
 

data = zeros(100,6);
data(:,2) = linspace(100,100);
data(:,3) = linspace(350,350);
 new_object('aircraft_2.mat',data,... 
'model','f-16.mat','scale',4,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1); 


data = zeros(100,6);
data(:,2) = linspace(400,400);
data(:,3) = linspace(200,200);
data(:,5) = -135;
 new_object('aircraft_3.mat',data,... 
'model','f-16.mat','scale',4,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1); 

data = zeros(100,6);
data(:,2) = linspace(400,400);
data(:,3) = linspace(300,300);
data(:,5) = -135;
 new_object('aircraft_4.mat',data,... 
'model','f-16.mat','scale',4,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1); 


data = zeros(100,6);
data(:,2) = linspace(400,400);
data(:,3) = linspace(400,400);
data(:,5) = -135;
new_object('aircraft_5.mat',data,... 
'model','f-16.mat','scale',4,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1); 

% generate the scene and save the result as animated gif file 
% generate the scene
flypath('aircraft_1.mat','aircraft_2.mat','aircraft_3.mat', 'aircraft_4.mat','aircraft_5.mat',...
'animate','off','output','none','step',15,...
'font','Georgia','fontsize',6,...
'view',[90 0],'xlim',[0 400],'ylim',[0 600],'zlim',[0 500]);


surf(x,y,sea_surf,...
'FaceColor',[.2 .4 .9],'EdgeColor',[.9 .9 .9],'FaceAlpha',1,'EdgeAlpha',.2);

% fireball radius for aircraft 1
[x,y,z] = sphere(100);
surf(100*x,100+50*y,250+50*z,...
'EdgeColor','none','FaceColor',[.89 .0 .27],'FaceAlpha',.4);


% fireball radius for aircraft 2
[x,y,z] = sphere(100);
surf(100*x,100+50*y,350+50*z,...
'EdgeColor','none','FaceColor',[.89 .0 .27],'FaceAlpha',.4);


% fireball radius for aircraft 3
[x,y,z] = sphere(100);
surf(100*x,400+50*y,200+50*z,...
'EdgeColor','none','FaceColor',[.89 .0 .27],'FaceAlpha',.4);

% fireball radius for aircraft 4
[x,y,z] = sphere(100);
surf(100*x,400+50*y,300+50*z,...
'EdgeColor','none','FaceColor',[.89 .0 .27],'FaceAlpha',.4);

% fireball radius for aircraft 5
[x,y,z] = sphere(100);
surf(100*x,400+50*y,400+50*z,...
'EdgeColor','none','FaceColor',[.89 .0 .27],'FaceAlpha',.4);



text(0,150,210,'Aircraft 1','FontName','Georgia','FontSize',6);
text(0,150,380,'Aircraft 2','FontName','Georgia','FontSize',6);
text(0,450,210,'Aircraft 3','FontName','Georgia','FontSize',6);
text(0,450,340,'Aircraft 4','FontName','Georgia','FontSize',6);
text(0,450,450,'Aircraft 5','FontName','Georgia','FontSize',6);
% save the result as png file
filename = 'aircraft_conflict_multiple.png';
eval(sprintf('print -dpng -r600 %s;',filename));
