load uav_1_position_data.mat
load uav_2_position_data.mat
load uav_3_position_data.mat
load uav_1_flight_data.mat
load uav_2_flight_data.mat
load uav_3_flight_data.mat

fig_1 = figure;
title('Altitude Rate vs simulation time');
plot(1:size(uav_1_flight_data.data(:,3),1),uav_1_flight_data.data(:,3),'r-')
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 1 Altitude Inputs');
saveas(fig_1,['uav_1_altitude_input.jpg']);
hold off

fig_1 = figure ;
title('Altitude Rate vs simulation time');
plot(1:size(uav_2_flight_data.data(:,3),1),uav_2_flight_data.data(:,3),'b-')
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
saveas(fig_1,['uav_2_altitude_input.jpg']);
legend('UAV 2 Altitude Inpts');

fig_1 = figure ;
title('Altitude Rate vs simulation time');
plot(1:size(uav_3_flight_data.data(:,3),1),uav_3_flight_data.data(:,3),'b-')
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
saveas(fig_1,['uav_3_altitude_input.jpg']);
legend('UAV 3 Altitude Inpts');


