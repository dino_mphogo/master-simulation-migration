function cost_function_j = input_variation_cost_function(tree)
    disp('here')
    node = tree(end); % one of the leafs
    tau_val = node.time_to_conflict;
    min_cost = node.cost_function_J;
    node_height = node.x_k(3);
        for i = size(tree,2):-1:1
         if(tree(i).time_to_conflict == tau_val)
             if(tree(i).x_k(3) == tree(1).x_k(3))
                 node = tree(i);
                 min_cost = node.cost_function_J;
             	 break;
             else
             	if(abs(tree(1).x_k(3) - node.x_k(3)) > abs(tree(1).x_k(3) - tree(i).x_k(3)))
             		node = tree(i);
             	
             	end
             	
             end
         else
         	break;
         end
         
     end
    
    path_nodes = node;
    cost_function_j = [];
    while(node.parent ~= 1)
    	if node.u_k_1 ~= tree(node.parent).u_k_1
    		if(abs(node.u_k_1) == 25)
    			cost_function_j(size(cost_function_j,2)+1) = 1;
    		else
    			
    			cost_function_j(size(cost_function_j,2)+1) = 3;
    		end
    	else
    		cost_function_j(size(cost_function_j,2)+1) = 0;
    	end
        node = tree(node.parent);
        path_nodes(size(path_nodes,2)+1) = node;
    end
    node = tree(node.parent);
    if node.u_k_1 ~= tree(node.parent).u_k_1
    		if(abs(node.u_k_1) == 25)
    			cost_function_j(size(cost_function_j,2)+1) = 1;
    		else
    			
    			cost_function_j(size(cost_function_j,2)+1) = 3;
    		end
    	else
    		cost_function_j(size(cost_function_j,2)+1) = 0;
    	end
end
