function hdot = get_hdot(href,h, threshold_h)
	hmax = 50; % ft/sec
	k = 0.5;
	if(abs(href - h) < threshold_h)
		hdot = k*(href-h);
	else
		if(href > h)
			hdot = hmax;
		else
			hdot = -hmax;
		end
	end

end
