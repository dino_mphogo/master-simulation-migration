function conflict_state = conflict_detection_tree(host_id, host_position,intruders_position, x_dots, T)
x_dmod_ta  = 3076.12*0.5;
x_dmod_ra  = 6076.12*0.35;
x_dot_max = 27.4910/0.3048;
%z_thr = 850*0.3048;
z_thr_ta = 650;
conflict_state = 0;

for(l = 1:size(intruders_position,1))
	if(l ~= host_id)
		if(size(intruders_position,2) > 1)
			z_delt = host_position(3) - intruders_position(l,3);
			x_delt = host_position(1) - intruders_position(l,1);
			x_dot_relative = x_dots(host_id) - x_dots(l);
			x_threshold_scaled = abs(x_dots(host_id)/x_dots(l))*x_dmod_ta;
			tau_new = x_delt/x_dot_relative; 
			% abs(tau_new) <= max([T(l) T(host_id)])
			% abs(tau_new) <= 3*T(host_id))
			% abs(tau_new) <= T(host_id))
			if(abs(z_delt) <= z_thr_ta && abs(x_delt) <= x_dmod_ta)
    				conflict_state = 1;
   	        		break;
			end
		end
	end	

	
end

end
