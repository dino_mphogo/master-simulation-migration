function path_inputs  = construct_path(planning_tree, leaf_node)
        path_inputs = [];
        path_inputs(size(path_inputs,2)+1) = leaf_node.u_k_1;
        while(leaf_node.parent ~= 1)
            leaf_node = planning_tree(leaf_node.parent);    
            path_inputs(size(path_inputs,2)+1) = leaf_node.u_k_1;
        end
       
end
