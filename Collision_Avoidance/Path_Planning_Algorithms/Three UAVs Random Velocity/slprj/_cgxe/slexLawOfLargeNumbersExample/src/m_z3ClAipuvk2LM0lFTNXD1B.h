#ifndef __z3ClAipuvk2LM0lFTNXD1B_h__
#define __z3ClAipuvk2LM0lFTNXD1B_h__

/* Include files */
#include "simstruc.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_slE07I4lDg6FknjQ3k8Q9CG
#define struct_slE07I4lDg6FknjQ3k8Q9CG

struct slE07I4lDg6FknjQ3k8Q9CG
{
  real_T Index;
  real_T DataType;
  real_T IsSigned;
  real_T MantBits;
  real_T FixExp;
  real_T Slope;
  real_T Bias;
};

#endif                                 /*struct_slE07I4lDg6FknjQ3k8Q9CG*/

#ifndef typedef_slE07I4lDg6FknjQ3k8Q9CG
#define typedef_slE07I4lDg6FknjQ3k8Q9CG

typedef struct slE07I4lDg6FknjQ3k8Q9CG slE07I4lDg6FknjQ3k8Q9CG;

#endif                                 /*typedef_slE07I4lDg6FknjQ3k8Q9CG*/

#ifndef struct_sJ2iBbgIVXkrCjsQ0QDLa3
#define struct_sJ2iBbgIVXkrCjsQ0QDLa3

struct sJ2iBbgIVXkrCjsQ0QDLa3
{
  int32_T isInitialized;
  real_T counter;
  real_T consecCounter;
};

#endif                                 /*struct_sJ2iBbgIVXkrCjsQ0QDLa3*/

#ifndef typedef_slexLawOfLargeNumbersIntegerCounterSysObj
#define typedef_slexLawOfLargeNumbersIntegerCounterSysObj

typedef struct sJ2iBbgIVXkrCjsQ0QDLa3 slexLawOfLargeNumbersIntegerCounterSysObj;

#endif                                 /*typedef_slexLawOfLargeNumbersIntegerCounterSysObj*/

#ifndef struct_sU0K4wfUayAPGdDTNXr2bGE
#define struct_sU0K4wfUayAPGdDTNXr2bGE

struct sU0K4wfUayAPGdDTNXr2bGE
{
  real_T dimModes;
  real_T dims[4];
  real_T dType;
  real_T complexity;
  real_T outputBuiltInDTEqUsed;
};

#endif                                 /*struct_sU0K4wfUayAPGdDTNXr2bGE*/

#ifndef typedef_sfOd2wElE6un66xmZCZog7F
#define typedef_sfOd2wElE6un66xmZCZog7F

typedef struct sU0K4wfUayAPGdDTNXr2bGE sfOd2wElE6un66xmZCZog7F;

#endif                                 /*typedef_sfOd2wElE6un66xmZCZog7F*/

#ifndef struct_soelt8LyRRsHUTQeZErRIHC
#define struct_soelt8LyRRsHUTQeZErRIHC

struct soelt8LyRRsHUTQeZErRIHC
{
  char_T names[13];
  real_T dims[4];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_soelt8LyRRsHUTQeZErRIHC*/

#ifndef typedef_sZVQz5WVraeIWEljxFvLe8
#define typedef_sZVQz5WVraeIWEljxFvLe8

typedef struct soelt8LyRRsHUTQeZErRIHC sZVQz5WVraeIWEljxFvLe8;

#endif                                 /*typedef_sZVQz5WVraeIWEljxFvLe8*/

#ifndef struct_sU0K4wfUayAPGdDTNXr2bGE_size
#define struct_sU0K4wfUayAPGdDTNXr2bGE_size

struct sU0K4wfUayAPGdDTNXr2bGE_size
{
  int32_T dims[2];
};

#endif                                 /*struct_sU0K4wfUayAPGdDTNXr2bGE_size*/

#ifndef typedef_sfOd2wElE6un66xmZCZog7F_size
#define typedef_sfOd2wElE6un66xmZCZog7F_size

typedef struct sU0K4wfUayAPGdDTNXr2bGE_size sfOd2wElE6un66xmZCZog7F_size;

#endif                                 /*typedef_sfOd2wElE6un66xmZCZog7F_size*/

#ifndef struct_soelt8LyRRsHUTQeZErRIHC_size
#define struct_soelt8LyRRsHUTQeZErRIHC_size

struct soelt8LyRRsHUTQeZErRIHC_size
{
  int32_T names[2];
  int32_T dims[2];
};

#endif                                 /*struct_soelt8LyRRsHUTQeZErRIHC_size*/

#ifndef typedef_sZVQz5WVraeIWEljxFvLe8_size
#define typedef_sZVQz5WVraeIWEljxFvLe8_size

typedef struct soelt8LyRRsHUTQeZErRIHC_size sZVQz5WVraeIWEljxFvLe8_size;

#endif                                 /*typedef_sZVQz5WVraeIWEljxFvLe8_size*/

#ifndef struct_sfOd2wElE6un66xmZCZog7F
#define struct_sfOd2wElE6un66xmZCZog7F

struct sfOd2wElE6un66xmZCZog7F
{
  real_T dimModes;
  real_T dims[3];
  real_T dType;
  real_T complexity;
  real_T outputBuiltInDTEqUsed;
};

#endif                                 /*struct_sfOd2wElE6un66xmZCZog7F*/

#ifndef typedef_b_sfOd2wElE6un66xmZCZog7F
#define typedef_b_sfOd2wElE6un66xmZCZog7F

typedef struct sfOd2wElE6un66xmZCZog7F b_sfOd2wElE6un66xmZCZog7F;

#endif                                 /*typedef_b_sfOd2wElE6un66xmZCZog7F*/

#ifndef struct_sRzepbcAzWdfpiYnMYop13F
#define struct_sRzepbcAzWdfpiYnMYop13F

struct sRzepbcAzWdfpiYnMYop13F
{
  real_T dimModes;
  real_T dims[4];
  real_T dType;
  real_T complexity;
  real_T outputBuiltInDTEqUsed;
};

#endif                                 /*struct_sRzepbcAzWdfpiYnMYop13F*/

#ifndef typedef_sRzepbcAzWdfpiYnMYop13F
#define typedef_sRzepbcAzWdfpiYnMYop13F

typedef struct sRzepbcAzWdfpiYnMYop13F sRzepbcAzWdfpiYnMYop13F;

#endif                                 /*typedef_sRzepbcAzWdfpiYnMYop13F*/

#ifndef struct_sZVQz5WVraeIWEljxFvLe8
#define struct_sZVQz5WVraeIWEljxFvLe8

struct sZVQz5WVraeIWEljxFvLe8
{
  char_T names;
  real_T dims[3];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_sZVQz5WVraeIWEljxFvLe8*/

#ifndef typedef_b_sZVQz5WVraeIWEljxFvLe8
#define typedef_b_sZVQz5WVraeIWEljxFvLe8

typedef struct sZVQz5WVraeIWEljxFvLe8 b_sZVQz5WVraeIWEljxFvLe8;

#endif                                 /*typedef_b_sZVQz5WVraeIWEljxFvLe8*/

#ifndef struct_sWfXrrLeGNvp4Lc5rCVBsbH
#define struct_sWfXrrLeGNvp4Lc5rCVBsbH

struct sWfXrrLeGNvp4Lc5rCVBsbH
{
  char_T names[7];
  real_T dims[4];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_sWfXrrLeGNvp4Lc5rCVBsbH*/

#ifndef typedef_sWfXrrLeGNvp4Lc5rCVBsbH
#define typedef_sWfXrrLeGNvp4Lc5rCVBsbH

typedef struct sWfXrrLeGNvp4Lc5rCVBsbH sWfXrrLeGNvp4Lc5rCVBsbH;

#endif                                 /*typedef_sWfXrrLeGNvp4Lc5rCVBsbH*/

#ifndef struct_sc3VTylUkbr6pYiOsCVYrxB
#define struct_sc3VTylUkbr6pYiOsCVYrxB

struct sc3VTylUkbr6pYiOsCVYrxB
{
  char_T names[13];
  real_T dims[4];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_sc3VTylUkbr6pYiOsCVYrxB*/

#ifndef typedef_sc3VTylUkbr6pYiOsCVYrxB
#define typedef_sc3VTylUkbr6pYiOsCVYrxB

typedef struct sc3VTylUkbr6pYiOsCVYrxB sc3VTylUkbr6pYiOsCVYrxB;

#endif                                 /*typedef_sc3VTylUkbr6pYiOsCVYrxB*/

#ifndef struct_sIvmHumfM4VG8K4LjAjoqqB
#define struct_sIvmHumfM4VG8K4LjAjoqqB

struct sIvmHumfM4VG8K4LjAjoqqB
{
  char_T names;
  real_T dims[3];
  real_T dType;
  real_T dTypeSize;
  char_T dTypeName;
  real_T dTypeIndex;
  char_T dTypeChksum;
  real_T complexity;
};

#endif                                 /*struct_sIvmHumfM4VG8K4LjAjoqqB*/

#ifndef typedef_sIvmHumfM4VG8K4LjAjoqqB
#define typedef_sIvmHumfM4VG8K4LjAjoqqB

typedef struct sIvmHumfM4VG8K4LjAjoqqB sIvmHumfM4VG8K4LjAjoqqB;

#endif                                 /*typedef_sIvmHumfM4VG8K4LjAjoqqB*/

#ifndef struct_s7UBIGHSehQY1gCsIQWwr5C
#define struct_s7UBIGHSehQY1gCsIQWwr5C

struct s7UBIGHSehQY1gCsIQWwr5C
{
  real_T chksum[4];
};

#endif                                 /*struct_s7UBIGHSehQY1gCsIQWwr5C*/

#ifndef typedef_s7UBIGHSehQY1gCsIQWwr5C
#define typedef_s7UBIGHSehQY1gCsIQWwr5C

typedef struct s7UBIGHSehQY1gCsIQWwr5C s7UBIGHSehQY1gCsIQWwr5C;

#endif                                 /*typedef_s7UBIGHSehQY1gCsIQWwr5C*/

#ifndef struct_sznE3FlCyo7TBPrpitCkVZC
#define struct_sznE3FlCyo7TBPrpitCkVZC

struct sznE3FlCyo7TBPrpitCkVZC
{
  s7UBIGHSehQY1gCsIQWwr5C dlgPrmChksum;
  s7UBIGHSehQY1gCsIQWwr5C propChksum[3];
  s7UBIGHSehQY1gCsIQWwr5C CGOnlyParamChksum;
  s7UBIGHSehQY1gCsIQWwr5C postPropOnlyChksum;
};

#endif                                 /*struct_sznE3FlCyo7TBPrpitCkVZC*/

#ifndef typedef_sznE3FlCyo7TBPrpitCkVZC
#define typedef_sznE3FlCyo7TBPrpitCkVZC

typedef struct sznE3FlCyo7TBPrpitCkVZC sznE3FlCyo7TBPrpitCkVZC;

#endif                                 /*typedef_sznE3FlCyo7TBPrpitCkVZC*/

#ifndef typedef_InstanceStruct_z3ClAipuvk2LM0lFTNXD1B
#define typedef_InstanceStruct_z3ClAipuvk2LM0lFTNXD1B

typedef struct {
  SimStruct *S;
  covrtInstance covInst;
  slexLawOfLargeNumbersIntegerCounterSysObj sysobj;
  boolean_T sysobj_not_empty;
} InstanceStruct_z3ClAipuvk2LM0lFTNXD1B;

#endif                                 /*typedef_InstanceStruct_z3ClAipuvk2LM0lFTNXD1B*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
extern void method_dispatcher_z3ClAipuvk2LM0lFTNXD1B(SimStruct *S, int_T method,
  void* data);
extern int autoInfer_dispatcher_z3ClAipuvk2LM0lFTNXD1B(mxArray *lhs[], const
  char* commandName);

#endif
