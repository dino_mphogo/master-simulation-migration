function minimum_range = get_range_min(h_x_k, all_uavs_pos, host_id)
	minimum_range = inf;
	for(l = 1:size(all_uavs_pos,1))
		if(l ~= host_id)
			range =  sum(power(all_uavs_pos(l,:) - h_x_k,2));
			if(range < minimum_range)
				minimum_range = range;
			end
		end
	end
end
