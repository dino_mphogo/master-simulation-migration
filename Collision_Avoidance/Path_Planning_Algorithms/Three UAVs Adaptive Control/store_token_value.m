function [] = store_token_value(in_token_value)
	global token_value
	if(ismember(in_token_value,token_value) ~= 1)
		token_value(size(token_value,2)+1) = in_token_value;
	end
	
end
