a = 15/0.3048;
b = 27.4910/0.3048;
velocity_mean = 0.5*(b+a);
velocity_varience = 1;
fig = figure;
x =  15/0.3048:0.1:27.4910/0.3048;
y = normpdf(x,velocity_mean,velocity_varience);

plot(x,y,'-g');
grid on
hold on

velocities =  round(X_dots, 3);
uav_1 = [abs(velocities(1)) 0; abs(velocities(1)) y(find(abs(x - abs(velocities(1))) < 0.1,1))];
uav_2 = [abs(velocities(2)) 0; abs(velocities(2)) y(find(abs(x - abs(velocities(2))) < 0.1,1))];
uav_3 = [abs(velocities(3)) 0; abs(velocities(3)) y(find(abs(x - abs(velocities(3))) < 0.1,1))];
%plot([velocity_mean velocity_mean], [max(y) 0], '-b')
plot([a a], [y(find(abs(x - a) < 0.1,1)) 0], '*b')
plot([b b], [y(find(abs(x - b) < 0.1,1)) 0], '*r')
%plot(uav_1(:,1),uav_1(:,2))
%plot(uav_2(:,1),uav_2(:,2))
%plot(uav_3(:,1),uav_3(:,2))
legend('Velocity Density Function','V_{min}','V_{max}')
xlim([a b])
% ','UAV 1 Velocity', 'UAV 2 Velocity', 'UAV 3 Velocity')
saveas(fig,['velocity_distribution','.png']);
hold off
