load 'uav_2_position_data.mat'
load 'uav_3_position_data.mat'
load 'uavs_simulated_time.mat'
x_ra = 3076.12*0.5;
uav_2 = uav_2_position_data.data;
uav_3 = uav_3_position_data.data;
sim_time = uavs_simulated_time.data;
y = linspace(x_ra ,x_ra, size(sim_time,1));
sim_time = sim_time - sim_time(1)
figure = fig;
plot(sim_time, abs(uav_2(:,1) - uav_3(:,1)))
hold on 
plot(sim_time, y, '-r')
grid on
xlabel('time(s)')
ylabel('|x_i - x_j|')
legend('|x_i - x_j|', 'Horizontal threshold x_{ra}')
hold off
