
for index = 1:size(simulated_positions,2)
    simulated = simulated_positions{index};
    uav_1 = [simulated{1}(1,:); simulated{2}(1,:); simulated{3}(1,:); simulated{4}(1,:); simulated{5}(1,:); simulated{6}(1,:)];
    uav_2 = [simulated{1}(2,:); simulated{2}(2,:); simulated{3}(2,:); simulated{4}(2,:); simulated{5}(2,:); simulated{6}(2,:)];
    uav_3 = [simulated{1}(3,:); simulated{2}(3,:); simulated{3}(3,:); simulated{4}(3,:); simulated{5}(3,:); simulated{6}(3,:)];
    figure
    
    [x,y,z] = sphere(100);
     hold on
     view(1,3)
     plot3(uav_1(:,1),uav_1(:,2),uav_1(:,3), 'r')
     scatter3(uav_1(:,1),uav_1(:,2),uav_1(:,3),'MarkerFaceColor',[1 0 0])
     plot3(uav_2(:,1), uav_2(:,2), uav_2(:,3), 'g')
      scatter3(uav_2(:,1),uav_2(:,2),uav_2(:,3),'MarkerFaceColor',[0 1 0])
     plot3(uav_3(:,1), uav_3(:,2), uav_3(:,3), 'b')
      scatter3(uav_3(:,1),uav_3(:,2),uav_3(:,3),'MarkerFaceColor',[0 0 1])
     grid on
     xlabel('North(feet)')
     zlabel('Altitude(feet)')
     legend('UAV 1 Path','UAV 1 Path Sample', 'UAV 2 Path','UAV 2 Path Sample', 'UAV 3 Path', 'UAV 3 Path Sample')
     title('Path Prediction')
      t = 0;
      for indx = 1:6
      	surf(uav_1(indx,1)+1076.12*0.5*x,uav_1(indx,2)+100.12*0.5*y,uav_1(indx,3)+650*z,... 
	'EdgeColor','none','FaceColor',[1 .0 0],'FaceAlpha',.2); 
	surf(uav_2(indx,1)+1076.12*0.5*x,uav_2(indx,2)+100.12*0.5*y,uav_2(indx,3)+650*z,... 
	'EdgeColor','none','FaceColor',[0 1 0],'FaceAlpha',.2); 
	surf(uav_3(indx,1)+1076.12*0.5*x,uav_3(indx,2)+100.12*0.5*y,uav_3(indx,3)+650*z,... 
	'EdgeColor','none','FaceColor',[0 0 1],'FaceAlpha',.2); 
	text(uav_1(indx,1),uav_1(indx,2),uav_1(indx,3)+100,['T = ',num2str(t)],'FontName','Georgia','FontSize',10);
	text(uav_2(indx,1),uav_2(indx,2),uav_2(indx,3)+100,['T = ',num2str(t)],'FontName','Georgia','FontSize',10);
	text(uav_3(indx,1),uav_3(indx,2),uav_3(indx,3)+100,['T = ',num2str(t)],'FontName','Georgia','FontSize',10);
	t = t + 1;
      end
	
	
end
