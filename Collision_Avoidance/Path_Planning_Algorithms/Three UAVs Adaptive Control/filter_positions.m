function [positions, x_dots] = filter_positions(intruder_positions, plans_tracks, x_dots_input, uav_id)
alg_altitude_threshold = 1000;
active_plans = find(plans_tracks > 0);
active_plans = active_plans(active_plans ~= uav_id);
below_agl = intruder_positions(find(intruder_positions(:,3) <= alg_altitude_threshold),:);
positions = below_agl;
x_dots = x_dots_input(active_plans);
if(size(active_plans,2) > 0)
	for (index = 1:size(active_plans,2))
		positions(size(positions,1)+1,:) = intruder_positions(active_plans(index),:);
	end
end

end
