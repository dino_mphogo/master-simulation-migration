/* Include files */

#include <stddef.h>
#include "blas.h"
#include "slexLawOfLargeNumbersExample_cgxe.h"
#include "m_z3ClAipuvk2LM0lFTNXD1B.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
static emlrtMCInfo emlrtMCI = { 1, 1, "SystemCore",
  "/usr/local/MATLAB/R2015a/toolbox/shared/system/coder/+matlab/+system/+coder/SystemCore.p"
};

/* Function Declarations */
static void cgxe_mdl_start(InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *moduleInstance);
static void cgxe_mdl_initialize(InstanceStruct_z3ClAipuvk2LM0lFTNXD1B
  *moduleInstance);
static void cgxe_mdl_outputs(InstanceStruct_z3ClAipuvk2LM0lFTNXD1B
  *moduleInstance);
static void cgxe_mdl_update(InstanceStruct_z3ClAipuvk2LM0lFTNXD1B
  *moduleInstance);
static void cgxe_mdl_terminate(InstanceStruct_z3ClAipuvk2LM0lFTNXD1B
  *moduleInstance);
static const mxArray *mw__internal__name__resolution__fcn(void);
static void info_helper(const mxArray **info);
static const mxArray *emlrt_marshallOut(const char * u);
static const mxArray *b_emlrt_marshallOut(const uint32_T u);
static const mxArray *mw__internal__autoInference__fcn(void);
static const mxArray *c_emlrt_marshallOut(const real_T u_data[], const int32_T
  u_sizes[2]);
static const mxArray *mw__internal__getSimState__fcn
  (InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *moduleInstance);
static real_T emlrt_marshallIn(const mxArray *b_consecCounter, const char_T
  *identifier);
static real_T b_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId);
static slexLawOfLargeNumbersIntegerCounterSysObj c_emlrt_marshallIn(const
  mxArray *b_sysobj, const char_T *identifier);
static slexLawOfLargeNumbersIntegerCounterSysObj d_emlrt_marshallIn(const
  mxArray *u, const emlrtMsgIdentifier *parentId);
static int32_T e_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId);
static boolean_T f_emlrt_marshallIn(const mxArray *b_sysobj_not_empty, const
  char_T *identifier);
static boolean_T g_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId);
static void mw__internal__setSimState__fcn(InstanceStruct_z3ClAipuvk2LM0lFTNXD1B
  *moduleInstance, const mxArray *st);
static const mxArray *message(const mxArray *b, const mxArray *c, emlrtMCInfo
  *location);
static void error(const mxArray *b, emlrtMCInfo *location);
static real_T h_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId);
static int32_T i_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId);
static boolean_T j_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId);

/* Function Definitions */
static void cgxe_mdl_start(InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *moduleInstance)
{
  slexLawOfLargeNumbersIntegerCounterSysObj *obj;
  int32_T i0;
  static char_T cv0[51] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'L', 'o', 'c', 'k', 'e', 'd', 'R', 'e', 'l', 'e',
    'a', 's', 'e', 'd', 'C', 'o', 'd', 'e', 'g', 'e', 'n' };

  char_T u[51];
  const mxArray *y;
  static const int32_T iv0[2] = { 1, 51 };

  const mxArray *m0;
  static char_T cv1[5] = { 's', 'e', 't', 'u', 'p' };

  char_T b_u[5];
  const mxArray *b_y;
  static const int32_T iv1[2] = { 1, 5 };

  real_T *counter;
  real_T *consecCounter;
  consecCounter = (real_T *)ssGetDWork(moduleInstance->S, 1U);
  counter = (real_T *)ssGetDWork(moduleInstance->S, 0U);

  /* Allocate instance data */
  covrtAllocateInstanceData(&moduleInstance->covInst);

  /* Initialize Coverage Information */
  covrtScriptInit(&moduleInstance->covInst,
                  "/usr/local/MATLAB/R2015a/toolbox/simulink/simdemos/simfeatures/slexLawOfLargeNumbersIntegerCounterSysObj.m",
                  0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0);

  /* Initialize Function Information */
  covrtFcnInit(&moduleInstance->covInst, 0, 0,
               "slexLawOfLargeNumbersIntegerCounterSysObj_slexLawOfLargeNumbersIntegerCounterSysObj",
               444, -1, 577);
  covrtFcnInit(&moduleInstance->covInst, 0, 1,
               "slexLawOfLargeNumbersIntegerCounterSysObj_stepImpl", 1005, -1,
               1222);

  /* Initialize Basic Block Information */
  covrtBasicBlockInit(&moduleInstance->covInst, 0, 0, 524, -1, 564);
  covrtBasicBlockInit(&moduleInstance->covInst, 0, 1, 1055, -1, 1209);

  /* Initialize If Information */
  /* Initialize MCDC Information */
  /* Initialize For Information */
  /* Initialize While Information */
  /* Initialize Switch Information */
  /* Start callback for coverage engine */
  covrtScriptStart(&moduleInstance->covInst, 0U);
  if (!moduleInstance->sysobj_not_empty) {
    covrtLogFcn(&moduleInstance->covInst, 0, 0);
    covrtLogBasicBlock(&moduleInstance->covInst, 0, 0);

    /*  Integer counter - it counts number of times called and time since last time enabled  */
    /*  Copyright 2013 The MathWorks Inc.  */
    /*  Shall count the number of times the system is called */
    /*  Shall count the number of times consecutively called */
    moduleInstance->sysobj.isInitialized = 0;
    moduleInstance->sysobj_not_empty = true;
  }

  obj = &moduleInstance->sysobj;
  if (moduleInstance->sysobj.isInitialized != 0) {
    for (i0 = 0; i0 < 51; i0++) {
      u[i0] = cv0[i0];
    }

    y = NULL;
    m0 = emlrtCreateCharArray(2, iv0);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 51, m0, &u[0]);
    emlrtAssign(&y, m0);
    for (i0 = 0; i0 < 5; i0++) {
      b_u[i0] = cv1[i0];
    }

    b_y = NULL;
    m0 = emlrtCreateCharArray(2, iv1);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 5, m0, &b_u[0]);
    emlrtAssign(&b_y, m0);
    error(message(y, b_y, &emlrtMCI), &emlrtMCI);
  }

  obj->isInitialized = 1;

  /*  The conescutiveCounter shall be reset whenever  */
  /*  the enabled subsystem is reset and the enabled */
  /*  subsystem has been designed to reset state when */
  /*  The conescutiveCounter shall be reset whenever  */
  /*  the enabled subsystem is reset and the enabled */
  /*  subsystem has been designed to reset state when */
  obj->counter = 0.0;
  obj->consecCounter = 0.0;
  *counter = moduleInstance->sysobj.counter;
  *consecCounter = moduleInstance->sysobj.consecCounter;
}

static void cgxe_mdl_initialize(InstanceStruct_z3ClAipuvk2LM0lFTNXD1B
  *moduleInstance)
{
  slexLawOfLargeNumbersIntegerCounterSysObj *obj;
  int32_T i1;
  static char_T cv2[45] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'R', 'e', 'l', 'e', 'a', 's', 'e', 'd', 'C', 'o',
    'd', 'e', 'g', 'e', 'n' };

  char_T u[45];
  const mxArray *y;
  static const int32_T iv2[2] = { 1, 45 };

  const mxArray *m1;
  static char_T cv3[8] = { 'i', 's', 'L', 'o', 'c', 'k', 'e', 'd' };

  char_T b_u[8];
  const mxArray *b_y;
  static const int32_T iv3[2] = { 1, 8 };

  boolean_T flag;
  char_T c_u[45];
  const mxArray *c_y;
  static const int32_T iv4[2] = { 1, 45 };

  static char_T cv4[5] = { 'r', 'e', 's', 'e', 't' };

  char_T d_u[5];
  const mxArray *d_y;
  static const int32_T iv5[2] = { 1, 5 };

  real_T *counter;
  real_T *consecCounter;
  consecCounter = (real_T *)ssGetDWork(moduleInstance->S, 1U);
  counter = (real_T *)ssGetDWork(moduleInstance->S, 0U);
  if (!moduleInstance->sysobj_not_empty) {
    covrtLogFcn(&moduleInstance->covInst, 0, 0);
    covrtLogBasicBlock(&moduleInstance->covInst, 0, 0);

    /*  Integer counter - it counts number of times called and time since last time enabled  */
    /*  Copyright 2013 The MathWorks Inc.  */
    /*  Shall count the number of times the system is called */
    /*  Shall count the number of times consecutively called */
    moduleInstance->sysobj.isInitialized = 0;
    moduleInstance->sysobj_not_empty = true;
  }

  obj = &moduleInstance->sysobj;
  if (moduleInstance->sysobj.isInitialized == 2) {
    for (i1 = 0; i1 < 45; i1++) {
      u[i1] = cv2[i1];
    }

    y = NULL;
    m1 = emlrtCreateCharArray(2, iv2);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m1, &u[0]);
    emlrtAssign(&y, m1);
    for (i1 = 0; i1 < 8; i1++) {
      b_u[i1] = cv3[i1];
    }

    b_y = NULL;
    m1 = emlrtCreateCharArray(2, iv3);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 8, m1, &b_u[0]);
    emlrtAssign(&b_y, m1);
    error(message(y, b_y, &emlrtMCI), &emlrtMCI);
  }

  flag = (obj->isInitialized == 1);
  if (flag) {
    obj = &moduleInstance->sysobj;
    if (moduleInstance->sysobj.isInitialized == 2) {
      for (i1 = 0; i1 < 45; i1++) {
        c_u[i1] = cv2[i1];
      }

      c_y = NULL;
      m1 = emlrtCreateCharArray(2, iv4);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m1, &c_u[0]);
      emlrtAssign(&c_y, m1);
      for (i1 = 0; i1 < 5; i1++) {
        d_u[i1] = cv4[i1];
      }

      d_y = NULL;
      m1 = emlrtCreateCharArray(2, iv5);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 5, m1, &d_u[0]);
      emlrtAssign(&d_y, m1);
      error(message(c_y, d_y, &emlrtMCI), &emlrtMCI);
    }

    if (obj->isInitialized == 1) {
      obj->consecCounter = 0.0;
    }
  }

  *counter = moduleInstance->sysobj.counter;
  *consecCounter = moduleInstance->sysobj.consecCounter;
}

static void cgxe_mdl_outputs(InstanceStruct_z3ClAipuvk2LM0lFTNXD1B
  *moduleInstance)
{
  slexLawOfLargeNumbersIntegerCounterSysObj *obj;
  int32_T i2;
  static char_T cv5[45] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'R', 'e', 'l', 'e', 'a', 's', 'e', 'd', 'C', 'o',
    'd', 'e', 'g', 'e', 'n' };

  char_T u[45];
  const mxArray *y;
  static const int32_T iv6[2] = { 1, 45 };

  const mxArray *m2;
  static char_T cv6[4] = { 's', 't', 'e', 'p' };

  char_T b_u[4];
  const mxArray *b_y;
  static const int32_T iv7[2] = { 1, 4 };

  static char_T cv7[51] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'L', 'o', 'c', 'k', 'e', 'd', 'R', 'e', 'l', 'e',
    'a', 's', 'e', 'd', 'C', 'o', 'd', 'e', 'g', 'e', 'n' };

  char_T c_u[51];
  const mxArray *c_y;
  static const int32_T iv8[2] = { 1, 51 };

  static char_T cv8[5] = { 's', 'e', 't', 'u', 'p' };

  char_T d_u[5];
  const mxArray *d_y;
  static const int32_T iv9[2] = { 1, 5 };

  real_T d0;
  real_T *counter;
  real_T *consecCounter;
  real_T *b_y0;
  real_T *b_y1;
  consecCounter = (real_T *)ssGetDWork(moduleInstance->S, 1U);
  counter = (real_T *)ssGetDWork(moduleInstance->S, 0U);
  b_y1 = (real_T *)ssGetOutputPortSignal(moduleInstance->S, 1U);
  b_y0 = (real_T *)ssGetOutputPortSignal(moduleInstance->S, 0U);
  moduleInstance->sysobj.counter = *counter;
  moduleInstance->sysobj.consecCounter = *consecCounter;
  if (!moduleInstance->sysobj_not_empty) {
    covrtLogFcn(&moduleInstance->covInst, 0, 0);
    covrtLogBasicBlock(&moduleInstance->covInst, 0, 0);

    /*  Integer counter - it counts number of times called and time since last time enabled  */
    /*  Copyright 2013 The MathWorks Inc.  */
    /*  Shall count the number of times the system is called */
    /*  Shall count the number of times consecutively called */
    moduleInstance->sysobj.isInitialized = 0;
    moduleInstance->sysobj_not_empty = true;
  }

  obj = &moduleInstance->sysobj;
  if (moduleInstance->sysobj.isInitialized == 2) {
    for (i2 = 0; i2 < 45; i2++) {
      u[i2] = cv5[i2];
    }

    y = NULL;
    m2 = emlrtCreateCharArray(2, iv6);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m2, &u[0]);
    emlrtAssign(&y, m2);
    for (i2 = 0; i2 < 4; i2++) {
      b_u[i2] = cv6[i2];
    }

    b_y = NULL;
    m2 = emlrtCreateCharArray(2, iv7);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 4, m2, &b_u[0]);
    emlrtAssign(&b_y, m2);
    error(message(y, b_y, &emlrtMCI), &emlrtMCI);
  }

  if (obj->isInitialized != 1) {
    if (obj->isInitialized != 0) {
      for (i2 = 0; i2 < 51; i2++) {
        c_u[i2] = cv7[i2];
      }

      c_y = NULL;
      m2 = emlrtCreateCharArray(2, iv8);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 51, m2, &c_u[0]);
      emlrtAssign(&c_y, m2);
      for (i2 = 0; i2 < 5; i2++) {
        d_u[i2] = cv8[i2];
      }

      d_y = NULL;
      m2 = emlrtCreateCharArray(2, iv9);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 5, m2, &d_u[0]);
      emlrtAssign(&d_y, m2);
      error(message(c_y, d_y, &emlrtMCI), &emlrtMCI);
    }

    obj->isInitialized = 1;

    /*  The conescutiveCounter shall be reset whenever  */
    /*  the enabled subsystem is reset and the enabled */
    /*  subsystem has been designed to reset state when */
    /*  The conescutiveCounter shall be reset whenever  */
    /*  the enabled subsystem is reset and the enabled */
    /*  subsystem has been designed to reset state when */
    obj->counter = 0.0;
    obj->consecCounter = 0.0;
    obj->consecCounter = 0.0;
  }

  /*  The conescutiveCounter shall be reset whenever  */
  /*  the enabled subsystem is reset and the enabled */
  /*  subsystem has been designed to reset state when */
  covrtLogFcn(&moduleInstance->covInst, 0, 1);
  covrtLogBasicBlock(&moduleInstance->covInst, 0, 1);
  d0 = obj->consecCounter + 1.0;
  obj->consecCounter = d0;
  d0 = obj->counter + 1.0;
  obj->counter = d0;
  *b_y0 = obj->consecCounter;
  *b_y1 = obj->counter;
  *counter = moduleInstance->sysobj.counter;
  *consecCounter = moduleInstance->sysobj.consecCounter;
}

static void cgxe_mdl_update(InstanceStruct_z3ClAipuvk2LM0lFTNXD1B
  *moduleInstance)
{
  real_T *counter;
  real_T *consecCounter;
  consecCounter = (real_T *)ssGetDWork(moduleInstance->S, 1U);
  counter = (real_T *)ssGetDWork(moduleInstance->S, 0U);
  *counter = moduleInstance->sysobj.counter;
  *consecCounter = moduleInstance->sysobj.consecCounter;
}

static void cgxe_mdl_terminate(InstanceStruct_z3ClAipuvk2LM0lFTNXD1B
  *moduleInstance)
{
  slexLawOfLargeNumbersIntegerCounterSysObj *obj;
  int32_T i3;
  static char_T cv9[45] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'R', 'e', 'l', 'e', 'a', 's', 'e', 'd', 'C', 'o',
    'd', 'e', 'g', 'e', 'n' };

  char_T u[45];
  const mxArray *y;
  static const int32_T iv10[2] = { 1, 45 };

  const mxArray *m3;
  static char_T cv10[8] = { 'i', 's', 'L', 'o', 'c', 'k', 'e', 'd' };

  char_T b_u[8];
  const mxArray *b_y;
  static const int32_T iv11[2] = { 1, 8 };

  boolean_T flag;
  char_T c_u[45];
  const mxArray *c_y;
  static const int32_T iv12[2] = { 1, 45 };

  static char_T cv11[7] = { 'r', 'e', 'l', 'e', 'a', 's', 'e' };

  char_T d_u[7];
  const mxArray *d_y;
  static const int32_T iv13[2] = { 1, 7 };

  real_T *counter;
  real_T *consecCounter;
  consecCounter = (real_T *)ssGetDWork(moduleInstance->S, 1U);
  counter = (real_T *)ssGetDWork(moduleInstance->S, 0U);
  if (!moduleInstance->sysobj_not_empty) {
    covrtLogFcn(&moduleInstance->covInst, 0, 0);
    covrtLogBasicBlock(&moduleInstance->covInst, 0, 0);

    /*  Integer counter - it counts number of times called and time since last time enabled  */
    /*  Copyright 2013 The MathWorks Inc.  */
    /*  Shall count the number of times the system is called */
    /*  Shall count the number of times consecutively called */
    moduleInstance->sysobj.isInitialized = 0;
    moduleInstance->sysobj_not_empty = true;
  }

  obj = &moduleInstance->sysobj;
  if (moduleInstance->sysobj.isInitialized == 2) {
    for (i3 = 0; i3 < 45; i3++) {
      u[i3] = cv9[i3];
    }

    y = NULL;
    m3 = emlrtCreateCharArray(2, iv10);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m3, &u[0]);
    emlrtAssign(&y, m3);
    for (i3 = 0; i3 < 8; i3++) {
      b_u[i3] = cv10[i3];
    }

    b_y = NULL;
    m3 = emlrtCreateCharArray(2, iv11);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 8, m3, &b_u[0]);
    emlrtAssign(&b_y, m3);
    error(message(y, b_y, &emlrtMCI), &emlrtMCI);
  }

  flag = (obj->isInitialized == 1);
  if (flag) {
    obj = &moduleInstance->sysobj;
    if (moduleInstance->sysobj.isInitialized == 2) {
      for (i3 = 0; i3 < 45; i3++) {
        c_u[i3] = cv9[i3];
      }

      c_y = NULL;
      m3 = emlrtCreateCharArray(2, iv12);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m3, &c_u[0]);
      emlrtAssign(&c_y, m3);
      for (i3 = 0; i3 < 7; i3++) {
        d_u[i3] = cv11[i3];
      }

      d_y = NULL;
      m3 = emlrtCreateCharArray(2, iv13);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 7, m3, &d_u[0]);
      emlrtAssign(&d_y, m3);
      error(message(c_y, d_y, &emlrtMCI), &emlrtMCI);
    }

    if (obj->isInitialized == 1) {
      obj->isInitialized = 2;
    }
  }

  /* Free instance data */
  covrtFreeInstanceData(&moduleInstance->covInst);
  *counter = moduleInstance->sysobj.counter;
  *consecCounter = moduleInstance->sysobj.consecCounter;
}

static const mxArray *mw__internal__name__resolution__fcn(void)
{
  const mxArray *nameCaptureInfo;
  nameCaptureInfo = NULL;
  emlrtAssign(&nameCaptureInfo, emlrtCreateStructMatrix(45, 1, 0, NULL));
  info_helper(&nameCaptureInfo);
  emlrtNameCapturePostProcessR2013b(&nameCaptureInfo);
  return nameCaptureInfo;
}

static void info_helper(const mxArray **info)
{
  emlrtAddField(*info, emlrt_marshallOut(""), "context", 0);
  emlrtAddField(*info, emlrt_marshallOut("repmat"), "name", 0);
  emlrtAddField(*info, emlrt_marshallOut("struct"), "dominantType", 0);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "resolved", 0);
  emlrtAddField(*info, b_emlrt_marshallOut(1408770110U), "fileTimeLo", 0);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 0);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 0);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 0);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "context", 1);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.narginchk"), "name", 1);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 1);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/narginchk.m"),
                "resolved", 1);
  emlrtAddField(*info, b_emlrt_marshallOut(1363710958U), "fileTimeLo", 1);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 1);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 1);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 1);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/narginchk.m"),
                "context", 2);
  emlrtAddField(*info, emlrt_marshallOut("floor"), "name", 2);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 2);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "resolved", 2);
  emlrtAddField(*info, b_emlrt_marshallOut(1363710254U), "fileTimeLo", 2);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 2);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 2);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 2);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context", 3);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 3);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 3);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 3);
  emlrtAddField(*info, b_emlrt_marshallOut(1395928256U), "fileTimeLo", 3);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 3);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 3);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 3);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context", 4);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalar_floor"), "name", 4);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 4);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_floor.m"),
                "resolved", 4);
  emlrtAddField(*info, b_emlrt_marshallOut(1286818726U), "fileTimeLo", 4);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 4);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 4);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 4);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "context", 5);
  emlrtAddField(*info, emlrt_marshallOut("eml_assert_valid_size_arg"), "name", 5);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 5);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m"),
                "resolved", 5);
  emlrtAddField(*info, b_emlrt_marshallOut(1368183030U), "fileTimeLo", 5);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 5);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 5);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 5);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m"),
                "context", 6);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 6);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 6);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 6);
  emlrtAddField(*info, b_emlrt_marshallOut(1395928256U), "fileTimeLo", 6);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 6);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 6);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 6);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isintegral"),
                "context", 7);
  emlrtAddField(*info, emlrt_marshallOut("isinf"), "name", 7);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 7);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "resolved", 7);
  emlrtAddField(*info, b_emlrt_marshallOut(1363710256U), "fileTimeLo", 7);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 7);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 7);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 7);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "context", 8);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 8);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 8);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 8);
  emlrtAddField(*info, b_emlrt_marshallOut(1395928256U), "fileTimeLo", 8);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 8);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 8);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 8);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isinbounds"),
                "context", 9);
  emlrtAddField(*info, emlrt_marshallOut("eml_is_integer_class"), "name", 9);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 9);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_is_integer_class.m"),
                "resolved", 9);
  emlrtAddField(*info, b_emlrt_marshallOut(1286818782U), "fileTimeLo", 9);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 9);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 9);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 9);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isinbounds"),
                "context", 10);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 10);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 10);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 10);
  emlrtAddField(*info, b_emlrt_marshallOut(1362258282U), "fileTimeLo", 10);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 10);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 10);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 10);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "context", 11);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 11);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 11);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 11);
  emlrtAddField(*info, b_emlrt_marshallOut(1393327258U), "fileTimeLo", 11);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 11);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 11);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 11);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isinbounds"),
                "context", 12);
  emlrtAddField(*info, emlrt_marshallOut("intmin"), "name", 12);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 12);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved", 12);
  emlrtAddField(*info, b_emlrt_marshallOut(1362258282U), "fileTimeLo", 12);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 12);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 12);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 12);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "context", 13);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 13);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 13);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 13);
  emlrtAddField(*info, b_emlrt_marshallOut(1393327258U), "fileTimeLo", 13);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 13);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 13);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 13);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isinbounds"),
                "context", 14);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexIntRelop"), "name",
                14);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 14);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m"),
                "resolved", 14);
  emlrtAddField(*info, b_emlrt_marshallOut(1326724722U), "fileTimeLo", 14);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 14);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 14);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 14);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m!apply_float_relop"),
                "context", 15);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 15);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 15);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 15);
  emlrtAddField(*info, b_emlrt_marshallOut(1393327258U), "fileTimeLo", 15);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 15);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 15);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 15);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m!float_class_contains_indexIntClass"),
                "context", 16);
  emlrtAddField(*info, emlrt_marshallOut("eml_float_model"), "name", 16);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 16);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_model.m"),
                "resolved", 16);
  emlrtAddField(*info, b_emlrt_marshallOut(1326724396U), "fileTimeLo", 16);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 16);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 16);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 16);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m!is_signed_indexIntClass"),
                "context", 17);
  emlrtAddField(*info, emlrt_marshallOut("intmin"), "name", 17);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 17);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved", 17);
  emlrtAddField(*info, b_emlrt_marshallOut(1362258282U), "fileTimeLo", 17);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 17);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 17);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 17);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m"),
                "context", 18);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_class"), "name", 18);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 18);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                "resolved", 18);
  emlrtAddField(*info, b_emlrt_marshallOut(1323166978U), "fileTimeLo", 18);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 18);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 18);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 18);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m"),
                "context", 19);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 19);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 19);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 19);
  emlrtAddField(*info, b_emlrt_marshallOut(1362258282U), "fileTimeLo", 19);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 19);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 19);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 19);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "context", 20);
  emlrtAddField(*info, emlrt_marshallOut("max"), "name", 20);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 20);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/max.m"), "resolved", 20);
  emlrtAddField(*info, b_emlrt_marshallOut(1311255316U), "fileTimeLo", 20);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 20);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 20);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 20);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/max.m"), "context", 21);
  emlrtAddField(*info, emlrt_marshallOut("eml_min_or_max"), "name", 21);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 21);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m"),
                "resolved", 21);
  emlrtAddField(*info, b_emlrt_marshallOut(1378295984U), "fileTimeLo", 21);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 21);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 21);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 21);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_bin_extremum"),
                "context", 22);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalar_eg"), "name", 22);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 22);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                22);
  emlrtAddField(*info, b_emlrt_marshallOut(1375980688U), "fileTimeLo", 22);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 22);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 22);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 22);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "context",
                23);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.scalarEg"), "name", 23);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 23);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                "resolved", 23);
  emlrtAddField(*info, b_emlrt_marshallOut(1419947828U), "fileTimeLo", 23);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 23);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 23);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 23);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_bin_extremum"),
                "context", 24);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalexp_alloc"), "name", 24);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 24);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                "resolved", 24);
  emlrtAddField(*info, b_emlrt_marshallOut(1375980688U), "fileTimeLo", 24);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 24);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 24);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 24);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                "context", 25);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.scalexpAlloc"), "name",
                25);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 25);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalexpAlloc.p"),
                "resolved", 25);
  emlrtAddField(*info, b_emlrt_marshallOut(1419947828U), "fileTimeLo", 25);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 25);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 25);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 25);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalexpAlloc.p"),
                "context", 26);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.scalexpAllocNoCheck"),
                "name", 26);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 26);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalexpAllocNoCheck.p"),
                "resolved", 26);
  emlrtAddField(*info, b_emlrt_marshallOut(1419947828U), "fileTimeLo", 26);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 26);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 26);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 26);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_bin_extremum"),
                "context", 27);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_class"), "name", 27);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 27);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                "resolved", 27);
  emlrtAddField(*info, b_emlrt_marshallOut(1323166978U), "fileTimeLo", 27);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 27);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 27);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 27);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_scalar_bin_extremum"),
                "context", 28);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalar_eg"), "name", 28);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 28);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                28);
  emlrtAddField(*info, b_emlrt_marshallOut(1375980688U), "fileTimeLo", 28);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 28);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 28);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 28);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_scalar_bin_extremum"),
                "context", 29);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 29);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 29);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 29);
  emlrtAddField(*info, b_emlrt_marshallOut(1395928256U), "fileTimeLo", 29);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 29);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 29);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 29);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "context", 30);
  emlrtAddField(*info, emlrt_marshallOut("eml_int_forloop_overflow_check"),
                "name", 30);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 30);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                "resolved", 30);
  emlrtAddField(*info, b_emlrt_marshallOut(1400520780U), "fileTimeLo", 30);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 30);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 30);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 30);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                "context", 31);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 31);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 31);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 31);
  emlrtAddField(*info, b_emlrt_marshallOut(1346510358U), "fileTimeLo", 31);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 31);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 31);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 31);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "context", 32);
  emlrtAddField(*info, emlrt_marshallOut("isnumerictype"), "name", 32);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 32);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                32);
  emlrtAddField(*info, b_emlrt_marshallOut(1398875598U), "fileTimeLo", 32);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 32);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 32);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 32);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                "context", 33);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 33);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 33);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 33);
  emlrtAddField(*info, b_emlrt_marshallOut(1362258282U), "fileTimeLo", 33);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 33);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 33);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 33);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                "context", 34);
  emlrtAddField(*info, emlrt_marshallOut("intmin"), "name", 34);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 34);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved", 34);
  emlrtAddField(*info, b_emlrt_marshallOut(1362258282U), "fileTimeLo", 34);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 34);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 34);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 34);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 35);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.matlabCodegenHandle"),
                "name", 35);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 35);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/matlabCodegenHandle.p"),
                "resolved", 35);
  emlrtAddField(*info, b_emlrt_marshallOut(1419947830U), "fileTimeLo", 35);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 35);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 35);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 35);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/System.p"),
                "context", 36);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.SystemProp"),
                "name", 36);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 36);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "resolved", 36);
  emlrtAddField(*info, b_emlrt_marshallOut(1419946710U), "fileTimeLo", 36);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 36);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 36);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 36);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemCore.p"),
                "context", 37);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.matlabCodegenHandle"),
                "name", 37);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 37);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/matlabCodegenHandle.p"),
                "resolved", 37);
  emlrtAddField(*info, b_emlrt_marshallOut(1419947830U), "fileTimeLo", 37);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 37);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 37);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 37);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/System.p"),
                "context", 38);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.SystemCore"),
                "name", 38);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 38);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemCore.p"),
                "resolved", 38);
  emlrtAddField(*info, b_emlrt_marshallOut(1419946710U), "fileTimeLo", 38);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 38);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 38);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 38);
  emlrtAddField(*info, emlrt_marshallOut(
    "[XC]$matlabroot$/toolbox/simulink/simdemos/simfeatures/slexLawOfLargeNumbersIntegerCounterSysObj.m"),
                "context", 39);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.System"), "name",
                39);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 39);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/System.p"),
                "resolved", 39);
  emlrtAddField(*info, b_emlrt_marshallOut(1419946710U), "fileTimeLo", 39);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 39);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 39);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 39);
  emlrtAddField(*info, emlrt_marshallOut(""), "context", 40);
  emlrtAddField(*info, emlrt_marshallOut(
    "slexLawOfLargeNumbersIntegerCounterSysObj"), "name", 40);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 40);
  emlrtAddField(*info, emlrt_marshallOut(
    "[XC]$matlabroot$/toolbox/simulink/simdemos/simfeatures/slexLawOfLargeNumbersIntegerCounterSysObj.m"),
                "resolved", 40);
  emlrtAddField(*info, b_emlrt_marshallOut(1368184676U), "fileTimeLo", 40);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 40);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 40);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 40);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 41);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.SystemProp"),
                "name", 41);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 41);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "resolved", 41);
  emlrtAddField(*info, b_emlrt_marshallOut(1419946710U), "fileTimeLo", 41);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 41);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 41);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 41);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemCore.p"),
                "context", 42);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.SystemCore"),
                "name", 42);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 42);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemCore.p"),
                "resolved", 42);
  emlrtAddField(*info, b_emlrt_marshallOut(1419946710U), "fileTimeLo", 42);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 42);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 42);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 42);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 43);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 43);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 43);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 43);
  emlrtAddField(*info, b_emlrt_marshallOut(1393327258U), "fileTimeLo", 43);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 43);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 43);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 43);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 44);
  emlrtAddField(*info, emlrt_marshallOut("issparse"), "name", 44);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 44);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/sparfun/issparse.m"), "resolved",
                44);
  emlrtAddField(*info, b_emlrt_marshallOut(1286818830U), "fileTimeLo", 44);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 44);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 44);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 44);
}

static const mxArray *emlrt_marshallOut(const char * u)
{
  const mxArray *y;
  const mxArray *m4;
  y = NULL;
  m4 = emlrtCreateString(u);
  emlrtAssign(&y, m4);
  return y;
}

static const mxArray *b_emlrt_marshallOut(const uint32_T u)
{
  const mxArray *y;
  const mxArray *m5;
  y = NULL;
  m5 = emlrtCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
  *(uint32_T *)mxGetData(m5) = u;
  emlrtAssign(&y, m5);
  return y;
}

static const mxArray *mw__internal__autoInference__fcn(void)
{
  const mxArray *infoCache;
  sfOd2wElE6un66xmZCZog7F Ports_data[2];
  sfOd2wElE6un66xmZCZog7F_size Ports_elems_sizes[2];
  int32_T i4;
  static int8_T iv14[4] = { 1, 2, 1, 1 };

  sZVQz5WVraeIWEljxFvLe8_size dWork_elems_sizes[2];
  static char_T cv12[7] = { 'c', 'o', 'u', 'n', 't', 'e', 'r' };

  sZVQz5WVraeIWEljxFvLe8 dWork_data[2];
  static char_T cv13[13] = { 'c', 'o', 'n', 's', 'e', 'c', 'C', 'o', 'u', 'n',
    't', 'e', 'r' };

  uint32_T RestoreInfo_cgxeChksum[4];
  uint32_T info_VerificationInfo_dlgPrmChksum_chksum[4];
  static uint32_T uv0[4] = { 101263322U, 4231397629U, 3840658636U, 1797259858U };

  static uint32_T t0_dlgPrmChksum_chksum[4] = { 4229830352U, 3428762220U,
    3588391521U, 3882426728U };

  s7UBIGHSehQY1gCsIQWwr5C info_VerificationInfo_propChksum[3];
  static s7UBIGHSehQY1gCsIQWwr5C b_t0_propChksum[3] = { { { 0.0, 0.0, 0.0, 0.0 }
    }, { { 0.0, 0.0, 0.0, 0.0 } }, { { 0.0, 0.0, 0.0, 0.0 } } };

  uint32_T b_t0_dlgPrmChksum_chksum[4];
  uint32_T info_VerificationInfo_postPropOnlyChksum_chksum[4];
  static uint32_T t0_CGOnlyParamChksum_chksum[4] = { 907274058U, 3220786148U,
    779473529U, 2769831971U };

  static uint32_T t0_postPropOnlyChksum_chksum[4] = { 2911221907U, 2308967934U,
    2419390157U, 1906300239U };

  char_T info_slVer[3];
  static char_T cv14[3] = { '8', '.', '5' };

  const mxArray *y;
  const mxArray *b_y;
  const mxArray *c_y;
  sfOd2wElE6un66xmZCZog7F_size u_elems_sizes[2];
  sfOd2wElE6un66xmZCZog7F u_data[2];
  const mxArray *d_y;
  int32_T iv15[2];
  const sfOd2wElE6un66xmZCZog7F_size *tmp_elems_sizes;
  const sfOd2wElE6un66xmZCZog7F *tmp_data;
  real_T u;
  const mxArray *e_y;
  const mxArray *m6;
  const mxArray *f_y;
  const mxArray *g_y;
  const mxArray *h_y;
  sZVQz5WVraeIWEljxFvLe8_size b_u_elems_sizes[2];
  sZVQz5WVraeIWEljxFvLe8 b_u_data[2];
  const mxArray *i_y;
  const sZVQz5WVraeIWEljxFvLe8_size *b_tmp_elems_sizes;
  const sZVQz5WVraeIWEljxFvLe8 *b_tmp_data;
  int32_T u_sizes[2];
  int32_T loop_ub;
  int32_T i;
  char_T c_u_data[13];
  const mxArray *j_y;
  const mxArray *k_y;
  const mxArray *l_y;
  const mxArray *m_y;
  static const int32_T iv16[2] = { 1, 0 };

  const mxArray *n_y;
  const mxArray *o_y;
  const mxArray *p_y;
  static const int32_T iv17[2] = { 1, 0 };

  const mxArray *q_y;
  const mxArray *r_y;
  const mxArray *s_y;
  const mxArray *t_y;
  const mxArray *u_y;
  const mxArray *v_y;
  static const int32_T iv18[2] = { 0, 0 };

  const mxArray *w_y;
  const mxArray *x_y;
  static const int32_T iv19[2] = { 1, 4 };

  real_T *pData;
  const mxArray *y_y;
  const mxArray *ab_y;
  const mxArray *bb_y;
  static const int32_T iv20[2] = { 1, 4 };

  s7UBIGHSehQY1gCsIQWwr5C b_u[3];
  const mxArray *cb_y;
  const s7UBIGHSehQY1gCsIQWwr5C *r0;
  real_T c_u[4];
  const mxArray *db_y;
  static const int32_T iv21[2] = { 1, 4 };

  const mxArray *eb_y;
  const mxArray *fb_y;
  static const int32_T iv22[2] = { 1, 4 };

  const mxArray *gb_y;
  const mxArray *hb_y;
  static const int32_T iv23[2] = { 1, 4 };

  char_T d_u[3];
  const mxArray *ib_y;
  static const int32_T iv24[2] = { 1, 3 };

  infoCache = NULL;
  Ports_data[0].dimModes = 0.0;
  Ports_elems_sizes[0].dims[0] = 1;
  Ports_elems_sizes[0].dims[1] = 4;
  for (i4 = 0; i4 < 4; i4++) {
    Ports_data[0].dims[i4] = (real_T)iv14[i4];
  }

  Ports_data[0].dType = 0.0;
  Ports_data[0].complexity = 0.0;
  Ports_data[0].outputBuiltInDTEqUsed = 0.0;
  Ports_data[1].dimModes = 0.0;
  Ports_elems_sizes[1].dims[0] = 1;
  Ports_elems_sizes[1].dims[1] = 4;
  for (i4 = 0; i4 < 4; i4++) {
    Ports_data[1].dims[i4] = (real_T)iv14[i4];
  }

  Ports_data[1].dType = 0.0;
  Ports_data[1].complexity = 0.0;
  Ports_data[1].outputBuiltInDTEqUsed = 0.0;
  dWork_elems_sizes[0].names[0] = 1;
  dWork_elems_sizes[0].names[1] = 7;
  for (i4 = 0; i4 < 7; i4++) {
    dWork_data[0].names[i4] = cv12[i4];
  }

  dWork_elems_sizes[0].dims[0] = 1;
  dWork_elems_sizes[0].dims[1] = 4;
  for (i4 = 0; i4 < 4; i4++) {
    dWork_data[0].dims[i4] = (real_T)iv14[i4];
  }

  dWork_data[0].dType = 0.0;
  dWork_data[0].complexity = 0.0;
  dWork_elems_sizes[1].names[0] = 1;
  dWork_elems_sizes[1].names[1] = 13;
  for (i4 = 0; i4 < 13; i4++) {
    dWork_data[1].names[i4] = cv13[i4];
  }

  dWork_elems_sizes[1].dims[0] = 1;
  dWork_elems_sizes[1].dims[1] = 4;
  for (i4 = 0; i4 < 4; i4++) {
    dWork_data[1].dims[i4] = (real_T)iv14[i4];
  }

  dWork_data[1].dType = 0.0;
  dWork_data[1].complexity = 0.0;
  for (i4 = 0; i4 < 4; i4++) {
    RestoreInfo_cgxeChksum[i4] = uv0[i4];
    info_VerificationInfo_dlgPrmChksum_chksum[i4] = t0_dlgPrmChksum_chksum[i4];
  }

  for (i4 = 0; i4 < 3; i4++) {
    info_VerificationInfo_propChksum[i4] = b_t0_propChksum[i4];
  }

  for (i4 = 0; i4 < 4; i4++) {
    b_t0_dlgPrmChksum_chksum[i4] = t0_CGOnlyParamChksum_chksum[i4];
    info_VerificationInfo_postPropOnlyChksum_chksum[i4] =
      t0_postPropOnlyChksum_chksum[i4];
  }

  for (i4 = 0; i4 < 3; i4++) {
    info_slVer[i4] = cv14[i4];
  }

  y = NULL;
  emlrtAssign(&y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  b_y = NULL;
  emlrtAssign(&b_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  c_y = NULL;
  emlrtAssign(&c_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  for (i4 = 0; i4 < 2; i4++) {
    u_elems_sizes[i4] = Ports_elems_sizes[i4];
    u_data[i4] = Ports_data[i4];
  }

  d_y = NULL;
  for (i4 = 0; i4 < 2; i4++) {
    iv15[i4] = 1 + i4;
  }

  emlrtAssign(&d_y, emlrtCreateStructArray(2, iv15, 0, NULL));
  for (i4 = 0; i4 < 2; i4++) {
    tmp_elems_sizes = &u_elems_sizes[i4];
    tmp_data = &u_data[i4];
    u = tmp_data->dimModes;
    e_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&e_y, m6);
    emlrtAddField(d_y, e_y, "dimModes", i4);
    emlrtAddField(d_y, c_emlrt_marshallOut(tmp_data->dims, tmp_elems_sizes->dims),
                  "dims", i4);
    u = tmp_data->dType;
    f_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&f_y, m6);
    emlrtAddField(d_y, f_y, "dType", i4);
    u = tmp_data->complexity;
    g_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&g_y, m6);
    emlrtAddField(d_y, g_y, "complexity", i4);
    u = tmp_data->outputBuiltInDTEqUsed;
    h_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&h_y, m6);
    emlrtAddField(d_y, h_y, "outputBuiltInDTEqUsed", i4);
  }

  emlrtAddField(c_y, d_y, "Ports", 0);
  for (i4 = 0; i4 < 2; i4++) {
    b_u_elems_sizes[i4] = dWork_elems_sizes[i4];
    b_u_data[i4] = dWork_data[i4];
  }

  i_y = NULL;
  for (i4 = 0; i4 < 2; i4++) {
    iv15[i4] = 1 + i4;
  }

  emlrtAssign(&i_y, emlrtCreateStructArray(2, iv15, 0, NULL));
  for (i4 = 0; i4 < 2; i4++) {
    b_tmp_elems_sizes = &b_u_elems_sizes[i4];
    b_tmp_data = &b_u_data[i4];
    u_sizes[0] = 1;
    u_sizes[1] = b_tmp_elems_sizes->names[1];
    loop_ub = b_tmp_elems_sizes->names[0] * b_tmp_elems_sizes->names[1];
    for (i = 0; i < loop_ub; i++) {
      c_u_data[i] = b_tmp_data->names[i];
    }

    j_y = NULL;
    m6 = emlrtCreateCharArray(2, u_sizes);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, u_sizes[1], m6, &c_u_data[0]);
    emlrtAssign(&j_y, m6);
    emlrtAddField(i_y, j_y, "names", i4);
    emlrtAddField(i_y, c_emlrt_marshallOut(b_tmp_data->dims,
      b_tmp_elems_sizes->dims), "dims", i4);
    u = b_tmp_data->dType;
    k_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&k_y, m6);
    emlrtAddField(i_y, k_y, "dType", i4);
    u = b_tmp_data->complexity;
    l_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&l_y, m6);
    emlrtAddField(i_y, l_y, "complexity", i4);
  }

  emlrtAddField(c_y, i_y, "dWork", 0);
  m_y = NULL;
  m6 = emlrtCreateCharArray(2, iv16);
  emlrtAssign(&m_y, m6);
  emlrtAddField(c_y, m_y, "objTypeName", 0);
  n_y = NULL;
  m6 = emlrtCreateDoubleScalar(0.0);
  emlrtAssign(&n_y, m6);
  emlrtAddField(c_y, n_y, "objTypeSize", 0);
  o_y = NULL;
  for (i4 = 0; i4 < 2; i4++) {
    iv15[i4] = 1 - i4;
  }

  emlrtAssign(&o_y, emlrtCreateStructArray(2, iv15, 0, NULL));
  emlrtAddField(o_y, NULL, "names", 0);
  emlrtAddField(o_y, NULL, "dims", 0);
  emlrtAddField(o_y, NULL, "dType", 0);
  emlrtAddField(o_y, NULL, "dTypeSize", 0);
  emlrtAddField(o_y, NULL, "dTypeName", 0);
  emlrtAddField(o_y, NULL, "dTypeIndex", 0);
  emlrtAddField(o_y, NULL, "dTypeChksum", 0);
  emlrtAddField(o_y, NULL, "complexity", 0);
  emlrtAddField(c_y, o_y, "persisVarDWork", 0);
  p_y = NULL;
  m6 = emlrtCreateCharArray(2, iv17);
  emlrtAssign(&p_y, m6);
  emlrtAddField(c_y, p_y, "sysObjChksum", 0);
  q_y = NULL;
  emlrtAssign(&q_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  r_y = NULL;
  for (i4 = 0; i4 < 2; i4++) {
    iv15[i4] = 1 - i4;
  }

  emlrtAssign(&r_y, emlrtCreateStructArray(2, iv15, 0, NULL));
  emlrtAddField(r_y, NULL, "Index", 0);
  emlrtAddField(r_y, NULL, "DataType", 0);
  emlrtAddField(r_y, NULL, "IsSigned", 0);
  emlrtAddField(r_y, NULL, "MantBits", 0);
  emlrtAddField(r_y, NULL, "FixExp", 0);
  emlrtAddField(r_y, NULL, "Slope", 0);
  emlrtAddField(r_y, NULL, "Bias", 0);
  emlrtAddField(q_y, r_y, "Out", 0);
  s_y = NULL;
  for (i4 = 0; i4 < 2; i4++) {
    iv15[i4] = 1 - i4;
  }

  emlrtAssign(&s_y, emlrtCreateStructArray(2, iv15, 0, NULL));
  emlrtAddField(s_y, NULL, "Index", 0);
  emlrtAddField(s_y, NULL, "DataType", 0);
  emlrtAddField(s_y, NULL, "IsSigned", 0);
  emlrtAddField(s_y, NULL, "MantBits", 0);
  emlrtAddField(s_y, NULL, "FixExp", 0);
  emlrtAddField(s_y, NULL, "Slope", 0);
  emlrtAddField(s_y, NULL, "Bias", 0);
  emlrtAddField(q_y, s_y, "DW", 0);
  t_y = NULL;
  for (i4 = 0; i4 < 2; i4++) {
    iv15[i4] = 1 - i4;
  }

  emlrtAssign(&t_y, emlrtCreateStructArray(2, iv15, 0, NULL));
  emlrtAddField(t_y, NULL, "Index", 0);
  emlrtAddField(t_y, NULL, "DataType", 0);
  emlrtAddField(t_y, NULL, "IsSigned", 0);
  emlrtAddField(t_y, NULL, "MantBits", 0);
  emlrtAddField(t_y, NULL, "FixExp", 0);
  emlrtAddField(t_y, NULL, "Slope", 0);
  emlrtAddField(t_y, NULL, "Bias", 0);
  emlrtAddField(q_y, t_y, "PersisDW", 0);
  emlrtAddField(c_y, q_y, "mapsInfo", 0);
  u_y = NULL;
  m6 = emlrtCreateDoubleScalar(0.0);
  emlrtAssign(&u_y, m6);
  emlrtAddField(c_y, u_y, "objDWorkTypeNameIndex", 0);
  v_y = NULL;
  m6 = emlrtCreateNumericArray(2, iv18, mxDOUBLE_CLASS, mxREAL);
  emlrtAssign(&v_y, m6);
  emlrtAddField(c_y, v_y, "inputDFFlagsIndexField", 0);
  w_y = NULL;
  m6 = emlrtCreateDoubleScalar(0.0);
  emlrtAssign(&w_y, m6);
  emlrtAddField(c_y, w_y, "postPropRun", 0);
  emlrtAddField(b_y, c_y, "DispatcherInfo", 0);
  x_y = NULL;
  m6 = emlrtCreateNumericArray(2, iv19, mxDOUBLE_CLASS, mxREAL);
  pData = (real_T *)mxGetPr(m6);
  for (i = 0; i < 4; i++) {
    pData[i] = (real_T)RestoreInfo_cgxeChksum[i];
  }

  emlrtAssign(&x_y, m6);
  emlrtAddField(b_y, x_y, "cgxeChksum", 0);
  emlrtAddField(y, b_y, "RestoreInfo", 0);
  y_y = NULL;
  emlrtAssign(&y_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  ab_y = NULL;
  emlrtAssign(&ab_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  bb_y = NULL;
  m6 = emlrtCreateNumericArray(2, iv20, mxDOUBLE_CLASS, mxREAL);
  pData = (real_T *)mxGetPr(m6);
  for (i = 0; i < 4; i++) {
    pData[i] = (real_T)info_VerificationInfo_dlgPrmChksum_chksum[i];
  }

  emlrtAssign(&bb_y, m6);
  emlrtAddField(ab_y, bb_y, "chksum", 0);
  emlrtAddField(y_y, ab_y, "dlgPrmChksum", 0);
  for (i4 = 0; i4 < 3; i4++) {
    b_u[i4] = info_VerificationInfo_propChksum[i4];
  }

  cb_y = NULL;
  for (i4 = 0; i4 < 2; i4++) {
    iv15[i4] = 1 + (i4 << 1);
  }

  emlrtAssign(&cb_y, emlrtCreateStructArray(2, iv15, 0, NULL));
  for (i4 = 0; i4 < 3; i4++) {
    r0 = &b_u[i4];
    for (i = 0; i < 4; i++) {
      c_u[i] = r0->chksum[i];
    }

    db_y = NULL;
    m6 = emlrtCreateNumericArray(2, iv21, mxDOUBLE_CLASS, mxREAL);
    pData = (real_T *)mxGetPr(m6);
    for (i = 0; i < 4; i++) {
      pData[i] = c_u[i];
    }

    emlrtAssign(&db_y, m6);
    emlrtAddField(cb_y, db_y, "chksum", i4);
  }

  emlrtAddField(y_y, cb_y, "propChksum", 0);
  eb_y = NULL;
  emlrtAssign(&eb_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  fb_y = NULL;
  m6 = emlrtCreateNumericArray(2, iv22, mxDOUBLE_CLASS, mxREAL);
  pData = (real_T *)mxGetPr(m6);
  for (i = 0; i < 4; i++) {
    pData[i] = (real_T)b_t0_dlgPrmChksum_chksum[i];
  }

  emlrtAssign(&fb_y, m6);
  emlrtAddField(eb_y, fb_y, "chksum", 0);
  emlrtAddField(y_y, eb_y, "CGOnlyParamChksum", 0);
  gb_y = NULL;
  emlrtAssign(&gb_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  hb_y = NULL;
  m6 = emlrtCreateNumericArray(2, iv23, mxDOUBLE_CLASS, mxREAL);
  pData = (real_T *)mxGetPr(m6);
  for (i = 0; i < 4; i++) {
    pData[i] = (real_T)info_VerificationInfo_postPropOnlyChksum_chksum[i];
  }

  emlrtAssign(&hb_y, m6);
  emlrtAddField(gb_y, hb_y, "chksum", 0);
  emlrtAddField(y_y, gb_y, "postPropOnlyChksum", 0);
  emlrtAddField(y, y_y, "VerificationInfo", 0);
  for (i4 = 0; i4 < 3; i4++) {
    d_u[i4] = info_slVer[i4];
  }

  ib_y = NULL;
  m6 = emlrtCreateCharArray(2, iv24);
  emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 3, m6, &d_u[0]);
  emlrtAssign(&ib_y, m6);
  emlrtAddField(y, ib_y, "slVer", 0);
  emlrtAssign(&infoCache, y);
  return infoCache;
}

static const mxArray *c_emlrt_marshallOut(const real_T u_data[], const int32_T
  u_sizes[2])
{
  const mxArray *y;
  const mxArray *m7;
  real_T *pData;
  int32_T i5;
  int32_T i;
  y = NULL;
  m7 = emlrtCreateNumericArray(2, u_sizes, mxDOUBLE_CLASS, mxREAL);
  pData = (real_T *)mxGetPr(m7);
  i5 = 0;
  for (i = 0; i < u_sizes[1]; i++) {
    pData[i5] = u_data[u_sizes[0] * i];
    i5++;
  }

  emlrtAssign(&y, m7);
  return y;
}

static const mxArray *mw__internal__getSimState__fcn
  (InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *moduleInstance)
{
  const mxArray *st;
  const mxArray *y;
  const mxArray *b_y;
  const mxArray *m8;
  const mxArray *c_y;
  const mxArray *d_y;
  const mxArray *e_y;
  const mxArray *f_y;
  const mxArray *g_y;
  const mxArray *h_y;
  real_T *consecCounter;
  real_T *counter;
  consecCounter = (real_T *)ssGetDWork(moduleInstance->S, 1U);
  counter = (real_T *)ssGetDWork(moduleInstance->S, 0U);
  st = NULL;
  y = NULL;
  emlrtAssign(&y, emlrtCreateCellMatrix(4, 1));
  b_y = NULL;
  m8 = emlrtCreateDoubleScalar(*consecCounter);
  emlrtAssign(&b_y, m8);
  emlrtSetCell(y, 0, b_y);
  c_y = NULL;
  m8 = emlrtCreateDoubleScalar(*counter);
  emlrtAssign(&c_y, m8);
  emlrtSetCell(y, 1, c_y);
  d_y = NULL;
  emlrtAssign(&d_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  e_y = NULL;
  m8 = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)mxGetData(m8) = moduleInstance->sysobj.isInitialized;
  emlrtAssign(&e_y, m8);
  emlrtAddField(d_y, e_y, "isInitialized", 0);
  f_y = NULL;
  m8 = emlrtCreateDoubleScalar(moduleInstance->sysobj.counter);
  emlrtAssign(&f_y, m8);
  emlrtAddField(d_y, f_y, "counter", 0);
  g_y = NULL;
  m8 = emlrtCreateDoubleScalar(moduleInstance->sysobj.consecCounter);
  emlrtAssign(&g_y, m8);
  emlrtAddField(d_y, g_y, "consecCounter", 0);
  emlrtSetCell(y, 2, d_y);
  h_y = NULL;
  m8 = emlrtCreateLogicalScalar(moduleInstance->sysobj_not_empty);
  emlrtAssign(&h_y, m8);
  emlrtSetCell(y, 3, h_y);
  emlrtAssign(&st, y);
  return st;
}

static real_T emlrt_marshallIn(const mxArray *b_consecCounter, const char_T
  *identifier)
{
  real_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = b_emlrt_marshallIn(emlrtAlias(b_consecCounter), &thisId);
  emlrtDestroyArray(&b_consecCounter);
  return y;
}

static real_T b_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId)
{
  real_T y;
  y = h_emlrt_marshallIn(emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static slexLawOfLargeNumbersIntegerCounterSysObj c_emlrt_marshallIn(const
  mxArray *b_sysobj, const char_T *identifier)
{
  slexLawOfLargeNumbersIntegerCounterSysObj y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = d_emlrt_marshallIn(emlrtAlias(b_sysobj), &thisId);
  emlrtDestroyArray(&b_sysobj);
  return y;
}

static slexLawOfLargeNumbersIntegerCounterSysObj d_emlrt_marshallIn(const
  mxArray *u, const emlrtMsgIdentifier *parentId)
{
  slexLawOfLargeNumbersIntegerCounterSysObj y;
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[3] = { "isInitialized", "counter",
    "consecCounter" };

  thisId.fParent = parentId;
  emlrtCheckStructR2012b(emlrtRootTLSGlobal, parentId, u, 3, fieldNames, 0U, 0);
  thisId.fIdentifier = "isInitialized";
  y.isInitialized = e_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "isInitialized")), &thisId);
  thisId.fIdentifier = "counter";
  y.counter = b_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "counter")), &thisId);
  thisId.fIdentifier = "consecCounter";
  y.consecCounter = b_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "consecCounter")), &thisId);
  emlrtDestroyArray(&u);
  return y;
}

static int32_T e_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId)
{
  int32_T y;
  y = i_emlrt_marshallIn(emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static boolean_T f_emlrt_marshallIn(const mxArray *b_sysobj_not_empty, const
  char_T *identifier)
{
  boolean_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = g_emlrt_marshallIn(emlrtAlias(b_sysobj_not_empty), &thisId);
  emlrtDestroyArray(&b_sysobj_not_empty);
  return y;
}

static boolean_T g_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId)
{
  boolean_T y;
  y = j_emlrt_marshallIn(emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static void mw__internal__setSimState__fcn(InstanceStruct_z3ClAipuvk2LM0lFTNXD1B
  *moduleInstance, const mxArray *st)
{
  const mxArray *u;
  real_T *consecCounter;
  real_T *counter;
  consecCounter = (real_T *)ssGetDWork(moduleInstance->S, 1U);
  counter = (real_T *)ssGetDWork(moduleInstance->S, 0U);
  u = emlrtAlias(st);
  *consecCounter = emlrt_marshallIn(emlrtAlias(emlrtGetCell(emlrtRootTLSGlobal,
    "consecCounter", u, 0)), "consecCounter");
  *counter = emlrt_marshallIn(emlrtAlias(emlrtGetCell(emlrtRootTLSGlobal,
    "counter", u, 1)), "counter");
  moduleInstance->sysobj = c_emlrt_marshallIn(emlrtAlias(emlrtGetCell
    (emlrtRootTLSGlobal, "sysobj", u, 2)), "sysobj");
  moduleInstance->sysobj_not_empty = f_emlrt_marshallIn(emlrtAlias(emlrtGetCell
    (emlrtRootTLSGlobal, "sysobj_not_empty", u, 3)), "sysobj_not_empty");
  emlrtDestroyArray(&u);
  emlrtDestroyArray(&st);
}

static const mxArray *message(const mxArray *b, const mxArray *c, emlrtMCInfo
  *location)
{
  const mxArray *pArrays[2];
  const mxArray *m9;
  pArrays[0] = b;
  pArrays[1] = c;
  return emlrtCallMATLABR2012b(emlrtRootTLSGlobal, 1, &m9, 2, pArrays, "message",
    true, location);
}

static void error(const mxArray *b, emlrtMCInfo *location)
{
  const mxArray *pArray;
  pArray = b;
  emlrtCallMATLABR2012b(emlrtRootTLSGlobal, 0, NULL, 1, &pArray, "error", true,
                        location);
}

static real_T h_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId)
{
  real_T ret;
  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "double", false, 0U, 0);
  ret = *(real_T *)mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static int32_T i_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId)
{
  int32_T ret;
  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "int32", false, 0U, 0);
  ret = *(int32_T *)mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static boolean_T j_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId)
{
  boolean_T ret;
  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "logical", false, 0U,
    0);
  ret = *mxGetLogicals(src);
  emlrtDestroyArray(&src);
  return ret;
}

/* CGXE Glue Code */
static void mdlOutputs_z3ClAipuvk2LM0lFTNXD1B(SimStruct *S, int_T tid)
{
  InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *moduleInstance;
  moduleInstance = (InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_outputs(moduleInstance);
  CGXERT_LEAVE_CHECK();
}

static void mdlInitialize_z3ClAipuvk2LM0lFTNXD1B(SimStruct *S)
{
  InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *moduleInstance;
  moduleInstance = (InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_initialize(moduleInstance);
  CGXERT_LEAVE_CHECK();
}

static void mdlUpdate_z3ClAipuvk2LM0lFTNXD1B(SimStruct *S, int_T tid)
{
  InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *moduleInstance;
  moduleInstance = (InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_update(moduleInstance);
  CGXERT_LEAVE_CHECK();
}

static mxArray* getSimState_z3ClAipuvk2LM0lFTNXD1B(SimStruct *S)
{
  InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *moduleInstance;
  mxArray* mxSS;
  moduleInstance = (InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  mxSS = (mxArray *) mw__internal__getSimState__fcn(moduleInstance);
  CGXERT_LEAVE_CHECK();
  return mxSS;
}

static void setSimState_z3ClAipuvk2LM0lFTNXD1B(SimStruct *S, const mxArray *ss)
{
  InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *moduleInstance;
  moduleInstance = (InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  mw__internal__setSimState__fcn(moduleInstance, emlrtAlias(ss));
  CGXERT_LEAVE_CHECK();
}

static void mdlTerminate_z3ClAipuvk2LM0lFTNXD1B(SimStruct *S)
{
  InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *moduleInstance;
  moduleInstance = (InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_terminate(moduleInstance);
  CGXERT_LEAVE_CHECK();
  free((void *)moduleInstance);
  ssSetUserData(S, NULL);
}

static void mdlStart_z3ClAipuvk2LM0lFTNXD1B(SimStruct *S)
{
  InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *moduleInstance;
  moduleInstance = (InstanceStruct_z3ClAipuvk2LM0lFTNXD1B *)calloc(1, sizeof
    (InstanceStruct_z3ClAipuvk2LM0lFTNXD1B));
  moduleInstance->S = S;
  ssSetUserData(S, (void *)moduleInstance);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_start(moduleInstance);
  CGXERT_LEAVE_CHECK();

  {
    uint_T options = ssGetOptions(S);
    options |= SS_OPTION_RUNTIME_EXCEPTION_FREE_CODE;
    ssSetOptions(S, options);
  }

  ssSetmdlOutputs(S, mdlOutputs_z3ClAipuvk2LM0lFTNXD1B);
  ssSetmdlInitializeConditions(S, mdlInitialize_z3ClAipuvk2LM0lFTNXD1B);
  ssSetmdlUpdate(S, mdlUpdate_z3ClAipuvk2LM0lFTNXD1B);
  ssSetmdlTerminate(S, mdlTerminate_z3ClAipuvk2LM0lFTNXD1B);
}

static void mdlProcessParameters_z3ClAipuvk2LM0lFTNXD1B(SimStruct *S)
{
}

void method_dispatcher_z3ClAipuvk2LM0lFTNXD1B(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_z3ClAipuvk2LM0lFTNXD1B(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_z3ClAipuvk2LM0lFTNXD1B(S);
    break;

   case SS_CALL_MDL_GET_SIM_STATE:
    *((mxArray**) data) = getSimState_z3ClAipuvk2LM0lFTNXD1B(S);
    break;

   case SS_CALL_MDL_SET_SIM_STATE:
    setSimState_z3ClAipuvk2LM0lFTNXD1B(S, (const mxArray *) data);
    break;

   default:
    /* Unhandled method */
    /*
       sf_mex_error_message("Stateflow Internal Error:\n"
       "Error calling method dispatcher for module: z3ClAipuvk2LM0lFTNXD1B.\n"
       "Can't handle method %d.\n", method);
     */
    break;
  }
}

int autoInfer_dispatcher_z3ClAipuvk2LM0lFTNXD1B(mxArray* plhs[], const char
  * commandName)
{
  if (strcmp(commandName, "NameResolution") == 0) {
    plhs[0] = (mxArray*) mw__internal__name__resolution__fcn();
    return 1;
  }

  if (strcmp(commandName, "AutoInfer") == 0) {
    plhs[0] = (mxArray*) mw__internal__autoInference__fcn();
    return 1;
  }

  return 0;
}

mxArray *cgxe_z3ClAipuvk2LM0lFTNXD1B_BuildInfoUpdate(void)
{
  mxArray * mxBIArgs;
  mxArray * elem_1;
  mxArray * elem_2;
  mxArray * elem_3;
  mxArray * elem_4;
  mxArray * elem_5;
  mxArray * elem_6;
  mxArray * elem_7;
  mxArray * elem_8;
  mxArray * elem_9;
  mxBIArgs = mxCreateCellMatrix(1,3);
  elem_1 = mxCreateCellMatrix(1,6);
  elem_2 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,0,elem_2);
  elem_3 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,1,elem_3);
  elem_4 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,2,elem_4);
  elem_5 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,3,elem_5);
  elem_6 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,4,elem_6);
  elem_7 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,5,elem_7);
  mxSetCell(mxBIArgs,0,elem_1);
  elem_8 = mxCreateCellMatrix(1,0);
  mxSetCell(mxBIArgs,1,elem_8);
  elem_9 = mxCreateCellMatrix(1,0);
  mxSetCell(mxBIArgs,2,elem_9);
  return mxBIArgs;
}
