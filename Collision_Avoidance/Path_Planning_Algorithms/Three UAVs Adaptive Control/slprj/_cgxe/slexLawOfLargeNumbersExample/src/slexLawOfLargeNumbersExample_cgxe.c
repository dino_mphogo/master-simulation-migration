/* Include files */

#include "slexLawOfLargeNumbersExample_cgxe.h"
#include "m_z3ClAipuvk2LM0lFTNXD1B.h"

static unsigned int cgxeModelInitialized = 0;
emlrtContext emlrtContextGlobal = { true, true, EMLRT_VERSION_INFO, NULL, "",
  NULL, false, { 0, 0, 0, 0 }, NULL };

void *emlrtRootTLSGlobal = NULL;
char cgxeRtErrBuf[4096];

/* CGXE Glue Code */
void cgxe_slexLawOfLargeNumbersExample_initializer(void)
{
  if (cgxeModelInitialized == 0) {
    cgxeModelInitialized = 1;
    emlrtRootTLSGlobal = NULL;
    emlrtCreateSimulinkRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1,
      false, 0);
  }
}

void cgxe_slexLawOfLargeNumbersExample_terminator(void)
{
  if (cgxeModelInitialized != 0) {
    cgxeModelInitialized = 0;
    emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
    emlrtRootTLSGlobal = NULL;
  }
}

unsigned int cgxe_slexLawOfLargeNumbersExample_method_dispatcher(SimStruct* S,
  int_T method, void* data)
{
  if (ssGetChecksum0(S) == 101263322 &&
      ssGetChecksum1(S) == 4231397629 &&
      ssGetChecksum2(S) == 3840658636 &&
      ssGetChecksum3(S) == 1797259858) {
    method_dispatcher_z3ClAipuvk2LM0lFTNXD1B(S, method, data);
    return 1;
  }

  return 0;
}

int cgxe_slexLawOfLargeNumbersExample_autoInfer_dispatcher(const mxArray* prhs,
  mxArray* lhs[], const char* commandName)
{
  char sid[64];
  mxGetString(prhs,sid, sizeof(sid)/sizeof(char));
  sid[(sizeof(sid)/sizeof(char)-1)] = '\0';
  if (strcmp(sid, "slexLawOfLargeNumbersExample:71") == 0 ) {
    return autoInfer_dispatcher_z3ClAipuvk2LM0lFTNXD1B(lhs, commandName);
  }

  if (strcmp(sid, "slexLawOfLargeNumbersExample:70") == 0 ) {
    return autoInfer_dispatcher_z3ClAipuvk2LM0lFTNXD1B(lhs, commandName);
  }

  return 0;
}
