/* Include files */

#include <stddef.h>
#include "blas.h"
#include "Three_UAVs_sfun.h"
#include "c14_Three_UAVs.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Three_UAVs_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c14_debug_family_names[19] = { "i", "j", "conflict_status",
  "conflict_time", "nargin", "nargout", "number_of_uavs", "path_track",
  "Fleet_Positions", "uavs_velocities", "path_plan_in", "conflict_state",
  "token_value", "intruder_index", "path_tracker_", "path_plan_out",
  "conflict_state_", "token_value_", "intruder_index_" };

static const char * c14_b_debug_family_names[16] = { "x_dmod_ta", "x_dmod_ra",
  "z_thr_ta", "z_thr_ra", "agl_threshold", "own_altitude", "sensetivity_level",
  "ta", "ra", "z_delt", "x_delt", "nargin", "nargout", "host_position",
  "intruder_position", "conflict_state" };

static const char * c14_c_debug_family_names[13] = { "host_uav_position",
  "intruder_uav_position", "planned_climb_rates_host",
  "planned_climb_rates_intruder", "i", "nargin", "nargout", "uavs_positions",
  "current_velocities", "planned_climb_rates", "time_length", "conflict_state",
  "time_space" };

/* Function Declarations */
static void initialize_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance);
static void initialize_params_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance);
static void enable_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct *chartInstance);
static void disable_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct *chartInstance);
static void c14_update_debugger_state_c14_Three_UAVs
  (SFc14_Three_UAVsInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c14_Three_UAVs
  (SFc14_Three_UAVsInstanceStruct *chartInstance);
static void set_sim_state_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance, const mxArray *c14_st);
static void finalize_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance);
static void sf_gateway_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance);
static void mdl_start_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance);
static void c14_chartstep_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance);
static void initSimStructsc14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c14_machineNumber, uint32_T
  c14_chartNumber, uint32_T c14_instanceNumber);
static const mxArray *c14_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData);
static real_T c14_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_b_intruder_index_, const char_T *c14_identifier);
static real_T c14_b_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct
  *chartInstance, const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId);
static void c14_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData);
static const mxArray *c14_b_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData);
static void c14_c_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_b_conflict_state_, const char_T *c14_identifier, real_T
  c14_y[9]);
static void c14_d_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId, real_T c14_y[9]);
static void c14_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData);
static const mxArray *c14_c_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData);
static void c14_e_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_b_path_plan_out, const char_T *c14_identifier, real_T
  c14_y[18]);
static void c14_f_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId, real_T c14_y[18]);
static void c14_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData);
static const mxArray *c14_d_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData);
static void c14_g_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId, real_T c14_y[3]);
static void c14_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData);
static const mxArray *c14_e_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData);
static void c14_h_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId, real_T c14_y[12]);
static void c14_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData);
static const mxArray *c14_f_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData);
static void c14_i_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId, real_T c14_y[6]);
static void c14_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData);
static const mxArray *c14_g_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData);
static void c14_j_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId, real_T c14_y[6]);
static void c14_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData);
static void c14_info_helper(const mxArray **c14_info);
static const mxArray *c14_emlrt_marshallOut(const char * c14_u);
static const mxArray *c14_b_emlrt_marshallOut(const uint32_T c14_u);
static const mxArray *c14_h_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData);
static int32_T c14_k_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct
  *chartInstance, const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId);
static void c14_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData);
static uint8_T c14_l_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct
  *chartInstance, const mxArray *c14_b_is_active_c14_Three_UAVs, const char_T
  *c14_identifier);
static uint8_T c14_m_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct
  *chartInstance, const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId);
static void init_dsm_address_info(SFc14_Three_UAVsInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc14_Three_UAVsInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    initSimStructsc14_Three_UAVs(chartInstance);
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c14_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c14_is_active_c14_Three_UAVs = 0U;
}

static void initialize_params_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void enable_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c14_update_debugger_state_c14_Three_UAVs
  (SFc14_Three_UAVsInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c14_Three_UAVs
  (SFc14_Three_UAVsInstanceStruct *chartInstance)
{
  const mxArray *c14_st;
  const mxArray *c14_y = NULL;
  const mxArray *c14_b_y = NULL;
  real_T c14_hoistedGlobal;
  real_T c14_u;
  const mxArray *c14_c_y = NULL;
  const mxArray *c14_d_y = NULL;
  real_T c14_b_hoistedGlobal;
  real_T c14_b_u;
  const mxArray *c14_e_y = NULL;
  real_T c14_c_hoistedGlobal;
  real_T c14_c_u;
  const mxArray *c14_f_y = NULL;
  uint8_T c14_d_hoistedGlobal;
  uint8_T c14_d_u;
  const mxArray *c14_g_y = NULL;
  c14_st = NULL;
  c14_st = NULL;
  c14_y = NULL;
  sf_mex_assign(&c14_y, sf_mex_createcellmatrix(6, 1), false);
  c14_b_y = NULL;
  sf_mex_assign(&c14_b_y, sf_mex_create("y", *chartInstance->c14_conflict_state_,
    0, 0U, 1U, 0U, 2, 3, 3), false);
  sf_mex_setcell(c14_y, 0, c14_b_y);
  c14_hoistedGlobal = *chartInstance->c14_intruder_index_;
  c14_u = c14_hoistedGlobal;
  c14_c_y = NULL;
  sf_mex_assign(&c14_c_y, sf_mex_create("y", &c14_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c14_y, 1, c14_c_y);
  c14_d_y = NULL;
  sf_mex_assign(&c14_d_y, sf_mex_create("y", *chartInstance->c14_path_plan_out,
    0, 0U, 1U, 0U, 2, 3, 6), false);
  sf_mex_setcell(c14_y, 2, c14_d_y);
  c14_b_hoistedGlobal = *chartInstance->c14_path_tracker_;
  c14_b_u = c14_b_hoistedGlobal;
  c14_e_y = NULL;
  sf_mex_assign(&c14_e_y, sf_mex_create("y", &c14_b_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c14_y, 3, c14_e_y);
  c14_c_hoistedGlobal = *chartInstance->c14_token_value_;
  c14_c_u = c14_c_hoistedGlobal;
  c14_f_y = NULL;
  sf_mex_assign(&c14_f_y, sf_mex_create("y", &c14_c_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c14_y, 4, c14_f_y);
  c14_d_hoistedGlobal = chartInstance->c14_is_active_c14_Three_UAVs;
  c14_d_u = c14_d_hoistedGlobal;
  c14_g_y = NULL;
  sf_mex_assign(&c14_g_y, sf_mex_create("y", &c14_d_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c14_y, 5, c14_g_y);
  sf_mex_assign(&c14_st, c14_y, false);
  return c14_st;
}

static void set_sim_state_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance, const mxArray *c14_st)
{
  const mxArray *c14_u;
  real_T c14_dv0[9];
  int32_T c14_i0;
  real_T c14_dv1[18];
  int32_T c14_i1;
  chartInstance->c14_doneDoubleBufferReInit = true;
  c14_u = sf_mex_dup(c14_st);
  c14_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "conflict_state_", c14_u, 0)), "conflict_state_", c14_dv0);
  for (c14_i0 = 0; c14_i0 < 9; c14_i0++) {
    (*chartInstance->c14_conflict_state_)[c14_i0] = c14_dv0[c14_i0];
  }

  *chartInstance->c14_intruder_index_ = c14_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("intruder_index_", c14_u, 1)), "intruder_index_");
  c14_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "path_plan_out", c14_u, 2)), "path_plan_out", c14_dv1);
  for (c14_i1 = 0; c14_i1 < 18; c14_i1++) {
    (*chartInstance->c14_path_plan_out)[c14_i1] = c14_dv1[c14_i1];
  }

  *chartInstance->c14_path_tracker_ = c14_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("path_tracker_", c14_u, 3)), "path_tracker_");
  *chartInstance->c14_token_value_ = c14_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("token_value_", c14_u, 4)), "token_value_");
  chartInstance->c14_is_active_c14_Three_UAVs = c14_l_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_active_c14_Three_UAVs", c14_u,
       5)), "is_active_c14_Three_UAVs");
  sf_mex_destroy(&c14_u);
  c14_update_debugger_state_c14_Three_UAVs(chartInstance);
  sf_mex_destroy(&c14_st);
}

static void finalize_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance)
{
  int32_T c14_i2;
  int32_T c14_i3;
  int32_T c14_i4;
  int32_T c14_i5;
  int32_T c14_i6;
  int32_T c14_i7;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 13U, chartInstance->c14_sfEvent);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c14_intruder_index, 7U, 1U, 0U,
                        chartInstance->c14_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c14_token_value, 6U, 1U, 0U,
                        chartInstance->c14_sfEvent, false);
  for (c14_i2 = 0; c14_i2 < 9; c14_i2++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c14_conflict_state)[c14_i2], 5U, 1U,
                          0U, chartInstance->c14_sfEvent, false);
  }

  for (c14_i3 = 0; c14_i3 < 18; c14_i3++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c14_path_plan_in)[c14_i3], 4U, 1U, 0U,
                          chartInstance->c14_sfEvent, false);
  }

  for (c14_i4 = 0; c14_i4 < 9; c14_i4++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c14_uavs_velocities)[c14_i4], 3U, 1U,
                          0U, chartInstance->c14_sfEvent, false);
  }

  for (c14_i5 = 0; c14_i5 < 9; c14_i5++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c14_Fleet_Positions)[c14_i5], 2U, 1U,
                          0U, chartInstance->c14_sfEvent, false);
  }

  _SFD_DATA_RANGE_CHECK(*chartInstance->c14_path_track, 1U, 1U, 0U,
                        chartInstance->c14_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c14_number_of_uavs, 0U, 1U, 0U,
                        chartInstance->c14_sfEvent, false);
  chartInstance->c14_sfEvent = CALL_EVENT;
  c14_chartstep_c14_Three_UAVs(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_Three_UAVsMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c14_path_tracker_, 8U, 1U, 0U,
                        chartInstance->c14_sfEvent, false);
  for (c14_i6 = 0; c14_i6 < 18; c14_i6++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c14_path_plan_out)[c14_i6], 9U, 1U,
                          0U, chartInstance->c14_sfEvent, false);
  }

  for (c14_i7 = 0; c14_i7 < 9; c14_i7++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c14_conflict_state_)[c14_i7], 10U, 1U,
                          0U, chartInstance->c14_sfEvent, false);
  }

  _SFD_DATA_RANGE_CHECK(*chartInstance->c14_token_value_, 11U, 1U, 0U,
                        chartInstance->c14_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c14_intruder_index_, 12U, 1U, 0U,
                        chartInstance->c14_sfEvent, false);
}

static void mdl_start_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c14_chartstep_c14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance)
{
  real_T c14_hoistedGlobal;
  real_T c14_b_hoistedGlobal;
  real_T c14_c_hoistedGlobal;
  real_T c14_d_hoistedGlobal;
  real_T c14_b_number_of_uavs;
  real_T c14_b_path_track;
  int32_T c14_i8;
  real_T c14_b_Fleet_Positions[9];
  int32_T c14_i9;
  real_T c14_b_uavs_velocities[9];
  int32_T c14_i10;
  real_T c14_b_path_plan_in[18];
  int32_T c14_i11;
  real_T c14_b_conflict_state[9];
  real_T c14_b_token_value;
  real_T c14_b_intruder_index;
  uint32_T c14_debug_family_var_map[19];
  real_T c14_i;
  real_T c14_j;
  real_T c14_conflict_status;
  real_T c14_conflict_time;
  real_T c14_nargin = 8.0;
  real_T c14_nargout = 5.0;
  real_T c14_b_path_tracker_;
  real_T c14_b_path_plan_out[18];
  real_T c14_b_conflict_state_[9];
  real_T c14_b_token_value_;
  real_T c14_b_intruder_index_;
  real_T c14_c_number_of_uavs;
  int32_T c14_i12;
  int32_T c14_b_i;
  real_T c14_d_number_of_uavs;
  int32_T c14_i13;
  int32_T c14_b_j;
  int32_T c14_c_i;
  int32_T c14_c_j;
  int32_T c14_i14;
  real_T c14_uavs_positions[6];
  int32_T c14_i15;
  int32_T c14_d_i;
  int32_T c14_d_j;
  int32_T c14_i16;
  real_T c14_current_velocities[6];
  int32_T c14_i17;
  int32_T c14_e_i;
  int32_T c14_e_j;
  int32_T c14_i18;
  real_T c14_planned_climb_rates[12];
  int32_T c14_i19;
  real_T c14_time_length;
  uint32_T c14_b_debug_family_var_map[13];
  real_T c14_host_uav_position[3];
  real_T c14_intruder_uav_position[3];
  real_T c14_planned_climb_rates_host[6];
  real_T c14_planned_climb_rates_intruder[6];
  real_T c14_f_i;
  real_T c14_b_nargin = 4.0;
  real_T c14_b_nargout = 2.0;
  real_T c14_b_conflict_status;
  real_T c14_b_conflict_time;
  int32_T c14_i20;
  int32_T c14_i21;
  int32_T c14_i22;
  int32_T c14_i23;
  int32_T c14_i24;
  int32_T c14_i25;
  int32_T c14_i26;
  int32_T c14_i27;
  int32_T c14_g_i;
  int32_T c14_i28;
  real_T c14_host_position[3];
  int32_T c14_i29;
  real_T c14_intruder_position[3];
  uint32_T c14_c_debug_family_var_map[16];
  real_T c14_x_dmod_ta;
  real_T c14_x_dmod_ra;
  real_T c14_z_thr_ta;
  real_T c14_z_thr_ra;
  real_T c14_agl_threshold;
  real_T c14_own_altitude;
  real_T c14_sensetivity_level;
  real_T c14_ta;
  real_T c14_ra;
  real_T c14_z_delt;
  real_T c14_x_delt;
  real_T c14_c_nargin = 2.0;
  real_T c14_c_nargout = 1.0;
  real_T c14_d0;
  real_T c14_x;
  real_T c14_b_x;
  real_T c14_d1;
  real_T c14_c_x;
  real_T c14_d_x;
  real_T c14_d2;
  real_T c14_d3;
  real_T c14_u;
  const mxArray *c14_y = NULL;
  int32_T c14_i30;
  int32_T c14_i31;
  int32_T c14_i32;
  int32_T c14_i33;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  int32_T exitg1;
  int32_T exitg2;
  boolean_T guard11 = false;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 13U, chartInstance->c14_sfEvent);
  c14_hoistedGlobal = *chartInstance->c14_number_of_uavs;
  c14_b_hoistedGlobal = *chartInstance->c14_path_track;
  c14_c_hoistedGlobal = *chartInstance->c14_token_value;
  c14_d_hoistedGlobal = *chartInstance->c14_intruder_index;
  c14_b_number_of_uavs = c14_hoistedGlobal;
  c14_b_path_track = c14_b_hoistedGlobal;
  for (c14_i8 = 0; c14_i8 < 9; c14_i8++) {
    c14_b_Fleet_Positions[c14_i8] = (*chartInstance->c14_Fleet_Positions)[c14_i8];
  }

  for (c14_i9 = 0; c14_i9 < 9; c14_i9++) {
    c14_b_uavs_velocities[c14_i9] = (*chartInstance->c14_uavs_velocities)[c14_i9];
  }

  for (c14_i10 = 0; c14_i10 < 18; c14_i10++) {
    c14_b_path_plan_in[c14_i10] = (*chartInstance->c14_path_plan_in)[c14_i10];
  }

  for (c14_i11 = 0; c14_i11 < 9; c14_i11++) {
    c14_b_conflict_state[c14_i11] = (*chartInstance->c14_conflict_state)[c14_i11];
  }

  c14_b_token_value = c14_c_hoistedGlobal;
  c14_b_intruder_index = c14_d_hoistedGlobal;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 19U, 19U, c14_debug_family_names,
    c14_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_i, 0U, c14_sf_marshallOut,
    c14_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_j, 1U, c14_sf_marshallOut,
    c14_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_conflict_status, 2U,
    c14_sf_marshallOut, c14_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_conflict_time, 3U,
    c14_sf_marshallOut, c14_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_nargin, 4U, c14_sf_marshallOut,
    c14_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_nargout, 5U, c14_sf_marshallOut,
    c14_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c14_b_number_of_uavs, 6U, c14_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c14_b_path_track, 7U, c14_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(c14_b_Fleet_Positions, 8U, c14_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(c14_b_uavs_velocities, 9U, c14_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(c14_b_path_plan_in, 10U, c14_c_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(c14_b_conflict_state, 11U, c14_b_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c14_b_token_value, 12U, c14_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c14_b_intruder_index, 13U, c14_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_b_path_tracker_, 14U,
    c14_sf_marshallOut, c14_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c14_b_path_plan_out, 15U,
    c14_c_sf_marshallOut, c14_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c14_b_conflict_state_, 16U,
    c14_b_sf_marshallOut, c14_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_b_token_value_, 17U,
    c14_sf_marshallOut, c14_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_b_intruder_index_, 18U,
    c14_sf_marshallOut, c14_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 7);
  if (CV_EML_IF(0, 1, 0, CV_RELATIONAL_EVAL(4U, 0U, 0, c14_b_path_track, 0.0, -1,
        0U, c14_b_path_track == 0.0))) {
    _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 9);
    if (CV_EML_IF(0, 1, 1, CV_RELATIONAL_EVAL(4U, 0U, 1, c14_b_token_value,
          c14_b_intruder_index, -1, 0U, c14_b_token_value ==
          c14_b_intruder_index))) {
      _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 10);
      c14_b_intruder_index++;
    }

    _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 13);
    guard2 = false;
    if (CV_EML_COND(0, 1, 0, CV_RELATIONAL_EVAL(4U, 0U, 2, c14_b_token_value,
          c14_b_number_of_uavs, -1, 0U, c14_b_token_value ==
          c14_b_number_of_uavs))) {
      guard2 = true;
    } else if (CV_EML_COND(0, 1, 1, CV_RELATIONAL_EVAL(4U, 0U, 3,
                 c14_b_token_value, c14_b_number_of_uavs, -1, 4U,
                 c14_b_token_value > c14_b_number_of_uavs))) {
      guard2 = true;
    } else {
      CV_EML_MCDC(0, 1, 0, false);
      CV_EML_IF(0, 1, 2, false);
    }

    if (guard2 == true) {
      CV_EML_MCDC(0, 1, 0, true);
      CV_EML_IF(0, 1, 2, true);
      _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 14);
      if (CV_EML_IF(0, 1, 3, CV_RELATIONAL_EVAL(4U, 0U, 4, c14_b_token_value,
            3.0, -1, 0U, c14_b_token_value == 3.0))) {
        _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 15);
        c14_b_intruder_index = 1.0;
      } else {
        _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 17);
        c14_b_token_value = 1.0;
        _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 18);
        c14_b_intruder_index = 2.0;
      }
    }

    _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 21);
    c14_c_number_of_uavs = c14_b_number_of_uavs;
    c14_i12 = (int32_T)c14_c_number_of_uavs;
    _SFD_FOR_LOOP_VECTOR_CHECK(1.0, 1.0, c14_c_number_of_uavs, mxDOUBLE_CLASS,
      c14_i12);
    c14_i = 1.0;
    c14_b_i = 0;
    while (c14_b_i <= c14_i12 - 1) {
      c14_i = 1.0 + (real_T)c14_b_i;
      CV_EML_FOR(0, 1, 0, 1);
      _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 22);
      c14_d_number_of_uavs = c14_b_number_of_uavs;
      c14_i13 = (int32_T)c14_d_number_of_uavs;
      _SFD_FOR_LOOP_VECTOR_CHECK(1.0, 1.0, c14_d_number_of_uavs, mxDOUBLE_CLASS,
        c14_i13);
      c14_j = 1.0;
      c14_b_j = 0;
      do {
        exitg1 = 0;
        if (c14_b_j <= c14_i13 - 1) {
          c14_j = 1.0 + (real_T)c14_b_j;
          CV_EML_FOR(0, 1, 1, 1);
          _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 23);
          guard11 = false;
          if (CV_EML_IF(0, 1, 4, CV_RELATIONAL_EVAL(4U, 0U, 5, c14_i, c14_j, -1,
                1U, c14_i != c14_j))) {
            _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 24);
            c14_c_i = _SFD_EML_ARRAY_BOUNDS_CHECK("Fleet_Positions", (int32_T)
              _SFD_INTEGER_CHECK("i", c14_i), 1, 3, 1, 0) - 1;
            c14_c_j = _SFD_EML_ARRAY_BOUNDS_CHECK("Fleet_Positions", (int32_T)
              _SFD_INTEGER_CHECK("j", c14_j), 1, 3, 1, 0) - 1;
            for (c14_i14 = 0; c14_i14 < 3; c14_i14++) {
              c14_uavs_positions[c14_i14 << 1] = c14_b_Fleet_Positions[c14_c_i +
                3 * c14_i14];
            }

            for (c14_i15 = 0; c14_i15 < 3; c14_i15++) {
              c14_uavs_positions[1 + (c14_i15 << 1)] =
                c14_b_Fleet_Positions[c14_c_j + 3 * c14_i15];
            }

            c14_d_i = _SFD_EML_ARRAY_BOUNDS_CHECK("uavs_velocities", (int32_T)
              _SFD_INTEGER_CHECK("i", c14_i), 1, 3, 1, 0) - 1;
            c14_d_j = _SFD_EML_ARRAY_BOUNDS_CHECK("uavs_velocities", (int32_T)
              _SFD_INTEGER_CHECK("j", c14_j), 1, 3, 1, 0) - 1;
            for (c14_i16 = 0; c14_i16 < 3; c14_i16++) {
              c14_current_velocities[c14_i16 << 1] =
                c14_b_uavs_velocities[c14_d_i + 3 * c14_i16];
            }

            for (c14_i17 = 0; c14_i17 < 3; c14_i17++) {
              c14_current_velocities[1 + (c14_i17 << 1)] =
                c14_b_uavs_velocities[c14_d_j + 3 * c14_i17];
            }

            c14_e_i = _SFD_EML_ARRAY_BOUNDS_CHECK("path_plan_in", (int32_T)
              _SFD_INTEGER_CHECK("i", c14_i), 1, 3, 1, 0) - 1;
            c14_e_j = _SFD_EML_ARRAY_BOUNDS_CHECK("path_plan_in", (int32_T)
              _SFD_INTEGER_CHECK("j", c14_j), 1, 3, 1, 0) - 1;
            for (c14_i18 = 0; c14_i18 < 6; c14_i18++) {
              c14_planned_climb_rates[c14_i18 << 1] = c14_b_path_plan_in[c14_e_i
                + 3 * c14_i18];
            }

            for (c14_i19 = 0; c14_i19 < 6; c14_i19++) {
              c14_planned_climb_rates[1 + (c14_i19 << 1)] =
                c14_b_path_plan_in[c14_e_j + 3 * c14_i19];
            }

            c14_time_length = 5.0;
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 13U, 13U, c14_c_debug_family_names,
              c14_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c14_host_uav_position, 0U,
              c14_d_sf_marshallOut, c14_d_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c14_intruder_uav_position, 1U,
              c14_d_sf_marshallOut, c14_d_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c14_planned_climb_rates_host,
              2U, c14_g_sf_marshallOut, c14_g_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE
              (c14_planned_climb_rates_intruder, 3U, c14_g_sf_marshallOut,
               c14_g_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_f_i, 4U,
              c14_sf_marshallOut, c14_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_b_nargin, 5U,
              c14_sf_marshallOut, c14_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_b_nargout, 6U,
              c14_sf_marshallOut, c14_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c14_uavs_positions, 7U,
              c14_f_sf_marshallOut, c14_f_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c14_current_velocities, 8U,
              c14_f_sf_marshallOut, c14_f_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c14_planned_climb_rates, 9U,
              c14_e_sf_marshallOut, c14_e_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_time_length, 10U,
              c14_sf_marshallOut, c14_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_b_conflict_status, 11U,
              c14_sf_marshallOut, c14_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_b_conflict_time, 12U,
              c14_sf_marshallOut, c14_sf_marshallIn);
            CV_SCRIPT_FCN(0, 0);
            _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 2);
            c14_b_conflict_status = 0.0;
            _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 3);
            c14_i20 = 0;
            for (c14_i21 = 0; c14_i21 < 3; c14_i21++) {
              c14_host_uav_position[c14_i21] = c14_uavs_positions[c14_i20];
              c14_i20 += 2;
            }

            _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 4);
            c14_i22 = 0;
            for (c14_i23 = 0; c14_i23 < 3; c14_i23++) {
              c14_intruder_uav_position[c14_i23] = c14_uavs_positions[c14_i22 +
                1];
              c14_i22 += 2;
            }

            _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 5);
            c14_i24 = 0;
            for (c14_i25 = 0; c14_i25 < 6; c14_i25++) {
              c14_planned_climb_rates_host[c14_i25] =
                c14_planned_climb_rates[c14_i24];
              c14_i24 += 2;
            }

            _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 6);
            c14_i26 = 0;
            for (c14_i27 = 0; c14_i27 < 6; c14_i27++) {
              c14_planned_climb_rates_intruder[c14_i27] =
                c14_planned_climb_rates[c14_i26 + 1];
              c14_i26 += 2;
            }

            _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 7);
            c14_b_conflict_time = -1.0;
            _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 8);
            c14_f_i = 1.0;
            c14_g_i = 0;
            do {
              exitg2 = 0;
              if (c14_g_i < 6) {
                c14_f_i = 1.0 + (real_T)c14_g_i;
                CV_SCRIPT_FOR(0, 0, 1);
                _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 9);
                c14_host_uav_position[2] +=
                  c14_planned_climb_rates_host[_SFD_EML_ARRAY_BOUNDS_CHECK(
                  "planned_climb_rates_host", (int32_T)_SFD_INTEGER_CHECK("i",
                  c14_f_i), 1, 6, 1, 0) - 1] * c14_time_length;
                _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 10);
                c14_host_uav_position[0] += c14_current_velocities[0] *
                  c14_time_length;
                _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 11);
                c14_host_uav_position[1] += c14_current_velocities[2] *
                  c14_time_length;
                _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 13);
                c14_intruder_uav_position[2] +=
                  c14_planned_climb_rates_intruder[_SFD_EML_ARRAY_BOUNDS_CHECK(
                  "planned_climb_rates_intruder", (int32_T)_SFD_INTEGER_CHECK(
                  "i", c14_f_i), 1, 6, 1, 0) - 1] * c14_time_length;
                _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 14);
                c14_intruder_uav_position[0] += c14_current_velocities[1] *
                  c14_time_length;
                _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 15);
                c14_intruder_uav_position[1] += c14_current_velocities[3] *
                  c14_time_length;
                _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 16);
                for (c14_i28 = 0; c14_i28 < 3; c14_i28++) {
                  c14_host_position[c14_i28] = c14_host_uav_position[c14_i28];
                }

                for (c14_i29 = 0; c14_i29 < 3; c14_i29++) {
                  c14_intruder_position[c14_i29] =
                    c14_intruder_uav_position[c14_i29];
                }

                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 16U, 16U,
                  c14_b_debug_family_names, c14_c_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_x_dmod_ta, 0U,
                  c14_sf_marshallOut, c14_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_x_dmod_ra, 1U,
                  c14_sf_marshallOut, c14_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_z_thr_ta, 2U,
                  c14_sf_marshallOut, c14_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_z_thr_ra, 3U,
                  c14_sf_marshallOut, c14_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_agl_threshold, 4U,
                  c14_sf_marshallOut, c14_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_own_altitude, 5U,
                  c14_sf_marshallOut, c14_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_sensetivity_level, 6U,
                  c14_sf_marshallOut, c14_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_ta, 7U,
                  c14_sf_marshallOut, c14_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_ra, 8U,
                  c14_sf_marshallOut, c14_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_z_delt, 9U,
                  c14_sf_marshallOut, c14_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_x_delt, 10U,
                  c14_sf_marshallOut, c14_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_c_nargin, 11U,
                  c14_sf_marshallOut, c14_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_c_nargout, 12U,
                  c14_sf_marshallOut, c14_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c14_host_position, 13U,
                  c14_d_sf_marshallOut, c14_d_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c14_intruder_position, 14U,
                  c14_d_sf_marshallOut, c14_d_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c14_d0, 15U,
                  c14_sf_marshallOut, c14_sf_marshallIn);
                CV_SCRIPT_FCN(1, 0);
                _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, 2);
                c14_x_dmod_ta = 3038.06;
                _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, 3);
                c14_x_dmod_ra = 2126.642;
                _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, 5);
                c14_z_thr_ta = 850.0;
                _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, 6);
                c14_z_thr_ra = 650.0;
                _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, 8);
                c14_agl_threshold = 650.0;
                _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, 9);
                c14_own_altitude = c14_host_position[2];
                _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, 10);
                c14_sensetivity_level = 4.0;
                _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, 11);
                c14_ta = 30.0;
                _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, 12);
                c14_ra = 20.0;
                _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, 13);
                c14_z_delt = c14_host_position[2] - c14_intruder_position[2];
                _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, 14);
                c14_x_delt = c14_host_position[0] - c14_intruder_position[0];
                _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, 15);
                c14_x = c14_z_delt;
                c14_b_x = c14_x;
                c14_d1 = muDoubleScalarAbs(c14_b_x);
                guard1 = false;
                if (CV_SCRIPT_COND(1, 0, CV_RELATIONAL_EVAL(14U, 1U, 0, c14_d1,
                      c14_z_thr_ta, -1, 3U, c14_d1 <= c14_z_thr_ta))) {
                  c14_c_x = c14_x_delt;
                  c14_d_x = c14_c_x;
                  c14_d2 = muDoubleScalarAbs(c14_d_x);
                  if (CV_SCRIPT_COND(1, 1, CV_RELATIONAL_EVAL(14U, 1U, 1, c14_d2,
                        c14_x_dmod_ta, -1, 3U, c14_d2 <= c14_x_dmod_ta))) {
                    CV_SCRIPT_MCDC(1, 0, true);
                    CV_SCRIPT_IF(1, 0, true);
                    _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, 16);
                    c14_d0 = 1.0;
                  } else {
                    guard1 = true;
                  }
                } else {
                  guard1 = true;
                }

                if (guard1 == true) {
                  CV_SCRIPT_MCDC(1, 0, false);
                  CV_SCRIPT_IF(1, 0, false);
                  _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, 19);
                  c14_d0 = 0.0;
                }

                _SFD_SCRIPT_CALL(1U, chartInstance->c14_sfEvent, -19);
                _SFD_SYMBOL_SCOPE_POP();
                if (CV_SCRIPT_IF(0, 0, CV_RELATIONAL_EVAL(14U, 0U, 0, c14_d0,
                      0.0, -1, 1U, c14_d0 != 0.0))) {
                  _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 17);
                  c14_b_conflict_status = 1.0;
                  _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 18);
                  c14_b_conflict_time = c14_f_i * 10.0;
                  _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, 19);
                  exitg2 = 1;
                } else {
                  c14_g_i++;
                  _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
                }
              } else {
                CV_SCRIPT_FOR(0, 0, 0);
                exitg2 = 1;
              }
            } while (exitg2 == 0);

            _SFD_SCRIPT_CALL(0U, chartInstance->c14_sfEvent, -19);
            _SFD_SYMBOL_SCOPE_POP();
            c14_conflict_status = c14_b_conflict_status;
            c14_conflict_time = c14_b_conflict_time;
            _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 25);
            c14_b_conflict_state[(_SFD_EML_ARRAY_BOUNDS_CHECK("conflict_state",
              (int32_T)_SFD_INTEGER_CHECK("i", c14_i), 1, 3, 1, 0) + 3 *
                                  (_SFD_EML_ARRAY_BOUNDS_CHECK("conflict_state",
              (int32_T)_SFD_INTEGER_CHECK("j", c14_j), 1, 3, 2, 0) - 1)) - 1] =
              c14_conflict_status;
            _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 26);
            c14_d3 = c14_b_conflict_state[(_SFD_EML_ARRAY_BOUNDS_CHECK(
              "conflict_state", (int32_T)_SFD_INTEGER_CHECK("i", c14_i), 1, 3, 1,
              0) + 3 * (_SFD_EML_ARRAY_BOUNDS_CHECK("conflict_state", (int32_T)
              _SFD_INTEGER_CHECK("j", c14_j), 1, 3, 2, 0) - 1)) - 1];
            if (CV_EML_IF(0, 1, 5, CV_RELATIONAL_EVAL(4U, 0U, 6, c14_d3, 1.0, -1,
                  0U, c14_d3 == 1.0))) {
              _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 27);
              c14_b_conflict_state[(_SFD_EML_ARRAY_BOUNDS_CHECK("conflict_state",
                (int32_T)_SFD_INTEGER_CHECK("j", c14_j), 1, 3, 1, 0) + 3 *
                                    (_SFD_EML_ARRAY_BOUNDS_CHECK(
                "conflict_state", (int32_T)_SFD_INTEGER_CHECK("i", c14_i), 1, 3,
                2, 0) - 1)) - 1] = c14_conflict_status;
              _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 28);
              c14_b_token_value = c14_i;
              _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 29);
              c14_b_intruder_index = c14_j;
              _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 30);
              exitg1 = 1;
            } else {
              guard11 = true;
            }
          } else {
            guard11 = true;
          }

          if (guard11 == true) {
            c14_b_j++;
            _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
          }
        } else {
          CV_EML_FOR(0, 1, 1, 0);
          exitg1 = 1;
        }
      } while (exitg1 == 0);

      c14_b_i++;
      _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
    }

    CV_EML_FOR(0, 1, 0, 0);
  } else {
    _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 37);
    if (CV_EML_IF(0, 1, 6, CV_RELATIONAL_EVAL(4U, 0U, 7, c14_b_token_value,
          c14_b_intruder_index, -1, 0U, c14_b_token_value ==
          c14_b_intruder_index))) {
      _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 38);
      sf_mex_printf("%s =\\n", "token_value");
      c14_u = c14_b_token_value;
      c14_y = NULL;
      sf_mex_assign(&c14_y, sf_mex_create("y", &c14_u, 0, 0U, 0U, 0U, 0), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c14_y);
    }
  }

  _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 41);
  for (c14_i30 = 0; c14_i30 < 9; c14_i30++) {
    c14_b_conflict_state_[c14_i30] = c14_b_conflict_state[c14_i30];
  }

  _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 43);
  c14_b_token_value_ = c14_b_token_value;
  _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 44);
  c14_b_intruder_index_ = c14_b_intruder_index;
  _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 45);
  for (c14_i31 = 0; c14_i31 < 18; c14_i31++) {
    c14_b_path_plan_out[c14_i31] = c14_b_path_plan_in[c14_i31];
  }

  _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, 46);
  c14_b_path_tracker_ = c14_b_path_track;
  _SFD_EML_CALL(0U, chartInstance->c14_sfEvent, -46);
  _SFD_SYMBOL_SCOPE_POP();
  *chartInstance->c14_path_tracker_ = c14_b_path_tracker_;
  for (c14_i32 = 0; c14_i32 < 18; c14_i32++) {
    (*chartInstance->c14_path_plan_out)[c14_i32] = c14_b_path_plan_out[c14_i32];
  }

  for (c14_i33 = 0; c14_i33 < 9; c14_i33++) {
    (*chartInstance->c14_conflict_state_)[c14_i33] =
      c14_b_conflict_state_[c14_i33];
  }

  *chartInstance->c14_token_value_ = c14_b_token_value_;
  *chartInstance->c14_intruder_index_ = c14_b_intruder_index_;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 13U, chartInstance->c14_sfEvent);
}

static void initSimStructsc14_Three_UAVs(SFc14_Three_UAVsInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c14_machineNumber, uint32_T
  c14_chartNumber, uint32_T c14_instanceNumber)
{
  (void)c14_machineNumber;
  _SFD_SCRIPT_TRANSLATION(c14_chartNumber, c14_instanceNumber, 0U,
    sf_debug_get_script_id(
    "/home/dinorego/Documents/Multiple_UAV_collision_Avoidance/Collision_Avoidance/Path_Planning_Algorithms/Three UAVs/conflict_predi"
    "ction.m"));
  _SFD_SCRIPT_TRANSLATION(c14_chartNumber, c14_instanceNumber, 1U,
    sf_debug_get_script_id(
    "/home/dinorego/Documents/Multiple_UAV_collision_Avoidance/Collision_Avoidance/Path_Planning_Algorithms/Three UAVs/conflict_detec"
    "tion.m"));
}

static const mxArray *c14_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData)
{
  const mxArray *c14_mxArrayOutData = NULL;
  real_T c14_u;
  const mxArray *c14_y = NULL;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_mxArrayOutData = NULL;
  c14_u = *(real_T *)c14_inData;
  c14_y = NULL;
  sf_mex_assign(&c14_y, sf_mex_create("y", &c14_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c14_mxArrayOutData, c14_y, false);
  return c14_mxArrayOutData;
}

static real_T c14_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_b_intruder_index_, const char_T *c14_identifier)
{
  real_T c14_y;
  emlrtMsgIdentifier c14_thisId;
  c14_thisId.fIdentifier = c14_identifier;
  c14_thisId.fParent = NULL;
  c14_y = c14_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c14_b_intruder_index_),
    &c14_thisId);
  sf_mex_destroy(&c14_b_intruder_index_);
  return c14_y;
}

static real_T c14_b_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct
  *chartInstance, const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId)
{
  real_T c14_y;
  real_T c14_d4;
  (void)chartInstance;
  sf_mex_import(c14_parentId, sf_mex_dup(c14_u), &c14_d4, 1, 0, 0U, 0, 0U, 0);
  c14_y = c14_d4;
  sf_mex_destroy(&c14_u);
  return c14_y;
}

static void c14_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData)
{
  const mxArray *c14_b_intruder_index_;
  const char_T *c14_identifier;
  emlrtMsgIdentifier c14_thisId;
  real_T c14_y;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_b_intruder_index_ = sf_mex_dup(c14_mxArrayInData);
  c14_identifier = c14_varName;
  c14_thisId.fIdentifier = c14_identifier;
  c14_thisId.fParent = NULL;
  c14_y = c14_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c14_b_intruder_index_),
    &c14_thisId);
  sf_mex_destroy(&c14_b_intruder_index_);
  *(real_T *)c14_outData = c14_y;
  sf_mex_destroy(&c14_mxArrayInData);
}

static const mxArray *c14_b_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData)
{
  const mxArray *c14_mxArrayOutData = NULL;
  int32_T c14_i34;
  int32_T c14_i35;
  int32_T c14_i36;
  real_T c14_u[9];
  const mxArray *c14_y = NULL;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_mxArrayOutData = NULL;
  c14_i34 = 0;
  for (c14_i35 = 0; c14_i35 < 3; c14_i35++) {
    for (c14_i36 = 0; c14_i36 < 3; c14_i36++) {
      c14_u[c14_i36 + c14_i34] = (*(real_T (*)[9])c14_inData)[c14_i36 + c14_i34];
    }

    c14_i34 += 3;
  }

  c14_y = NULL;
  sf_mex_assign(&c14_y, sf_mex_create("y", c14_u, 0, 0U, 1U, 0U, 2, 3, 3), false);
  sf_mex_assign(&c14_mxArrayOutData, c14_y, false);
  return c14_mxArrayOutData;
}

static void c14_c_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_b_conflict_state_, const char_T *c14_identifier, real_T
  c14_y[9])
{
  emlrtMsgIdentifier c14_thisId;
  c14_thisId.fIdentifier = c14_identifier;
  c14_thisId.fParent = NULL;
  c14_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c14_b_conflict_state_),
    &c14_thisId, c14_y);
  sf_mex_destroy(&c14_b_conflict_state_);
}

static void c14_d_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId, real_T c14_y[9])
{
  real_T c14_dv2[9];
  int32_T c14_i37;
  (void)chartInstance;
  sf_mex_import(c14_parentId, sf_mex_dup(c14_u), c14_dv2, 1, 0, 0U, 1, 0U, 2, 3,
                3);
  for (c14_i37 = 0; c14_i37 < 9; c14_i37++) {
    c14_y[c14_i37] = c14_dv2[c14_i37];
  }

  sf_mex_destroy(&c14_u);
}

static void c14_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData)
{
  const mxArray *c14_b_conflict_state_;
  const char_T *c14_identifier;
  emlrtMsgIdentifier c14_thisId;
  real_T c14_y[9];
  int32_T c14_i38;
  int32_T c14_i39;
  int32_T c14_i40;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_b_conflict_state_ = sf_mex_dup(c14_mxArrayInData);
  c14_identifier = c14_varName;
  c14_thisId.fIdentifier = c14_identifier;
  c14_thisId.fParent = NULL;
  c14_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c14_b_conflict_state_),
    &c14_thisId, c14_y);
  sf_mex_destroy(&c14_b_conflict_state_);
  c14_i38 = 0;
  for (c14_i39 = 0; c14_i39 < 3; c14_i39++) {
    for (c14_i40 = 0; c14_i40 < 3; c14_i40++) {
      (*(real_T (*)[9])c14_outData)[c14_i40 + c14_i38] = c14_y[c14_i40 + c14_i38];
    }

    c14_i38 += 3;
  }

  sf_mex_destroy(&c14_mxArrayInData);
}

static const mxArray *c14_c_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData)
{
  const mxArray *c14_mxArrayOutData = NULL;
  int32_T c14_i41;
  int32_T c14_i42;
  int32_T c14_i43;
  real_T c14_u[18];
  const mxArray *c14_y = NULL;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_mxArrayOutData = NULL;
  c14_i41 = 0;
  for (c14_i42 = 0; c14_i42 < 6; c14_i42++) {
    for (c14_i43 = 0; c14_i43 < 3; c14_i43++) {
      c14_u[c14_i43 + c14_i41] = (*(real_T (*)[18])c14_inData)[c14_i43 + c14_i41];
    }

    c14_i41 += 3;
  }

  c14_y = NULL;
  sf_mex_assign(&c14_y, sf_mex_create("y", c14_u, 0, 0U, 1U, 0U, 2, 3, 6), false);
  sf_mex_assign(&c14_mxArrayOutData, c14_y, false);
  return c14_mxArrayOutData;
}

static void c14_e_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_b_path_plan_out, const char_T *c14_identifier, real_T
  c14_y[18])
{
  emlrtMsgIdentifier c14_thisId;
  c14_thisId.fIdentifier = c14_identifier;
  c14_thisId.fParent = NULL;
  c14_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c14_b_path_plan_out),
    &c14_thisId, c14_y);
  sf_mex_destroy(&c14_b_path_plan_out);
}

static void c14_f_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId, real_T c14_y[18])
{
  real_T c14_dv3[18];
  int32_T c14_i44;
  (void)chartInstance;
  sf_mex_import(c14_parentId, sf_mex_dup(c14_u), c14_dv3, 1, 0, 0U, 1, 0U, 2, 3,
                6);
  for (c14_i44 = 0; c14_i44 < 18; c14_i44++) {
    c14_y[c14_i44] = c14_dv3[c14_i44];
  }

  sf_mex_destroy(&c14_u);
}

static void c14_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData)
{
  const mxArray *c14_b_path_plan_out;
  const char_T *c14_identifier;
  emlrtMsgIdentifier c14_thisId;
  real_T c14_y[18];
  int32_T c14_i45;
  int32_T c14_i46;
  int32_T c14_i47;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_b_path_plan_out = sf_mex_dup(c14_mxArrayInData);
  c14_identifier = c14_varName;
  c14_thisId.fIdentifier = c14_identifier;
  c14_thisId.fParent = NULL;
  c14_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c14_b_path_plan_out),
    &c14_thisId, c14_y);
  sf_mex_destroy(&c14_b_path_plan_out);
  c14_i45 = 0;
  for (c14_i46 = 0; c14_i46 < 6; c14_i46++) {
    for (c14_i47 = 0; c14_i47 < 3; c14_i47++) {
      (*(real_T (*)[18])c14_outData)[c14_i47 + c14_i45] = c14_y[c14_i47 +
        c14_i45];
    }

    c14_i45 += 3;
  }

  sf_mex_destroy(&c14_mxArrayInData);
}

static const mxArray *c14_d_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData)
{
  const mxArray *c14_mxArrayOutData = NULL;
  int32_T c14_i48;
  real_T c14_u[3];
  const mxArray *c14_y = NULL;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_mxArrayOutData = NULL;
  for (c14_i48 = 0; c14_i48 < 3; c14_i48++) {
    c14_u[c14_i48] = (*(real_T (*)[3])c14_inData)[c14_i48];
  }

  c14_y = NULL;
  sf_mex_assign(&c14_y, sf_mex_create("y", c14_u, 0, 0U, 1U, 0U, 2, 1, 3), false);
  sf_mex_assign(&c14_mxArrayOutData, c14_y, false);
  return c14_mxArrayOutData;
}

static void c14_g_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId, real_T c14_y[3])
{
  real_T c14_dv4[3];
  int32_T c14_i49;
  (void)chartInstance;
  sf_mex_import(c14_parentId, sf_mex_dup(c14_u), c14_dv4, 1, 0, 0U, 1, 0U, 2, 1,
                3);
  for (c14_i49 = 0; c14_i49 < 3; c14_i49++) {
    c14_y[c14_i49] = c14_dv4[c14_i49];
  }

  sf_mex_destroy(&c14_u);
}

static void c14_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData)
{
  const mxArray *c14_intruder_position;
  const char_T *c14_identifier;
  emlrtMsgIdentifier c14_thisId;
  real_T c14_y[3];
  int32_T c14_i50;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_intruder_position = sf_mex_dup(c14_mxArrayInData);
  c14_identifier = c14_varName;
  c14_thisId.fIdentifier = c14_identifier;
  c14_thisId.fParent = NULL;
  c14_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c14_intruder_position),
    &c14_thisId, c14_y);
  sf_mex_destroy(&c14_intruder_position);
  for (c14_i50 = 0; c14_i50 < 3; c14_i50++) {
    (*(real_T (*)[3])c14_outData)[c14_i50] = c14_y[c14_i50];
  }

  sf_mex_destroy(&c14_mxArrayInData);
}

static const mxArray *c14_e_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData)
{
  const mxArray *c14_mxArrayOutData = NULL;
  int32_T c14_i51;
  int32_T c14_i52;
  int32_T c14_i53;
  real_T c14_u[12];
  const mxArray *c14_y = NULL;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_mxArrayOutData = NULL;
  c14_i51 = 0;
  for (c14_i52 = 0; c14_i52 < 6; c14_i52++) {
    for (c14_i53 = 0; c14_i53 < 2; c14_i53++) {
      c14_u[c14_i53 + c14_i51] = (*(real_T (*)[12])c14_inData)[c14_i53 + c14_i51];
    }

    c14_i51 += 2;
  }

  c14_y = NULL;
  sf_mex_assign(&c14_y, sf_mex_create("y", c14_u, 0, 0U, 1U, 0U, 2, 2, 6), false);
  sf_mex_assign(&c14_mxArrayOutData, c14_y, false);
  return c14_mxArrayOutData;
}

static void c14_h_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId, real_T c14_y[12])
{
  real_T c14_dv5[12];
  int32_T c14_i54;
  (void)chartInstance;
  sf_mex_import(c14_parentId, sf_mex_dup(c14_u), c14_dv5, 1, 0, 0U, 1, 0U, 2, 2,
                6);
  for (c14_i54 = 0; c14_i54 < 12; c14_i54++) {
    c14_y[c14_i54] = c14_dv5[c14_i54];
  }

  sf_mex_destroy(&c14_u);
}

static void c14_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData)
{
  const mxArray *c14_planned_climb_rates;
  const char_T *c14_identifier;
  emlrtMsgIdentifier c14_thisId;
  real_T c14_y[12];
  int32_T c14_i55;
  int32_T c14_i56;
  int32_T c14_i57;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_planned_climb_rates = sf_mex_dup(c14_mxArrayInData);
  c14_identifier = c14_varName;
  c14_thisId.fIdentifier = c14_identifier;
  c14_thisId.fParent = NULL;
  c14_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c14_planned_climb_rates),
    &c14_thisId, c14_y);
  sf_mex_destroy(&c14_planned_climb_rates);
  c14_i55 = 0;
  for (c14_i56 = 0; c14_i56 < 6; c14_i56++) {
    for (c14_i57 = 0; c14_i57 < 2; c14_i57++) {
      (*(real_T (*)[12])c14_outData)[c14_i57 + c14_i55] = c14_y[c14_i57 +
        c14_i55];
    }

    c14_i55 += 2;
  }

  sf_mex_destroy(&c14_mxArrayInData);
}

static const mxArray *c14_f_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData)
{
  const mxArray *c14_mxArrayOutData = NULL;
  int32_T c14_i58;
  int32_T c14_i59;
  int32_T c14_i60;
  real_T c14_u[6];
  const mxArray *c14_y = NULL;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_mxArrayOutData = NULL;
  c14_i58 = 0;
  for (c14_i59 = 0; c14_i59 < 3; c14_i59++) {
    for (c14_i60 = 0; c14_i60 < 2; c14_i60++) {
      c14_u[c14_i60 + c14_i58] = (*(real_T (*)[6])c14_inData)[c14_i60 + c14_i58];
    }

    c14_i58 += 2;
  }

  c14_y = NULL;
  sf_mex_assign(&c14_y, sf_mex_create("y", c14_u, 0, 0U, 1U, 0U, 2, 2, 3), false);
  sf_mex_assign(&c14_mxArrayOutData, c14_y, false);
  return c14_mxArrayOutData;
}

static void c14_i_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId, real_T c14_y[6])
{
  real_T c14_dv6[6];
  int32_T c14_i61;
  (void)chartInstance;
  sf_mex_import(c14_parentId, sf_mex_dup(c14_u), c14_dv6, 1, 0, 0U, 1, 0U, 2, 2,
                3);
  for (c14_i61 = 0; c14_i61 < 6; c14_i61++) {
    c14_y[c14_i61] = c14_dv6[c14_i61];
  }

  sf_mex_destroy(&c14_u);
}

static void c14_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData)
{
  const mxArray *c14_current_velocities;
  const char_T *c14_identifier;
  emlrtMsgIdentifier c14_thisId;
  real_T c14_y[6];
  int32_T c14_i62;
  int32_T c14_i63;
  int32_T c14_i64;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_current_velocities = sf_mex_dup(c14_mxArrayInData);
  c14_identifier = c14_varName;
  c14_thisId.fIdentifier = c14_identifier;
  c14_thisId.fParent = NULL;
  c14_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c14_current_velocities),
    &c14_thisId, c14_y);
  sf_mex_destroy(&c14_current_velocities);
  c14_i62 = 0;
  for (c14_i63 = 0; c14_i63 < 3; c14_i63++) {
    for (c14_i64 = 0; c14_i64 < 2; c14_i64++) {
      (*(real_T (*)[6])c14_outData)[c14_i64 + c14_i62] = c14_y[c14_i64 + c14_i62];
    }

    c14_i62 += 2;
  }

  sf_mex_destroy(&c14_mxArrayInData);
}

static const mxArray *c14_g_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData)
{
  const mxArray *c14_mxArrayOutData = NULL;
  int32_T c14_i65;
  real_T c14_u[6];
  const mxArray *c14_y = NULL;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_mxArrayOutData = NULL;
  for (c14_i65 = 0; c14_i65 < 6; c14_i65++) {
    c14_u[c14_i65] = (*(real_T (*)[6])c14_inData)[c14_i65];
  }

  c14_y = NULL;
  sf_mex_assign(&c14_y, sf_mex_create("y", c14_u, 0, 0U, 1U, 0U, 2, 1, 6), false);
  sf_mex_assign(&c14_mxArrayOutData, c14_y, false);
  return c14_mxArrayOutData;
}

static void c14_j_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct *chartInstance,
  const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId, real_T c14_y[6])
{
  real_T c14_dv7[6];
  int32_T c14_i66;
  (void)chartInstance;
  sf_mex_import(c14_parentId, sf_mex_dup(c14_u), c14_dv7, 1, 0, 0U, 1, 0U, 2, 1,
                6);
  for (c14_i66 = 0; c14_i66 < 6; c14_i66++) {
    c14_y[c14_i66] = c14_dv7[c14_i66];
  }

  sf_mex_destroy(&c14_u);
}

static void c14_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData)
{
  const mxArray *c14_planned_climb_rates_intruder;
  const char_T *c14_identifier;
  emlrtMsgIdentifier c14_thisId;
  real_T c14_y[6];
  int32_T c14_i67;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_planned_climb_rates_intruder = sf_mex_dup(c14_mxArrayInData);
  c14_identifier = c14_varName;
  c14_thisId.fIdentifier = c14_identifier;
  c14_thisId.fParent = NULL;
  c14_j_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c14_planned_climb_rates_intruder), &c14_thisId, c14_y);
  sf_mex_destroy(&c14_planned_climb_rates_intruder);
  for (c14_i67 = 0; c14_i67 < 6; c14_i67++) {
    (*(real_T (*)[6])c14_outData)[c14_i67] = c14_y[c14_i67];
  }

  sf_mex_destroy(&c14_mxArrayInData);
}

const mxArray *sf_c14_Three_UAVs_get_eml_resolved_functions_info(void)
{
  const mxArray *c14_nameCaptureInfo = NULL;
  c14_nameCaptureInfo = NULL;
  sf_mex_assign(&c14_nameCaptureInfo, sf_mex_createstruct("structure", 2, 2, 1),
                false);
  c14_info_helper(&c14_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c14_nameCaptureInfo);
  return c14_nameCaptureInfo;
}

static void c14_info_helper(const mxArray **c14_info)
{
  const mxArray *c14_rhs0 = NULL;
  const mxArray *c14_lhs0 = NULL;
  const mxArray *c14_rhs1 = NULL;
  const mxArray *c14_lhs1 = NULL;
  sf_mex_addfield(*c14_info, c14_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c14_info, c14_emlrt_marshallOut("conflict_prediction"),
                  "name", "name", 0);
  sf_mex_addfield(*c14_info, c14_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c14_info, c14_emlrt_marshallOut(
    "[E]/home/dinorego/Documents/Multiple_UAV_collision_Avoidance/Collision_Avoidance/Path_Planning_Algorithms/Three UAVs/conflict_pr"
    "ediction.m"), "resolved", "resolved", 0);
  sf_mex_addfield(*c14_info, c14_b_emlrt_marshallOut(1507983178U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c14_info, c14_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c14_info, c14_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c14_info, c14_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c14_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c14_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c14_info, sf_mex_duplicatearraysafe(&c14_rhs0), "rhs", "rhs",
                  0);
  sf_mex_addfield(*c14_info, sf_mex_duplicatearraysafe(&c14_lhs0), "lhs", "lhs",
                  0);
  sf_mex_addfield(*c14_info, c14_emlrt_marshallOut(
    "[E]/home/dinorego/Documents/Multiple_UAV_collision_Avoidance/Collision_Avoidance/Path_Planning_Algorithms/Three UAVs/conflict_pr"
    "ediction.m"), "context", "context", 1);
  sf_mex_addfield(*c14_info, c14_emlrt_marshallOut("conflict_detection"), "name",
                  "name", 1);
  sf_mex_addfield(*c14_info, c14_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c14_info, c14_emlrt_marshallOut(
    "[E]/home/dinorego/Documents/Multiple_UAV_collision_Avoidance/Collision_Avoidance/Path_Planning_Algorithms/Three UAVs/conflict_de"
    "tection.m"), "resolved", "resolved", 1);
  sf_mex_addfield(*c14_info, c14_b_emlrt_marshallOut(1508076952U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c14_info, c14_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c14_info, c14_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c14_info, c14_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c14_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c14_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c14_info, sf_mex_duplicatearraysafe(&c14_rhs1), "rhs", "rhs",
                  1);
  sf_mex_addfield(*c14_info, sf_mex_duplicatearraysafe(&c14_lhs1), "lhs", "lhs",
                  1);
  sf_mex_destroy(&c14_rhs0);
  sf_mex_destroy(&c14_lhs0);
  sf_mex_destroy(&c14_rhs1);
  sf_mex_destroy(&c14_lhs1);
}

static const mxArray *c14_emlrt_marshallOut(const char * c14_u)
{
  const mxArray *c14_y = NULL;
  c14_y = NULL;
  sf_mex_assign(&c14_y, sf_mex_create("y", c14_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c14_u)), false);
  return c14_y;
}

static const mxArray *c14_b_emlrt_marshallOut(const uint32_T c14_u)
{
  const mxArray *c14_y = NULL;
  c14_y = NULL;
  sf_mex_assign(&c14_y, sf_mex_create("y", &c14_u, 7, 0U, 0U, 0U, 0), false);
  return c14_y;
}

static const mxArray *c14_h_sf_marshallOut(void *chartInstanceVoid, void
  *c14_inData)
{
  const mxArray *c14_mxArrayOutData = NULL;
  int32_T c14_u;
  const mxArray *c14_y = NULL;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_mxArrayOutData = NULL;
  c14_u = *(int32_T *)c14_inData;
  c14_y = NULL;
  sf_mex_assign(&c14_y, sf_mex_create("y", &c14_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c14_mxArrayOutData, c14_y, false);
  return c14_mxArrayOutData;
}

static int32_T c14_k_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct
  *chartInstance, const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId)
{
  int32_T c14_y;
  int32_T c14_i68;
  (void)chartInstance;
  sf_mex_import(c14_parentId, sf_mex_dup(c14_u), &c14_i68, 1, 6, 0U, 0, 0U, 0);
  c14_y = c14_i68;
  sf_mex_destroy(&c14_u);
  return c14_y;
}

static void c14_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c14_mxArrayInData, const char_T *c14_varName, void *c14_outData)
{
  const mxArray *c14_b_sfEvent;
  const char_T *c14_identifier;
  emlrtMsgIdentifier c14_thisId;
  int32_T c14_y;
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)chartInstanceVoid;
  c14_b_sfEvent = sf_mex_dup(c14_mxArrayInData);
  c14_identifier = c14_varName;
  c14_thisId.fIdentifier = c14_identifier;
  c14_thisId.fParent = NULL;
  c14_y = c14_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c14_b_sfEvent),
    &c14_thisId);
  sf_mex_destroy(&c14_b_sfEvent);
  *(int32_T *)c14_outData = c14_y;
  sf_mex_destroy(&c14_mxArrayInData);
}

static uint8_T c14_l_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct
  *chartInstance, const mxArray *c14_b_is_active_c14_Three_UAVs, const char_T
  *c14_identifier)
{
  uint8_T c14_y;
  emlrtMsgIdentifier c14_thisId;
  c14_thisId.fIdentifier = c14_identifier;
  c14_thisId.fParent = NULL;
  c14_y = c14_m_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c14_b_is_active_c14_Three_UAVs), &c14_thisId);
  sf_mex_destroy(&c14_b_is_active_c14_Three_UAVs);
  return c14_y;
}

static uint8_T c14_m_emlrt_marshallIn(SFc14_Three_UAVsInstanceStruct
  *chartInstance, const mxArray *c14_u, const emlrtMsgIdentifier *c14_parentId)
{
  uint8_T c14_y;
  uint8_T c14_u0;
  (void)chartInstance;
  sf_mex_import(c14_parentId, sf_mex_dup(c14_u), &c14_u0, 1, 3, 0U, 0, 0U, 0);
  c14_y = c14_u0;
  sf_mex_destroy(&c14_u);
  return c14_y;
}

static void init_dsm_address_info(SFc14_Three_UAVsInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc14_Three_UAVsInstanceStruct
  *chartInstance)
{
  chartInstance->c14_number_of_uavs = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c14_path_tracker_ = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c14_path_track = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c14_Fleet_Positions = (real_T (*)[9])
    ssGetInputPortSignal_wrapper(chartInstance->S, 2);
  chartInstance->c14_uavs_velocities = (real_T (*)[9])
    ssGetInputPortSignal_wrapper(chartInstance->S, 3);
  chartInstance->c14_path_plan_out = (real_T (*)[18])
    ssGetOutputPortSignal_wrapper(chartInstance->S, 2);
  chartInstance->c14_path_plan_in = (real_T (*)[18])ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c14_conflict_state = (real_T (*)[9])
    ssGetInputPortSignal_wrapper(chartInstance->S, 5);
  chartInstance->c14_conflict_state_ = (real_T (*)[9])
    ssGetOutputPortSignal_wrapper(chartInstance->S, 3);
  chartInstance->c14_token_value = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 6);
  chartInstance->c14_token_value_ = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c14_intruder_index_ = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 5);
  chartInstance->c14_intruder_index = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 7);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c14_Three_UAVs_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(915589842U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2882860851U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(4188314949U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2307088300U);
}

mxArray* sf_c14_Three_UAVs_get_post_codegen_info(void);
mxArray *sf_c14_Three_UAVs_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("f5PCYzoI364NLZMnSJbngC");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,8,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(3);
      pr[1] = (double)(3);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(3);
      pr[1] = (double)(3);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(3);
      pr[1] = (double)(6);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(3);
      pr[1] = (double)(3);
      mxSetField(mxData,5,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,5,"type",mxType);
    }

    mxSetField(mxData,5,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,6,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,6,"type",mxType);
    }

    mxSetField(mxData,6,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,7,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,7,"type",mxType);
    }

    mxSetField(mxData,7,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,5,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(3);
      pr[1] = (double)(6);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(3);
      pr[1] = (double)(3);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c14_Three_UAVs_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c14_Three_UAVs_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c14_Three_UAVs_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("pre");
  mxArray *fallbackReason = mxCreateString("hasBreakpoints");
  mxArray *hiddenFallbackType = mxCreateString("none");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c14_Three_UAVs_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c14_Three_UAVs_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c14_Three_UAVs(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x6'type','srcId','name','auxInfo'{{M[1],M[8],T\"conflict_state_\",},{M[1],M[11],T\"intruder_index_\",},{M[1],M[5],T\"path_plan_out\",},{M[1],M[18],T\"path_tracker_\",},{M[1],M[10],T\"token_value_\",},{M[8],M[0],T\"is_active_c14_Three_UAVs\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 6, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c14_Three_UAVs_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc14_Three_UAVsInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc14_Three_UAVsInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Three_UAVsMachineNumber_,
           14,
           1,
           1,
           0,
           13,
           0,
           0,
           0,
           0,
           2,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Three_UAVsMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Three_UAVsMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Three_UAVsMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"number_of_uavs");
          _SFD_SET_DATA_PROPS(1,1,1,0,"path_track");
          _SFD_SET_DATA_PROPS(2,1,1,0,"Fleet_Positions");
          _SFD_SET_DATA_PROPS(3,1,1,0,"uavs_velocities");
          _SFD_SET_DATA_PROPS(4,1,1,0,"path_plan_in");
          _SFD_SET_DATA_PROPS(5,1,1,0,"conflict_state");
          _SFD_SET_DATA_PROPS(6,1,1,0,"token_value");
          _SFD_SET_DATA_PROPS(7,1,1,0,"intruder_index");
          _SFD_SET_DATA_PROPS(8,2,0,1,"path_tracker_");
          _SFD_SET_DATA_PROPS(9,2,0,1,"path_plan_out");
          _SFD_SET_DATA_PROPS(10,2,0,1,"conflict_state_");
          _SFD_SET_DATA_PROPS(11,2,0,1,"token_value_");
          _SFD_SET_DATA_PROPS(12,2,0,1,"intruder_index_");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,7,0,0,0,2,0,2,1);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,1665);
        _SFD_CV_INIT_EML_IF(0,1,0,434,453,1432,1506);
        _SFD_CV_INIT_EML_IF(0,1,1,463,496,-1,558);
        _SFD_CV_INIT_EML_IF(0,1,2,563,628,-1,805);
        _SFD_CV_INIT_EML_IF(0,1,3,637,679,720,797);
        _SFD_CV_INIT_EML_IF(0,1,4,880,890,-1,1411);
        _SFD_CV_INIT_EML_IF(0,1,5,1165,1193,-1,1395);
        _SFD_CV_INIT_EML_IF(0,1,6,1441,1474,-1,1502);
        _SFD_CV_INIT_EML_FOR(0,1,0,810,835,1431);
        _SFD_CV_INIT_EML_FOR(0,1,1,843,868,1423);

        {
          static int condStart[] = { 566, 599 };

          static int condEnd[] = { 595, 627 };

          static int pfixExpr[] = { 0, 1, -2 };

          _SFD_CV_INIT_EML_MCDC(0,1,0,566,627,2,0,&(condStart[0]),&(condEnd[0]),
                                3,&(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(0,1,0,437,452,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,1,466,495,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,2,566,595,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,3,599,627,-1,4);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,4,640,678,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,5,883,889,-1,1);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,6,1168,1192,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,7,1444,1473,-1,0);
        _SFD_CV_INIT_SCRIPT(0,1,0,1,0,0,0,1,0,0,0);
        _SFD_CV_INIT_SCRIPT_FCN(0,0,"conflict_prediction",0,-1,1163);
        _SFD_CV_INIT_SCRIPT_IF(0,0,1022,1090,-1,-2);
        _SFD_CV_INIT_SCRIPT_FOR(0,0,428,471,1156);
        _SFD_CV_INIT_SCRIPT_RELATIONAL(0,0,1025,1089,-1,1);
        _SFD_CV_INIT_SCRIPT(1,1,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_SCRIPT_FCN(1,0,"conflict_detection",0,-1,576);
        _SFD_CV_INIT_SCRIPT_IF(1,0,454,509,538,570);

        {
          static int condStart[] = { 457, 484 };

          static int condEnd[] = { 480, 508 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_SCRIPT_MCDC(1,0,457,508,2,0,&(condStart[0]),&(condEnd[0]),
            3,&(pfixExpr[0]));
        }

        _SFD_CV_INIT_SCRIPT_RELATIONAL(1,0,457,480,-1,3);
        _SFD_CV_INIT_SCRIPT_RELATIONAL(1,1,484,508,-1,3);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c14_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c14_sf_marshallOut,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[2];
          dimVector[0]= 3;
          dimVector[1]= 3;
          _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c14_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 3;
          dimVector[1]= 3;
          _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c14_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 3;
          dimVector[1]= 6;
          _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c14_c_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 3;
          dimVector[1]= 3;
          _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c14_b_sf_marshallOut,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c14_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c14_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(8,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c14_sf_marshallOut,(MexInFcnForType)c14_sf_marshallIn);

        {
          unsigned int dimVector[2];
          dimVector[0]= 3;
          dimVector[1]= 6;
          _SFD_SET_DATA_COMPILED_PROPS(9,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c14_c_sf_marshallOut,(MexInFcnForType)
            c14_c_sf_marshallIn);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 3;
          dimVector[1]= 3;
          _SFD_SET_DATA_COMPILED_PROPS(10,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c14_b_sf_marshallOut,(MexInFcnForType)
            c14_b_sf_marshallIn);
        }

        _SFD_SET_DATA_COMPILED_PROPS(11,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c14_sf_marshallOut,(MexInFcnForType)c14_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(12,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c14_sf_marshallOut,(MexInFcnForType)c14_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Three_UAVsMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc14_Three_UAVsInstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc14_Three_UAVsInstanceStruct *) chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c14_number_of_uavs);
        _SFD_SET_DATA_VALUE_PTR(8U, chartInstance->c14_path_tracker_);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c14_path_track);
        _SFD_SET_DATA_VALUE_PTR(2U, *chartInstance->c14_Fleet_Positions);
        _SFD_SET_DATA_VALUE_PTR(3U, *chartInstance->c14_uavs_velocities);
        _SFD_SET_DATA_VALUE_PTR(9U, *chartInstance->c14_path_plan_out);
        _SFD_SET_DATA_VALUE_PTR(4U, *chartInstance->c14_path_plan_in);
        _SFD_SET_DATA_VALUE_PTR(5U, *chartInstance->c14_conflict_state);
        _SFD_SET_DATA_VALUE_PTR(10U, *chartInstance->c14_conflict_state_);
        _SFD_SET_DATA_VALUE_PTR(6U, chartInstance->c14_token_value);
        _SFD_SET_DATA_VALUE_PTR(11U, chartInstance->c14_token_value_);
        _SFD_SET_DATA_VALUE_PTR(12U, chartInstance->c14_intruder_index_);
        _SFD_SET_DATA_VALUE_PTR(7U, chartInstance->c14_intruder_index);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "spAHC0GUM7Dp6a1OvEFGI1";
}

static void sf_opaque_initialize_c14_Three_UAVs(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc14_Three_UAVsInstanceStruct*) chartInstanceVar)
    ->S,0);
  initialize_params_c14_Three_UAVs((SFc14_Three_UAVsInstanceStruct*)
    chartInstanceVar);
  initialize_c14_Three_UAVs((SFc14_Three_UAVsInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c14_Three_UAVs(void *chartInstanceVar)
{
  enable_c14_Three_UAVs((SFc14_Three_UAVsInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c14_Three_UAVs(void *chartInstanceVar)
{
  disable_c14_Three_UAVs((SFc14_Three_UAVsInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c14_Three_UAVs(void *chartInstanceVar)
{
  sf_gateway_c14_Three_UAVs((SFc14_Three_UAVsInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c14_Three_UAVs(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c14_Three_UAVs((SFc14_Three_UAVsInstanceStruct*)
    chartInfo->chartInstance);         /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c14_Three_UAVs(SimStruct* S, const mxArray
  *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c14_Three_UAVs((SFc14_Three_UAVsInstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c14_Three_UAVs(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc14_Three_UAVsInstanceStruct*) chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Three_UAVs_optimization_info();
    }

    finalize_c14_Three_UAVs((SFc14_Three_UAVsInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc14_Three_UAVs((SFc14_Three_UAVsInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c14_Three_UAVs(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c14_Three_UAVs((SFc14_Three_UAVsInstanceStruct*)
      (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c14_Three_UAVs(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_Three_UAVs_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      14);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,14,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,14);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 4, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 5, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 6, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 7, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,14,8);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,14,5);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=5; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 8; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,14);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(622229514U));
  ssSetChecksum1(S,(1421323834U));
  ssSetChecksum2(S,(1786651695U));
  ssSetChecksum3(S,(4148073462U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c14_Three_UAVs(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c14_Three_UAVs(SimStruct *S)
{
  SFc14_Three_UAVsInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc14_Three_UAVsInstanceStruct *)utMalloc(sizeof
    (SFc14_Three_UAVsInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc14_Three_UAVsInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c14_Three_UAVs;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c14_Three_UAVs;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c14_Three_UAVs;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c14_Three_UAVs;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c14_Three_UAVs;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c14_Three_UAVs;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c14_Three_UAVs;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c14_Three_UAVs;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c14_Three_UAVs;
  chartInstance->chartInfo.mdlStart = mdlStart_c14_Three_UAVs;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c14_Three_UAVs;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c14_Three_UAVs_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c14_Three_UAVs(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c14_Three_UAVs(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c14_Three_UAVs(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c14_Three_UAVs_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
