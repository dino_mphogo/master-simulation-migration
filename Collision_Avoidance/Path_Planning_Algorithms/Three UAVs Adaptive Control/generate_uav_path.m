function path = generate_uav_path(velocity_vect,position,uav_plan, t)
	x_i = position(1);
	y_i = position(2);
	z_i = position(3);
	path = [x_i y_i z_i];
	x_dot = velocity_vect(1);
	y_dot = velocity_vect(2);
	z_dot = 0;
	for index = 1:size(uav_plan,2)
		z_dot = uav_plan(index);	
		x_i = x_i + x_dot*t;
		y_i = y_i + y_dot*t;
		z_i = z_i + z_dot*t;
		path(size(path,1)+1, :) = [x_i y_i z_i];
	end
	
end
	
