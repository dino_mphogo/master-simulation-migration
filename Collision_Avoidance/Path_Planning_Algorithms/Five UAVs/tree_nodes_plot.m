figure
grid on
hold on
simulation_time = datenum(simulation_time);
title('Number of Tree Nodes per Planning State UAVs')
xlabel('Time(s)')
ylabel('Number of Tree Nodes')
	uav_1 = plot(simulation_time(:, 1), number_of_nodes(:,1)); uav_1_plot_label = 'UAV 1';
	scatter(simulation_time(:, 1), number_of_nodes(:,1))
	uav_2 = plot(simulation_time(:, 2), number_of_nodes(:,2)); uav_2_plot_label = 'UAV 2';
	scatter(simulation_time(:, 2), number_of_nodes(:,2))
	uav_3 =  plot(simulation_time(:, 3), number_of_nodes(:,3)); uav_3_plot_label = 'UAV 3';
	scatter(simulation_time(:, 3), number_of_nodes(:,3))
	uav_4 = plot(simulation_time(:, 4), number_of_nodes(:,4)) ; uav_4_plot_label = 'UAV 4';
	scatter(simulation_time(:, 4), number_of_nodes(:,4))
	uav_5 = plot(simulation_time(:, 5), number_of_nodes(:,5)) ; uav_5_plot_label = 'UAV 5';
	scatter(simulation_time(:, 5), number_of_nodes(:,5))
	legend([uav_1; uav_2; uav_3; uav_4; uav_5], [uav_1_plot_label; uav_2_plot_label; uav_3_plot_label; uav_4_plot_label; uav_5_plot_label])

