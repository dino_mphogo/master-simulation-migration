load('five_uavs_range_monte_carlo.mat')
load('five_uavs_sim_time.mat')
sim_time = five_uavs_sim_time.data;
uavs_data = five_uavs_range_monte_carlo.data;
figure
grid on
hold on
uav_1 = uavs_data(1,:,:);
uav_1 = reshape(uav_1, [ size(uav_1,2) size(uav_1,3) ])';
range = 1:size(uav_1,1);
plot(sim_time,uav_1(range,2),'r')
plot(sim_time,uav_1(range,3),'g')
plot(sim_time,uav_1(range,4),'b')
plot(sim_time,uav_1(range,5),'c')
title('UAV 1 and Intruders Relative Time-Distance')
xlabel('Time(s)')
ylabel('Distance(feet)')
legend('UAV 2','UAV 3','UAV 4','UAV 5')
filename = 'uav_1_and_intruders.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));

hold off
figure
hold on
grid on
uav_2 = uavs_data(2,:,:);
uav_2 = reshape(uav_2, [ size(uav_2,2) size(uav_2,3) ])';
plot(sim_time,uav_2(:,1),'r')
plot(sim_time,uav_2(:,3),'g')
plot(sim_time,uav_2(:,4),'b')
plot(sim_time,uav_2(:,5),'c')
title('UAV 2 and Intruders Relative Time-Distance')
xlabel('Time(s)')
ylabel('Distance(feet)')
legend( 'UAV 1', 'UAV 3','UAV 4','UAV 5')
filename = 'uav_2_and_intruders.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));

hold off
figure
hold on
grid on
uav_3 = uavs_data(3,:,:);
uav_3 = reshape(uav_3, [ size(uav_3,2) size(uav_3,3) ])';
plot(sim_time,uav_3(:,1),'r')
plot(sim_time,uav_3(:,2),'g')
plot(sim_time,uav_3(:,4),'b')
plot(sim_time,uav_3(:,5),'c')
title('UAV 3 and Intruders Relative Time-Distance')
xlabel('Time(s)')
ylabel('Distance(feet)')
legend('UAV 1','UAV 2','UAV 4','UAV 5')
filename = 'uav_3_and_intruders.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));

hold off
figure
hold on
grid on
uav_4 = uavs_data(4,:,:);
uav_4 = reshape(uav_4, [ size(uav_4,2) size(uav_4,3) ])';
plot(sim_time,uav_4(:,1),'r')
plot(sim_time,uav_4(:,2),'g')
plot(sim_time,uav_4(:,3),'b')
plot(sim_time,uav_4(:,5),'c')
title('UAV 4 and Intruders Relative Time-Distance')
xlabel('Time(s)')
ylabel('Distance(feet)')
legend('UAV 1', 'UAV 2','UAV 3','UAV 5')
filename = 'uav_4_and_intruders.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));



hold off
figure
hold on
grid on
uav_5 = uavs_data(5,:,:);
uav_5 = reshape(uav_5, [ size(uav_5,2) size(uav_5,3) ])';
plot(sim_time,uav_4(:,1),'r')
plot(sim_time,uav_5(:,2),'g')
plot(sim_time,uav_5(:,3),'b')
plot(sim_time,uav_5(:,4),'c')
title('UAV 5 and Intruders Relative Time-Distance')
xlabel('Time(s)')
ylabel('Distance(feet)')
legend('UAV 1', 'UAV 2','UAV 3','UAV 4')
filename = 'uav_5_and_intruders.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));

