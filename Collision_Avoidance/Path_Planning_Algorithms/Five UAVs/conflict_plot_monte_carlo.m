load('five_uavs_conflict_status_carlo.mat')
load('five_uavs_sim_time.mat')
sim_time = five_uavs_sim_time.data(2:end,:);
uavs_data = five_uavs_conflict_status_carlo.data;
figure
grid on
hold on
uav_1 = uavs_data(1,:,:);
uav_1 = reshape(uav_1, [ size(uav_1,2) size(uav_1,3) ])';
conflict_counts =  uav_1(:,2) + uav_1(:,3) + uav_1(:,4) + uav_1(:,5) ;
conflict_counts = conflict_counts(2:end,:);
g= area(sim_time,conflict_counts)
g.EdgeColor = 'red';
g.FaceColor = 'red';
title('UAV 1 Conflicts-Time')
xlabel('Time(s)')
ylabel('Conflicts Count')
area_conflict = trapz(2:size(uav_1,1),conflict_counts);
legend(['UAV 1 Conflict Area = ', num2str(area_conflict)])
filename = 'uav_1_conflict_counts.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off


figure
grid on
hold on
uav_2 = uavs_data(2,:,:);
uav_2 = reshape(uav_2, [ size(uav_2,2) size(uav_2,3) ])';
conflict_counts =  uav_2(:,1) + uav_2(:,3) + uav_2(:,4) + uav_2(:,5) ;
conflict_counts = conflict_counts(2:end,:);

g= area(sim_time,conflict_counts)
g.EdgeColor = 'red';
g.FaceColor = 'red';
title('UAV 2 Conflicts-Time')
xlabel('Time(s)')
ylabel('Conflicts Count')
area_conflict = trapz(2:size(uav_2,1),conflict_counts);
legend(['UAV 2 Conflict Area = ', num2str(area_conflict)])
filename = 'uav_2_conflict_counts.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off

figure
grid on
hold on
uav_3 = uavs_data(3,:,:);
uav_3 = reshape(uav_3, [ size(uav_3,2) size(uav_3,3) ])';
conflict_counts =  uav_3(:,1) + uav_3(:,2) + uav_3(:,4) + uav_3(:,5) ;
conflict_counts = conflict_counts(2:end,:);

g= area(sim_time,conflict_counts)
g.EdgeColor = 'red';
g.FaceColor = 'red';
title('UAV 3 Conflicts-Time')
xlabel('Time(s)')
ylabel('Conflicts Count')
area_conflict = trapz(2:size(uav_3,1),conflict_counts);
legend(['UAV 3 Conflict Area = ', num2str(area_conflict)])
filename = 'uav_3_conflict_counts.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off


figure
grid on
hold on
uav_4 = uavs_data(4,:,:);
uav_4 = reshape(uav_4, [ size(uav_4,2) size(uav_4,3) ])';
conflict_counts =  uav_4(:,1) + uav_4(:,2) + uav_4(:,3) + uav_4(:,5) ;
conflict_counts = conflict_counts(2:end,:);
g= area(sim_time,conflict_counts)
g.EdgeColor = 'red';
g.FaceColor = 'red';
title('UAV 4 Conflicts-Time')
xlabel('Time(s)')
ylabel('Conflicts Count')
area_conflict = trapz(2:size(uav_4,1),conflict_counts);
legend(['UAV 4 Conflict Area = ', num2str(area_conflict)])
filename = 'uav_4_conflict_counts.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off

figure
grid on
hold on
uav_5 = uavs_data(5,:,:);
uav_5 = reshape(uav_5, [ size(uav_5,2) size(uav_5,3) ])';
conflict_counts =  uav_5(:,1) + uav_5(:,2) + uav_5(:,3) + uav_5(:,4);
conflict_counts = conflict_counts(2:end,:);
g= area(sim_time,conflict_counts)
g.EdgeColor = 'red';
g.FaceColor = 'red';
title('UAV 5 Conflicts-Time')
xlabel('Time(s)')
ylabel('Conflicts Count')
area_conflict = trapz(2:size(uav_5,1),conflict_counts);
legend(['UAV 5 Conflict Area = ', num2str(area_conflict)])
filename = 'uav_5_conflict_counts.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off
