planning_uavs_indicators = csvread('historical_path_planning_uavs.txt');

for row_count = 2:size(planning_uavs_indicators,1)
	uav_id = planning_uavs_indicators(row_count,1);
	uav_sampled_data = csvread(['sampled_data_uav_',num2str(uav_id),'_', num2str(planning_uavs_indicators(row_count,2)) ,'.txt']);
	uav_conflict_nodes = csvread(['sampled_conflict_nodes_', num2str(uav_id), '_', num2str(planning_uavs_indicators(row_count,2)),'.txt']);
	uav_path = csvread(['path_plan_uav_',num2str(uav_id),'_',num2str(planning_uavs_indicators(row_count,2)) ,'.txt']);
	cost_nodes = csvread(['cost_nodes_',num2str(uav_id),'_',num2str(planning_uavs_indicators(row_count,2)) ,'.txt']);
	%uav_path_optimization(cost_nodes(:,1), cost_nodes(:,2), cost_nodes(:,3), ['uav_',num2str(uav_id),'_path_optimization.jpg']);
	%AltitudeCostsPlot(cost_nodes(:,2), cost_nodes(:,3), ['uav_',num2str(uav_id),'_altitude_optimization.jpg']);
	fig_1 = figure;
	hold on
	
	scatter(uav_sampled_data(:,1),uav_sampled_data(:,3),'b', 'filled');
	if uav_conflict_nodes(1,1) ~= -1
		scatter(uav_conflict_nodes(:,1),uav_conflict_nodes(:,3),'r', 'filled');
	end
	
	
	plot(uav_path(:,1),uav_path(:,3),'g');
	xlabel('range x(feet)'); % x-axis label
	ylabel('Altitude h(feet)'); % y-axis label
	legend('Conflict Free Nodes','Conflict Nodes','Chosen UAV Path');
	title(['UAV ', num2str(uav_id) ' position sampled data points in x-axis and y-axis']);
	saveas(fig_1,['uav_', num2str(uav_id) , '_sampled_path_',num2str(row_count-1),'.jpg']);
	hold off
	
	
end


