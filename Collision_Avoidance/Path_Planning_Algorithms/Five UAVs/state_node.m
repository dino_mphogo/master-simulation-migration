classdef state_node
   properties
      x_k;
      u_k_1;
      x_k_star;
      child_states;
      parent;
      conflict_state =  0;
      control_efforts_cost = 0;
      terminal_cost = 0;
      t_delt = 10;
      time_to_conflict;
      tree_index;
      input_space = [-3000 -1500 0 1500  3000]/60; % ft/min converted to ft/s with a division by 60
   end
   methods
      function obj = state_node(x_k,u_k_1,parent_index,tau)
      		obj.u_k_1 = u_k_1;
      		obj.parent = parent_index;
      		obj.x_k = x_k;
      		obj.time_to_conflict = tau;
      		
      		
      end
      function parent = get_parent(obj)
         parent = obj.parent;
      end
      
      function children = get_children(obj,node_input)
         children = obj.child_states;
      end
      
      function set_delt_tau(obj,delta_tau)
      		obj.t_delt = delta_tau;
      end
      function obj = set_node_tree_index(obj,tree_indx)
      		obj.tree_index = tree_indx;
      
      end
      function obj = populate_tree(obj, parent_index)
      
      		children_details = [0 0 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0];
      		for i = 1:size(obj.input_space,2)
      			child_details = [0 0 0];
      			child_details(1) = parent_index;
      			child_details(2) =  obj.input_space(i);
      			child_details(3) = obj.time_to_conflict - obj.t_delt;
      			children_details(i,:) = child_details; 
      		end
      		obj.child_states = children_details;
      end 
      
   end
end
