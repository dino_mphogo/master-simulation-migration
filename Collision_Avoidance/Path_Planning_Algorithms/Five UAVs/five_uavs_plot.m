load uav_1_position_data.mat
load uav_2_position_data.mat
load uav_3_position_data.mat
load uav_4_position_data.mat
load uav_5_position_data.mat
load uav_1_flight_data.mat
load uav_2_flight_data.mat
load uav_3_flight_data.mat
load uav_4_flight_data.mat
load uav_5_flight_data.mat
load five_uavs_sim_time.mat


sim_time = five_uavs_sim_time.data;
fig_1 = figure;
title('Altitude Rate vs simulation time');
plot(sim_time,uav_1_flight_data.data(:,3),'r-')
grid on
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 1 Altitude Inputs');
saveas(fig_1,['uav_1_altitude_input.jpg']);
hold off

fig_1 = figure ;
title('Altitude Rate vs simulation time');
plot(sim_time,uav_2_flight_data.data(:,3),'b-')
grid on
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 2 Altitude Inpts');
saveas(fig_1,['uav_2_altitude_input.jpg']);


hold off
fig_1 = figure ;
title('Altitude Rate vs simulation time');
plot(sim_time,uav_3_flight_data.data(:,3),'b-')
grid on
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 3 Altitude Inpts');
saveas(fig_1,['uav_3_altitude_input.jpg']);


hold off
fig_1 = figure ;
title('Altitude Rate vs simulation time');
plot(sim_time,uav_4_flight_data.data(:,3),'b-')
grid on
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 4 Altitude Inpts');
saveas(fig_1,['uav_4_altitude_input.jpg']);

hold off
fig_1 = figure ;
title('Altitude Rate vs simulation time');
plot(sim_time,uav_5_flight_data.data(:,3),'b-')
grid on
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 5 Altitude Inpts');
saveas(fig_1,['uav_5_altitude_input.jpg']);
