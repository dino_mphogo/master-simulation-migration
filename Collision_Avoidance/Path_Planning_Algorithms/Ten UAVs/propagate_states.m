function new_states = propagate_states(position, plan, velocity, path_track, t)
	for uav_count = 1:size( plan,1)
		position(uav_count, 1) = position(uav_count, 1) + velocity(uav_count, 1)*t;
		position(uav_count,2) = position(uav_count, 2) + velocity(uav_count, 2)*t;
		position(uav_count, 3) = position(uav_count, 3) + plan(uav_count, path_track)*t;
    end
	new_states = position;
end
