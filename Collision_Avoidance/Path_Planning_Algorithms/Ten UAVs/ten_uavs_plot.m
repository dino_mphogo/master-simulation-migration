load uav_1_position_data.mat
load uav_2_position_data.mat
load uav_3_position_data.mat
load uav_4_position_data.mat
load uav_5_position_data.mat
load uav_1_flight_data.mat
load uav_2_flight_data.mat
load uav_3_flight_data.mat
load uav_4_flight_data.mat
load uav_5_flight_data.mat

load uav_6_position_data.mat
load uav_7_position_data.mat
load uav_8_position_data.mat
load uav_9_position_data.mat
load uav_10_position_data.mat
load uav_6_flight_data.mat
load uav_7_flight_data.mat
load uav_8_flight_data.mat
load uav_9_flight_data.mat
load uav_10_flight_data.mat

fig_1 = figure;
title('Altitude Rate vs simulation time');
plot(1:size(uav_1_flight_data.data(:,3),1),uav_1_flight_data.data(:,3),'r-')
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 1 Altitude Inputs');
saveas(fig_1,['uav_1_altitude_input.jpg']);
hold off

fig_1 = figure ;
title('Altitude Rate vs simulation time');
plot(1:size(uav_2_flight_data.data(:,3),1),uav_2_flight_data.data(:,3),'b-')
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 2 Altitude Inpts');
saveas(fig_1,['uav_2_altitude_input.jpg']);


hold off
fig_1 = figure ;
title('Altitude Rate vs simulation time');
plot(1:size(uav_3_flight_data.data(:,3),1),uav_3_flight_data.data(:,3),'b-')
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 3 Altitude Inpts');
saveas(fig_1,['uav_3_altitude_input.jpg']);


hold off
fig_1 = figure ;
title('Altitude Rate vs simulation time');
plot(1:size(uav_4_flight_data.data(:,3),1),uav_4_flight_data.data(:,3),'b-')
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 4 Altitude Inpts');
saveas(fig_1,['uav_4_altitude_input.jpg']);

hold off
fig_1 = figure ;
title('Altitude Rate vs simulation time');
plot(1:size(uav_5_flight_data.data(:,3),1),uav_5_flight_data.data(:,3),'b-')
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 5 Altitude Inpts');
saveas(fig_1,['uav_5_altitude_input.jpg']);


fig_1 = figure;
title('Altitude Rate vs simulation time');
plot(1:size(uav_6_flight_data.data(:,3),1),uav_6_flight_data.data(:,3),'r-')
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 6 Altitude Inputs');
saveas(fig_1,['uav_6_altitude_input.jpg']);
hold off

fig_1 = figure ;
title('Altitude Rate vs simulation time');
plot(1:size(uav_7_flight_data.data(:,3),1),uav_7_flight_data.data(:,3),'b-')
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 7 Altitude Inpts');
saveas(fig_1,['uav_7_altitude_input.jpg']);


hold off
fig_1 = figure ;
title('Altitude Rate vs simulation time');
plot(1:size(uav_8_flight_data.data(:,3),1),uav_8_flight_data.data(:,3),'b-')
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 8 Altitude Inpts');
saveas(fig_1,['uav_8_altitude_input.jpg']);


hold off
fig_1 = figure ;
title('Altitude Rate vs simulation time');
plot(1:size(uav_9_flight_data.data(:,3),1),uav_9_flight_data.data(:,3),'b-')
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 9 Altitude Inpts');
saveas(fig_1,['uav_9_altitude_input.jpg']);

hold off
fig_1 = figure ;
title('Altitude Rate vs simulation time');
plot(1:size(uav_10_flight_data.data(:,3),1),uav_10_flight_data.data(:,3),'b-')
xlabel('time step');
ylabel('Altitude Rate(feet/s)');
legend('UAV 10 Altitude Inpts');
saveas(fig_1,['uav_10_altitude_input.jpg']);
