function conflict_state = conflict_prediction(uavs_positions, current_velocities, planned_climb_rates, time_length, t_plan_host, t_plan_intruder)

conflict_state = 0; % by default no conflict is reported before prediction
host_uav_position = uavs_positions(1,:);
intruder_uav_position = uavs_positions(2,:);
planned_climb_rates_host = planned_climb_rates(1,:);
planned_climb_rates_intruder = planned_climb_rates(2,:);
count = 1;
host_nominal_altitude = uavs_positions(1,3);
intruder_nominal_altitude = uavs_positions(2,3);
while count <= ceil(size(planned_climb_rates_host,2)/2)
    if t_plan_host <= size(planned_climb_rates_host,2) && t_plan_host > 0
        host_uav_position(3) = host_uav_position(3) + planned_climb_rates_host(t_plan_host)*time_length;
        host_uav_position(1) = host_uav_position(1) + current_velocities(1,1)*time_length;
        host_uav_position(2) = host_uav_position(2) + current_velocities(1,2)*time_length;
        t_plan_host = t_plan_host + 1;
        [t_plan_intruder, intruder_uav_position] = simulate_states(intruder_uav_position, planned_climb_rates_intruder,current_velocities(2, :), t_plan_intruder, size(planned_climb_rates_host,2), time_length, intruder_nominal_altitude);
    else
        host_uav_position(1) = host_uav_position(1) + current_velocities(1,1)*time_length;
        host_uav_position(2) = host_uav_position(2) + current_velocities(1,2)*time_length;
        host_h_dot = get_hdot(host_nominal_altitude, host_uav_position(3), 16.4042);
        host_uav_position(3) = host_uav_position(3) + host_h_dot*time_length;
        [t_plan_intruder, intruder_uav_position] = simulate_states(intruder_uav_position, planned_climb_rates_intruder,current_velocities(2, :), t_plan_intruder, size(planned_climb_rates_host,2), time_length, intruder_nominal_altitude);
    end
    if(conflict_detection(host_uav_position, intruder_uav_position) ~= 0)
        conflict_state = 1;
    end
    count = count + 1;
end
end

function [t_plan, position] = simulate_states(position, plan, normal_velocity, t_plan, plan_length, t, nominal_altitude)
    if t_plan <= plan_length && t_plan > 0
        position(3) = position(3) + plan(t_plan)*t;
        position(1) = position(1) + normal_velocity(1,1)*t;
        position(2) = position(2) + normal_velocity(1,2)*t;
        t_plan = t_plan + 1;
    else
        position(1) = position(1) + normal_velocity(1,1)*t;
        position(2) = position(2) + normal_velocity(1,2)*t;
        h_dot = get_hdot(nominal_altitude, position(3), 16.4042);
        position(3) = position(3) + h_dot*t;
    end
    
end

