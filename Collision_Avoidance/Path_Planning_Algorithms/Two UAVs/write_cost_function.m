function cost = write_cost_function(token, path_track)
	global host_cost_function_tcas;
	global host_cost_function_input_variation;
	cost = [0 0];
	if(token && size(host_cost_function_tcas,2) == 6)
		if(path_track > 0 && path_track <= 6)
			cost(1) = host_cost_function_tcas(path_track);
			cost(2) = host_cost_function_input_variation(path_track);
		end
		
	end
end
