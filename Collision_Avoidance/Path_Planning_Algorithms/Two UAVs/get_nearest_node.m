function nearest_nodes = get_nearest_node(node_list, node)
	nearest_nodes = node_list(1);
	epsilon = 0.00010;
	for node_index = 2:size(node_list,2)
		if (abs(node_list(node_index).terminal_cost - node.terminal_cost) < epsilon && sign(node.x_k(3))*sign(node_list(node_index).x_k(3) ) == 1) 
			nearest_nodes(size(nearest_nodes,2)+1) = node_list(node_index);		
		end
	end
end
