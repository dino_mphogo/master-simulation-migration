load('uav_2_position_error.mat')
load('uav_1_position_error.mat')
load('two_uavs_sim_time.mat')
load uav_1_flight_path_data.mat;
load uav_2_flight_path_data.mat;

sim_time = two_uavs_sim_time.data;

figure
uav_1 = abs(uav_1_position_error.Data);
uav_1 = uav_1 - uav_1(1,1);
cdistance = cumtrapz(uav_1);
plot(sim_time, cdistance)
title('UAV 1 Cumulative Altitude')
xlabel('Time (s)')
ylabel('Altitude (feet)')
grid on
filename = 'uav_1_cumulative_altitude.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off




figure
uav_2 = abs(uav_2_position_error.Data);
uav_2 = uav_2 - uav_2(1,1);

uav_2 = uav_2(:,:,:);
cdistance = cumtrapz(uav_2);
plot(sim_time,cdistance)
title('UAV 2 Cumulative Altitude')
xlabel('Time (s)')
ylabel('Altitude (feet)')
grid on
filename = 'uav_2_cumulative_altitude.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off



figure
grid on
hold on
title('Flight Path Angles');
plot(sim_time,uav_1_flight_path_data.data);
plot(sim_time,uav_2_flight_path_data.data);
xlabel('Time(s)');
ylabel('Flight Path Angle(Degrees)');
legend('UAV 1', 'UAV 2');
filename = 'flight_path_angles.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));

figure


uav_2 = uav_2(:,:,:);
plot(sim_time,uav_2)
title('UAV Altitude Change')
xlabel('Time (s)')
ylabel('Altitude (feet)')
grid on
filename = 'uav_2_altitude_change.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off
