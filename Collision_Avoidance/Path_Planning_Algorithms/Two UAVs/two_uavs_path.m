planning_uavs_indicators = csvread('historical_path_planning_uavs.txt');

hold on


load('uav_1_position_data.mat'); 
data = [uav_1_position_data.data(:,1:3)  asin(uav_1_position_data.data(:,4)/max(uav_1_position_data.data(:,5))) ones(size(uav_1_position_data.data(:,1),1),1)*90 zeros(size(uav_1_position_data.data(:,1),1),1) ];
uav_1_data = data;
new_object('air_1.mat',data,... 
'model','f-16.mat','edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.3 .3 .3],'pathwidth',1,'scale',50.0); 
 
% insert friendly aircraft 
load('uav_2_position_data.mat'); 
 data = [uav_2_position_data.data(:,1:3)  asin(uav_2_position_data.data(:,4)/max(uav_2_position_data.data(:,5))) ones(size(uav_2_position_data.data(:,1),1),1)*-90 zeros(size(uav_2_position_data.data(:,1),1),1)];
  data(:,3) = data(:,3) - 2000;
 uav_2_data = data;
new_object('air_2.mat',data,... 
'model','f-16.mat','edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.89 .27 .0],'pathwidth',1,'scale',70.0); 
 
 
% generate the scene and save the result as png file 
flypath('air_2.mat',... 
'animate','off','step',1000,... 
'font','Georgia','fontsize',10,... 
'view',[0 0],'window',[700 500],... 
'xlim',[4000 15000],'ylim',[-500 3500],'zlim',[0 6000]);


% uav text



for row_count = 2:size(planning_uavs_indicators,1)
	uav_id = planning_uavs_indicators(row_count,1);
	uav_conflict_nodes = csvread(['sampled_conflict_nodes_', num2str(uav_id), '_', num2str(planning_uavs_indicators(row_count,2)),'.txt']);
	uav_path = csvread(['path_plan_uav_',num2str(uav_id),'_',num2str(planning_uavs_indicators(row_count,2)) ,'.txt']);
	
	scatter3(uav_path(:,1),uav_path(:,2),uav_path(:,3)-2000,'b', 'filled');
end

%{
[x,y,z] = sphere(100);
for index = 1:1000:size(data,1)
	
	surf(uav_2_data(index,1)+3076.12*0.5*x,uav_2_data(index,2)+3076.12*0.5*y,uav_2_data(index,3)+650*z,... 
	'EdgeColor','none','FaceColor',[.89 .0 .27],'FaceAlpha',.2); 
	%surf(uav_2_data(index,1)+3076.12*0.5*x,uav_2_data(index,2)+3076.12*0.5*y,uav_2_data(index,3)+650*z,... 
	%'EdgeColor','none','FaceColor',[.89 .0 .27],'FaceAlpha',.2); 
	%surf(uav_3_data(index,1)+3076.12*0.5*x,uav_3_data(index,2)+3076.12*0.5*y,uav_3_data(index,3)+650*z,... 
	%'EdgeColor','none','FaceColor',[.89 .0 .27],'FaceAlpha',.2); 

end
%}


