function [cheap_leaf , altitudes, costs, all_nodes_costs] = get_cheap_leaf(all_leaf_nodes)

cheap_leaf_index = 1;
cheap_leaf = all_leaf_nodes(cheap_leaf_index);
best_terminal_cost = inf;
best_control_efforts_cost = inf;
epsilon  = 0.0010; % in feet because we use floating points for the distance valeus
altitudes = [];
costs = [];
all_nodes_costs = [];
% first pass get a node with smallest altitude cost
for node_index = 1:size(all_leaf_nodes,2)
        all_nodes_costs(size(all_nodes_costs,2)+1) =  all_leaf_nodes(node_index).terminal_cost + all_leaf_nodes(node_index).control_efforts_cost;
	if          all_leaf_nodes(node_index).terminal_cost < best_terminal_cost || ...
       (    abs(all_leaf_nodes(node_index).terminal_cost - best_terminal_cost) < epsilon ...
        &&      all_leaf_nodes(node_index).control_efforts_cost < best_control_efforts_cost )
        cheap_leaf                = all_leaf_nodes(node_index);
        best_terminal_cost        = all_leaf_nodes(node_index).terminal_cost;
        best_control_efforts_cost = all_leaf_nodes(node_index).control_efforts_cost;
        altitudes(size(altitudes,2)+1) = cheap_leaf.x_k(3);
        costs(size(costs,2)+1) = best_terminal_cost + best_control_efforts_cost;
	end    
end   




% lowwer_tree = all_leaf_nodes(1);
% upper_tree = all_leaf_nodes(1);
% equal_altitude = all_leaf_nodes(1);
% 
% for l = 1:size(all_leaf_nodes,2)
%     if(abs(all_leaf_nodes(l).x_k(3) - aircraft_altitude) < 100)
%     	if(size(equal_altitude,2) == 1)
%     		if(l == 1)
%     			equal_altitude(1) = all_leaf_nodes(l);
%     		else
%     			equal_altitude(size(equal_altitude,2)+1) = all_leaf_nodes(l);
%     		end
%     	else
%     		equal_altitude(size(equal_altitude,2)+1) = all_leaf_nodes(l);
%     	end
%     else
%     	if(all_leaf_nodes(l).x_k(3) < aircraft_altitude)
%     		if(size(lowwer_tree,2) == 1)
%     			if(l == 1)
%     				lowwer_tree(1) = all_leaf_nodes(l);
%     			else
%     				lowwer_tree(size(lowwer_tree,2)+1) = all_leaf_nodes(l);
%     			end
%     		else
%     			lowwer_tree(size(lowwer_tree,2)+1) = all_leaf_nodes(l);
%     		end
%     	else
%     		if(size(upper_tree,2) == 1)
%     			if(l == 1)
%     				upper_tree(1) = all_leaf_nodes(l);
%     			else
%     				upper_tree(size(upper_tree,2)+1) = all_leaf_nodes(l);
%     			end
%     		else
%     			upper_tree(size(upper_tree,2)+1) = all_leaf_nodes(l);
%     		end
%     	end
%     end
%    
%     
% end
% number_of_lowwer_nodes = size(lowwer_tree,2);
% number_of_upper_nodes = size(upper_tree,2);
% number_equal_nodes = size(equal_altitude,2);
% manoeurvrability_count = number_of_lowwer_nodes + number_of_upper_nodes + number_equal_nodes;
% if(size(equal_altitude, 2) > 1)
% 	cheap_leaf = equal_altitude(1);
%     	for l = 2:size(equal_altitude,2)
%            
%                 if(equal_altitude(l).control_efforts_cost < cheap_leaf.control_efforts_cost)
%                     cheap_leaf = equal_altitude(l);
%                 end
%            
%         end
% else
%     
%     if(number_of_lowwer_nodes >= number_of_upper_nodes && number_of_lowwer_nodes >= 2)
%         cheap_leaf = lowwer_tree(1);
%         for n = 2:size(lowwer_tree,2)
%             if(abs(aircraft_altitude - lowwer_tree(n).x_k(3)) < abs(aircraft_altitude - cheap_leaf.x_k(3)))
%                 cheap_leaf = lowwer_tree(n);
%             end
%             
%         end
%         
%         for l = 1:size(lowwer_tree,2)
%             if(lowwer_tree(l).x_k(3) == cheap_leaf.x_k(3))
%                 if(lowwer_tree(l).control_efforts_cost < cheap_leaf.control_efforts_cost)
%                     cheap_leaf = lowwer_tree(l);
%                 end
%             end
%             
%         end
%         
%     else
%         if(number_of_upper_nodes >= 2)
%             cheap_leaf = upper_tree(1);
%             for n = 2:size(upper_tree,2)
%                 if(abs(aircraft_altitude - upper_tree(n).x_k(3)) < abs(aircraft_altitude - cheap_leaf.x_k(3)))
%                     cheap_leaf = upper_tree(n);
%                 end
%                 
%             end
%             
%             for l = 1:size(upper_tree,2)
%                 if(upper_tree(l).x_k(3) == cheap_leaf.x_k(3))
%                     if(upper_tree(l).control_efforts_cost < cheap_leaf.control_efforts_cost)
%                         cheap_leaf = upper_tree(l);
%                     end
%                 end
%                 
%             end
%         else
%             cheap_leaf = all_leaf_nodes(1);
%         end
%         
%         
%     end
% end
% cheap_leaf
% aircraft_altitude
end
