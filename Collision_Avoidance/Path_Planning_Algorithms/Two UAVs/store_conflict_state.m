function [] = store_conflict_state(conflict_state)
	global conflict_states
	conflict_states = [conflict_state(1:2); conflict_state(3:4)];
end
