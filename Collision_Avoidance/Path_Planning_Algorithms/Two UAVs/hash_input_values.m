function n = hash_input_values(division_length,input_length,tau)
	% input length is th legnth of the list of inputs ie 6 for tau = 30
	% division length is the period of each division eg 10
	if(tau >= 0)
		n = ceil(tau/division_length) + floor(input_length*0.5);
		if(tau == 0)
			n =  ceil(input_length*0.5) + 1;
		else
			n = ceil(tau/division_length) + floor(input_length*0.5);
		end		
	else
		n = floor(tau/division_length) + ceil(input_length*0.5) + 1;
    end
end
