load uav_1_position_data.mat
load uav_2_position_data.mat
load uav_1_flight_data.mat
load uav_2_flight_data.mat
load('two_uavs_sim_time.mat')
sim_time = two_uavs_sim_time.data;


fig_1 = figure;

title('Altitude Rate vs simulation time');
plot(sim_time,uav_1_flight_data.data(:,3),'r-')
grid on
xlabel('Time(s)');
ylabel('Altitude Rate(feet/s)');
legend('UAV 1 Altitude Inputs');
saveas(fig_1,['uav_1_altitude_input.png']);
hold off

fig_1 = figure ;

title('Altitude Deviation');

plot(sim_time,uav_1_flight_data.data(:,1),'b-')
grid on
xlabel('Time(s)');
ylabel('Altitude (feet/s)');
saveas(fig_1,['uav_1_altitude_changes.png']);
legend('UAV 1 Altitude');


fig_1 = figure ;

title('Altitude Rate vs simulation time');

plot(sim_time,uav_2_flight_data.data(:,3),'b-')
grid on
xlabel('Time(s)');
ylabel('Altitude Rate(feet/s)');
saveas(fig_1,['uav_2_altitude_input.png']);
legend('UAV 2 Altitude Inpts');


fig_1 = figure ;

title('Altitude Deviation');

plot(sim_time,uav_2_flight_data.data(:,1),'b-')
grid on
xlabel('Time(s)');
ylabel('Altitude (feet/s)');
saveas(fig_1,['uav_2_altitude_changes.png']);
legend('UAV 2 Altitude');


