function [] = store_path_plan(current_plan)
	global path_plan
	path_plan = [current_plan(1:6); current_plan(7:12); current_plan(13:18)];

end
