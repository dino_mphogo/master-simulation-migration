load uavs_range.mat
load uav_1_control_inputs.mat
load uav_2_control_inputs.mat
load uav_1_cost_function_data.mat
load uav_2_cost_function_data.mat
figure 

	subplot(3,2,[1,2]);
	hold on
	grid on
	title('Position range of UAV 1 and UAV 2')
	plot(1:size(uavs_range_data.data,1),uavs_range_data.data(:,1),'r-')
	xlabel('Time steps(s)');
	ylabel('Range position position(feet)');
	legend('Position Range graph');
	
	subplot(3,2,[3,4]);
	hold on
	grid on
	title('UAV 1 Control Inputs');
	plot(1:size(uav_1_control_data.data,1),uav_1_control_data.data(:,3) ,'r-')
	xlabel('Time step(s)');
	ylabel('Control input u(t)(feet/s)');
	legend('UAV 1 Control Inputs');
	subplot(3,2,5);
	hold on
	grid on
	title('UAV 1 TCAS Cost Function J(u)');
	plot(1:size(uav_1_control_data.data,1),uav_1_cost_function_data.data(:,1) ,'g-')
	xlabel('Time step(s)');
	ylabel('Cost Functional J(u)');
	legend('UAV 1 Cost functional');
	
	subplot(3,2,6);
	hold on
	grid on
	title('UAV 1 Input Variation Cost functional J(u)');
	plot(1:size(uav_1_control_data.data,1),uav_1_cost_function_data.data(:,2) ,'b-')
	xlabel('Cost J(u)');
	ylabel('Time Step(t)');
	legend('UAV 1 Cost Function J(u)');
	
