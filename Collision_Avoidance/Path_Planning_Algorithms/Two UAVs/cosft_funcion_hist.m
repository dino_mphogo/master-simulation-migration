function cost_function_hist(X,Y)
fig = figure;
plot(X,Y);
grid on
xlabel('h(feet)'); 
ylabel('Density(h)');
title('Altitude Distribution Function');
legend('Altitude Sampling Distribution');
hold off
end
