load('two_uavs_conflict_status.mat')

uavs_data = two_uavs_conflict_status.data;
figure
grid on
hold on
uav_1 = uavs_data(1,1:2,:);
uav_1 = reshape(uav_1, [ 2 size(uav_1,3) ])';

sim_time = reshape(uavs_data(1,5,2:end), [size(uav_1,1)-1 1]);
conflict_counts =  uav_1(:,2);
conflict_counts = conflict_counts(2:end,:);
g = area(sim_time,conflict_counts)
g.EdgeColor = 'red';
g.FaceColor = 'red';
title('UAV Conflicts-Time')
xlabel('Time(s)')
ylabel('Conflicts Count')
area_conflict = trapz(2:size(uav_1,1),conflict_counts);
legend(['UAV Conflict Area = ', num2str(area_conflict)])
filename = 'uav_1_conflict_counts.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off



