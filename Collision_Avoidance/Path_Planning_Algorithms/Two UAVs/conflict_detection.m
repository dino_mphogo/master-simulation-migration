function conflict_state = conflict_detection(host_position,intruder_position)
x_dmod_ta  = 3076.12*0.5;
%z_thr = 850*0.3048;
z_thr_ta = 650;
%agl_threshold = 1000*0.3048;
agl_threshold = 650;
own_altitude = host_position(3);
sensetivity_level = 4; % this helps with the thresholds of TA or RA
ta = 30;
ra = 20;
z_delt = host_position(3) - intruder_position(3);
x_delt = host_position(1) - intruder_position(1);
if(abs(z_delt) <= z_thr_ta && abs(x_delt) <= x_dmod_ta)
    conflict_state = 1;
   
else 
   conflict_state = 0;
end

end
