load('uav_1_position_data.mat')
load('uav_2_position_data.mat')
load('uav_3_position_data.mat')
load('uav_4_position_data.mat')
load('uav_5_position_data.mat')
disp('##UAV 1 Stats##')
disp(' ')
original_altitude = uav_1_position_data.data(1,3);
disp(['Original altitude ', num2str(original_altitude), 'feet'])
disp(' ')
altitude_mean = mean(abs(uav_1_position_data.data(:,3) - original_altitude));
disp(['Mean altitude deviation ', num2str(altitude_mean)])
altitude_rate = mean(abs(uav_1_position_data.data(:,4) - uav_1_position_data.data(1,4)));
disp(['Mean altitude rate ', num2str(altitude_rate)])
disp(' ')
disp(' ')

disp('##UAV 2 Stats##')
disp(' ')
original_altitude = uav_2_position_data.data(1,3);
disp(['Original altitude ', num2str(original_altitude), 'feet'])
disp(' ')
altitude_mean = mean(abs(uav_2_position_data.data(:,3) - original_altitude));
disp(['Mean altitude deviation ', num2str(altitude_mean)])
disp(' ')
altitude_rate = mean(abs(uav_2_position_data.data(:,4) - uav_2_position_data.data(1,4)));
disp(['Mean altitude rate ', num2str(altitude_rate)])
disp(' ')

disp('##UAV 3 Stats##')
disp(' ')
original_altitude = uav_3_position_data.data(1,3);
disp(['Original altitude ', num2str(original_altitude), 'feet'])
altitude_mean = mean(abs(uav_3_position_data.data(:,3) - original_altitude))
disp(['Mean altitude deviation ', num2str(altitude_mean)])
disp(' ')
altitude_rate = mean(abs(uav_3_position_data.data(:,4) - uav_3_position_data.data(1,4)));
disp(['Mean altitude rate ', num2str(altitude_rate)])
disp(' ')

disp('##UAV 4 Stats##')
disp(' ')
original_altitude = uav_4_position_data.data(1,3);
disp(['Original altitude ', num2str(original_altitude), 'feet'])
altitude_mean = mean(abs(uav_4_position_data.data(:,3) - original_altitude))
disp(['Mean altitude deviation ', num2str(altitude_mean)])
disp(' ')
altitude_rate = mean(abs(uav_4_position_data.data(:,4) - uav_4_position_data.data(1,4)));
disp(['Mean altitude rate ', num2str(altitude_rate)])
disp(' ')


disp('##UAV 5 Stats##')
disp(' ')
original_altitude = uav_5_position_data.data(1,3);
disp(['Original altitude ', num2str(original_altitude), 'feet'])
altitude_mean = mean(abs(uav_5_position_data.data(:,3) - original_altitude))
disp(['Mean altitude deviation ', num2str(altitude_mean)])
disp(' ')
altitude_rate = mean(abs(uav_5_position_data.data(:,4) - uav_5_position_data.data(1,4)));
disp(['Mean altitude rate ', num2str(altitude_rate)])
disp(' ')


