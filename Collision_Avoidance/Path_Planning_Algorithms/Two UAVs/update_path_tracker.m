function [] = update_path_tracker(input_value)
	path_length = input_value(2);
	plan_track = input_value(1);
	global plan_tracker
	global solution_path_length
	if(path_length ~= 6 && plan_track == 0)
		plan_tracker = plan_track;
		solution_path_length = 6;
	end
end

