function [] = search_tree(uavs_positions, uavs_velocities, conflict_states)
global tree
global plan_tracker
global token_value
global path_plan 
global planning_count
global last_chosen_uav
global number_of_intruders
global uavs_tree_nodes_count
x_dot = 27.4910/0.3048;
y_dot = 0;
t = 10;
conflict_state = [conflict_states(1:2)' ; conflict_states(3:4)'];
%plan_tracker
row_count = 0;
% token allocation stratergy containers
uavs_intruder_count = zeros(1,size(conflict_state,1));
uavs_manoeuvrability_count = zeros(1,size(conflict_state,1));
closing_in_intruders = zeros(1,size(conflict_state,1));
uavs_velocities =  uavs_velocities';
velocity_vect = [uavs_velocities(1:3); uavs_velocities(4:6)];
uavs_positions = uavs_positions';
positions = [uavs_positions(1:3); uavs_positions(4:6)];
in_conflict_uavs = [];
uavs_plan = populate_uavs_plans(positions, path_plan, plan_tracker, t);
new_uavs_plan = uavs_plan; % this will change after planning
for uav_count = 1:size(conflict_state,1)
    conflict_row = conflict_state(uav_count,:);
    in_conflict = find(conflict_row == 1);
    uavs_intruder_count(uav_count) = size(in_conflict,2);
    %in_conflict = in_conflict(~ismember(in_conflict,token_value));
    %conflict_row = conflict_row(in_conflict);
    if(size(in_conflict,2) > 0 && ~ismember(uav_count,token_value))
		row_count = row_count + 1;
		in_conflict_uavs(size(in_conflict_uavs,2)+1) = uav_count;
	end
end
if(row_count > 0 )
    % in_conflict_uavs
    simulation_file = fopen('simulation_output.txt','a');
    sim_time = datetime('now');
    display_mes = ['#################### UAVs Path Plannning has started! ', datestr(sim_time), ' ######################'];
    disp(display_mes);
    display_mes = ['  Writing into a file simulation_output.txt'];
    disp(display_mes);
    display_mes = ['#################### UAVs Path Plannning has started! ', datestr(sim_time), ' ######################\n'];
    
    fprintf(simulation_file, display_mes);
    conflict_period = 60; % sixty seconds conflict period
    t_divisions = 10;
    tree_depth = conflict_period/t_divisions;
    % uav 1
    uav_1_tree = state_node(positions(1,:),0, 1, conflict_period/2);
    uav_1_tree = uav_1_tree.populate_tree(1);
    % uav 2
    uav_2_tree = state_node(positions(2,:),0, 1, conflict_period/2);
    uav_2_tree = uav_2_tree.populate_tree(1);
    % uav 3
    uav_3_tree = state_node(positions(2,:),0, 1, conflict_period/2);
    uav_3_tree = uav_2_tree.populate_tree(1);
    tree = [uav_1_tree; uav_2_tree; uav_3_tree];
    transition_cost_sum = inf(1,size(conflict_state,1));
    terminal_costs = inf(1,size(conflict_state,1));
    % encounted_range = inf(1,size(conflict_state,1));
    num_conflicted_nodes = zeros(1,size(conflict_state,1));
    solutions_length_matrix = zeros(1,size(conflict_state,1));
    has_planned = [0 0 0];
    propagated_position = {};
    current_position = positions;
    for input_length = 1:size(uavs_plan, 2)
        current_position = propagate_states(current_position, uavs_plan, velocity_vect, input_length, t_divisions);
        propagated_position(size(propagated_position, 2)+1) = {current_position};
    end
    for uav_counter = 1:size(in_conflict_uavs,2)
        uav_id = in_conflict_uavs(uav_counter);
        % uavs_nominal_altitude(uav_id) = positions(uav_id,3);
        uavs_tree_nodes_count(uav_id) = uavs_tree_nodes_count(uav_id) + 1;
        uav_conflict_state = conflict_state(uav_id,:);
        temp_tree = tree(uav_id);
        % plan_tracker_cpy = plan_tracker;
        all_uavs_position = propagated_position;
        sampled_data = [];
        conflicted_nodes = [];
        if(size(uav_conflict_state(uav_conflict_state == 1),2) > 0 || increasing_traffic_intensity(uav_id,size(uav_conflict_state(uav_conflict_state == 1),2)))
            children = temp_tree.child_states;
            display_mes = ['### UAV ',num2str(uav_id), ' will start with path re-planning now ###\n'];
            fprintf(simulation_file, display_mes);
            fprintf(simulation_file,'	i) Algorithm has started with tree initilization\n');
            fprintf(simulation_file,'	ii) Calculating path for different tau values...\n');
            parent_index = 1;
            sampled_data(size(sampled_data,1)+1,:)  = temp_tree.x_k;
            for n = 1:tree_depth
                number_of_children = size(children,1);
                child_states = children(1,:);
                intruders_position = all_uavs_position{n};
                for p = 1:number_of_children
                    x_k = temp_tree(children(p,1)).x_k;
                    x_k(1) = x_k(1) + sign(velocity_vect(uav_id,1))*x_dot*t_divisions;
                    x_k(2) = x_k(2) + sign(velocity_vect(uav_id,2))*y_dot*t_divisions;
                    x_k(3) = x_k(3) + children(p,2)*t_divisions;
                    node = state_node(x_k,children(p,2),children(p,1),children(p,3));
                    if(conflict_detection_tree(node.x_k,intruders_position, uav_id) == 0)
                    	sampled_data(size(sampled_data,1)+1,:)  = node.x_k;
                        parent_index = parent_index + 1;
                        node = node.populate_tree(parent_index);
                        node.tree_index = size(temp_tree,2)+1;
                        state_children = node.child_states;
                        temp_tree(size(temp_tree,2)+1) =  node;
                        for k = 1:size(state_children,1)
                            if(size(child_states,1) == 1 && k == 1)
                                child_states(1,:) = state_children(k,:);
                            else
                                child_states(size(child_states,1)+1,:) = state_children(k,:);
                            end
                            
                        end
                    else
                        num_conflicted_nodes(uav_id) = num_conflicted_nodes(uav_id) + 1;
                        conflicted_nodes(size(conflicted_nodes, 1)+1,:) = node.x_k;
                    end
                end
                children = child_states;
                
            end
            number_of_intruders(uav_id) = size(uav_conflict_state(uav_conflict_state == 1),2);
            display_mes = ['	iii) Number of Planning Tree nodes in conflict were ',num2str(num_conflicted_nodes), '\n'];
            fprintf(simulation_file,display_mes);
            csv_file_name = ['sampled_data_uav_',num2str(uav_id),'_temp.txt'];
            csvwrite(csv_file_name,sampled_data);
            csv_file_name = ['sampled_conflict_nodes_', num2str(uav_id), '_' , num2str(planning_count(uav_id)),'.txt'];
            csvwrite(csv_file_name, conflicted_nodes);
            tau_val = -conflict_period/2;
            temp_tree = tcas_cost_functional(temp_tree,uav_id);
            csv_file_name = ['cost_nodes_', num2str(uav_id), '_' , num2str(planning_count(uav_id)),'.txt'];
            nodes_costs = [];
            for indx = 1:size(temp_tree,2)
                nodes_costs(size(nodes_costs,1)+1, :) = [temp_tree(indx).x_k(1) temp_tree(indx).x_k(3) temp_tree(indx).control_efforts_cost+temp_tree(indx).terminal_cost];
            end
            csvwrite(csv_file_name, nodes_costs);
            leaf_nodes =  temp_tree(end);
            for w = size(temp_tree,2)-1:-1:1
                if(temp_tree(w).time_to_conflict == tau_val)
                    leaf_nodes(size(leaf_nodes,2)+1) = temp_tree(w);
                end
            end
            %node = get_nearest_node(leaf_nodes);
            node = get_cheap_leaf(leaf_nodes, node);
            node_count = size(leaf_nodes, 2);
            cheap_leaf = node;
            recomended_plan_inputs = construct_path(temp_tree, cheap_leaf);
            uavs_manoeuvrability_count(uav_id) = size(leaf_nodes,2);
            terminal_costs(uav_id) = node.terminal_cost;
            transition_cost_sum(uav_id)  = node.control_efforts_cost;
            if(cheap_leaf.time_to_conflict ~= tau_val)
                recomended_plan_inputs(size(recomended_plan_inputs,2)+1:size(path_plan, 2)) = 0;
                has_planned(uav_id) = 0;
            else
                has_planned(uav_id) = 1;
                recomended_plan_inputs = recomended_plan_inputs(1:size(path_plan, 2));
            end
            solutions_length_matrix(uav_id) = size(recomended_plan_inputs,2);
            recomended_plan_inputs_cpy = recomended_plan_inputs;
            p = 1;
            for l = size(recomended_plan_inputs,2):-1:1
                recomended_plan_inputs(p) = recomended_plan_inputs_cpy(l);
                p = p + 1;
            end
            path_cost(uav_id) = path_cost_estimation(recomended_plan_inputs);
            new_uavs_plan(uav_id,:) = recomended_plan_inputs;
            str_inputs = num2str(recomended_plan_inputs);
            display_mes = ['	iv) Sampled inputs ', str_inputs, ' ft/sec\n'];
            fprintf(simulation_file, display_mes);
            
            
        end
    end
    uavs_planned = find(has_planned == 1);
   

    %transition_cost_sum = transition_cost_sum(transition_cost_sum ~= inf)

    % token rule 1 : uavs with minimum number of intruders
    chosen_uav = uavs_planned(1);
    
    min_intruder = min(uavs_intruder_count(uavs_planned));
    for k = 1:size(conflict_state,1)
    	if (uavs_intruder_count(k) == min_intruder) && (size(uavs_planned(uavs_planned == k),2) > 0)
    		chosen_uav(size(chosen_uav,2)+1) = k;
    	end 
    end
    if(size(chosen_uav,2) > 1)
        % token rule 2 : Manoeuvrability count/Path Cost
        path_cost_min = min(path_cost);
        for k = 1:size(chosen_uav,1)
        	if path_cost(chosen_uav(k)) == path_cost_min
        		chosen_uav = chosen_uav(k);
        		break 
        	end
        end
        % uavs_manoeuvrability_count
        % chosen_uav 
         chosen_uav = find(uavs_manoeuvrability_count == min(uavs_manoeuvrability_count(uavs_planned)));
        % lastly rule 4
         chosen_uav = chosen_uav(end);
    end
    
    fprintf(simulation_file,'\n');
    fprintf(simulation_file,'### Token Allocation ###\n');
    chosen_planning_result = [ 'UAV ',num2str(chosen_uav), ' was selected\n'];
    fprintf(simulation_file,chosen_planning_result);
    
    uav_csv_file = ['sampled_data_uav_',num2str(chosen_uav),'_temp.txt'];
    uav_sampled_points = csvread(uav_csv_file);
    new_uav_sampled_data_file = ['sampled_data_uav_',num2str(chosen_uav),'_',num2str(planning_count(chosen_uav)),'.txt'];
    csvwrite(new_uav_sampled_data_file,uav_sampled_points);
    
    % history of all chesen uavs during simulation
    current_file_data = csvread('historical_path_planning_uavs.txt');
    new_line = [chosen_uav planning_count(chosen_uav) positions(chosen_uav,:)];
    new_file_data = [current_file_data; new_line];
    csvwrite('historical_path_planning_uavs.txt',new_file_data);
    
    uav_chosen_path = generate_uav_path(velocity_vect(chosen_uav,:),positions(chosen_uav,:),new_uavs_plan(chosen_uav,:));
    csv_file_name = ['path_plan_uav_',num2str(chosen_uav),'_',num2str(planning_count(chosen_uav)),'.txt'];
    csvwrite(csv_file_name,uav_chosen_path);
    csv_file_name = ['path_plan_uav_',num2str(chosen_uav),'_',num2str(planning_count(chosen_uav)),'_plan.txt'];
    csvwrite(csv_file_name,new_uavs_plan(chosen_uav,:));
    path_plan(chosen_uav,:) = new_uavs_plan(chosen_uav,:);
    token_value(size(token_value,2)+1) = chosen_uav;
    plan_tracker(chosen_uav) = 1;
    planning_count(chosen_uav) = planning_count(chosen_uav) + 1;
    disp(' ')
    display_mes = ['### Updated UAVs Path Plans ft/sec ###\n'];
    fprintf(simulation_file, display_mes);
    for line = 1:size(path_plan,1)
        file_line = [num2str(path_plan(line,:)), '\n'];
        fprintf(simulation_file, file_line);
    end
    
    
end

%     %transition_cost_sum = transition_cost_sum(transition_cost_sum ~= inf)
%     if(size(find(has_planned == 1),2) > 0)
%         uavs_planned = find(has_planned == 1);
%         selected_cost_sum = transition_cost_sum(uavs_planned) + terminal_costs(uavs_planned);
%         transition_cost_sum_planned = transition_cost_sum(uavs_planned);
%         if(size(transition_cost_sum_planned(transition_cost_sum_planned > 0),2 ) > 0)
%             min_index_value = uavs_planned(find(transition_cost_sum_planned == min(transition_cost_sum_planned(transition_cost_sum_planned > 0))));
%             if(size(min_index_value,2) > 1)
%                 chosen_min = min_index_value(1);
%                 for v = 2:size(min_index_value)
%                     if(terminal_costs(min_index_value(v)) < terminal_cost(chosen_min))
%                         chosen_min = min_index_value(v);
%                     end
%                 end
%                 min_index_value = chosen_min;
%             end
%         
%         else
%             planned_terminal_costs = terminal_costs(uavs_planned);
%             min_index_value = uavs_planned(find(planned_terminal_costs == min(planned_terminal_costs(planned_terminal_costs > 0))));
%             if size(min_index_value,2) > 0
%                 chosen_min = min_index_value(1);
%                 for v = 2:size(min_index_value)
%                     if(terminal_costs(min_index_value(v)) < terminal_cost(chosen_min))
%                         chosen_min = min_index_value(v);
%                     end
%                 end
%                 min_index_value = chosen_min;
%             else
%                 min_index_value = uavs_planned(1);
%             end
%         end
%         last_token = min_index_value;
%         if(~ismember(last_token, token_value))
%             token_value(size(token_value,2)+1) = last_token;
%             path_plan(last_token,:) = plans_generated(last_token,:);
%             plan_tracker(last_token) = 1;
%             
%         end
%     end
end

function cost = path_cost_estimation(input_set)
	cost = 0;
	for indx = 1:size(input_set,2)
		if abs(input_set(indx)) == 25
			cost = cost + 2;
		else
			if abs(input_set(indx)) == 50
				cost = cost + 3;
			end
		end	 
	end
end

