planning_uavs_indicators = csvread('historical_path_planning_uavs.txt');

hold on


load('uav_1_position_data.mat'); 
data = [uav_1_position_data.data(:,1:3)  asin(uav_1_position_data.data(:,4)/max(uav_1_position_data.data(:,5))) ones(size(uav_1_position_data.data(:,1),1),1)*90 zeros(size(uav_1_position_data.data(:,1),1),1) ];
new_object('air_1.mat',data,... 
'model','f-16.mat', 'scale',50,'edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.3 .3 .3],'pathwidth',1); 
 
% insert friendly aircraft 
load('uav_2_position_data.mat'); 
 data = [uav_2_position_data.data(:,1:3)  asin(uav_2_position_data.data(:,4)/max(uav_2_position_data.data(:,5))) ones(size(uav_2_position_data.data(:,1),1),1)*-90 zeros(size(uav_2_position_data.data(:,1),1),1)];
new_object('air_2.mat',data,... 
'model','f-16.mat','scale',50,'edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.89 .27 .0],'pathwidth',1); 
 
 
 % insert friendly aircraft 
load('uav_3_position_data.mat'); 
 data = [uav_3_position_data.data(:,1:3)  asin(uav_3_position_data.data(:,4)/max(uav_3_position_data.data(:,5))) ones(size(uav_3_position_data.data(:,1),1),1)*-90 zeros(size(uav_3_position_data.data(:,1),1),1)];
new_object('air_3.mat',data,... 
'model','f-16.mat','scale',50,'edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.0 .5 .1],'pathwidth',1); 
 
% generate the scene and save the result as png file 
flypath('air_1.mat','air_2.mat', 'air_3.mat',... 
'animate','off','step',1000,... 
'font','Georgia','fontsize',10,... 
'view',[0 0],'window',[1000 800],... 
'xlim',[-100 12000],'ylim',[-500 6000],'zlim',[0 7000]);


% uav text


for row_count = 2:size(planning_uavs_indicators,1)
	uav_id = planning_uavs_indicators(row_count,1);
	uav_conflict_nodes = csvread(['sampled_conflict_nodes_', num2str(uav_id), '_', num2str(planning_uavs_indicators(row_count,2)),'.txt']);
	uav_path = csvread(['path_plan_uav_',num2str(uav_id),'_',num2str(planning_uavs_indicators(row_count,2)) ,'.txt']);
	
	scatter3(uav_path(:,1),uav_path(:,2),uav_path(:,3),'b', 'filled');
end

filename = 'three_uavs_animation.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));

