function [cost_levels, groups] = group_by_cost(leaf_nodes)
groups = {};
cost_levels = [];
for index = 1:size(leaf_nodes,2)
	if size(find(cost_levels == leaf_nodes(index).terminal_cost), 2) > 0 
		group_index = find(cost_levels == leaf_nodes(index).terminal_cost);
		current_group = groups(group_index);
		group_mat = cell2mat(current_group);
		group_mat(size(group_mat,2)+1) = index;
		
		groups(group_index) = {group_mat};
		
	else
		cost_levels(size(cost_levels,2)+1) = leaf_nodes(index).terminal_cost;
		groups(size(groups,2)+1) = {index};
	end
end

end
