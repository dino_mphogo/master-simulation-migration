function [] = store_position_and_velocity(new_position,new_velocity)
	global uavs_position_vects
	global velocity_vectors
	velocity_vectors =[new_velocity(1:3); new_velocity(4:6)];
	uavs_position_vects = [new_position(1:3); new_position(4:6)];
	
end
