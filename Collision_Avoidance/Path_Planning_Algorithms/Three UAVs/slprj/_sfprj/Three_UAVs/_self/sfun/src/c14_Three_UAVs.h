#ifndef __c14_Three_UAVs_h__
#define __c14_Three_UAVs_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc14_Three_UAVsInstanceStruct
#define typedef_SFc14_Three_UAVsInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c14_sfEvent;
  boolean_T c14_isStable;
  boolean_T c14_doneDoubleBufferReInit;
  uint8_T c14_is_active_c14_Three_UAVs;
  real_T *c14_number_of_uavs;
  real_T *c14_path_tracker_;
  real_T *c14_path_track;
  real_T (*c14_Fleet_Positions)[9];
  real_T (*c14_uavs_velocities)[9];
  real_T (*c14_path_plan_out)[18];
  real_T (*c14_path_plan_in)[18];
  real_T (*c14_conflict_state)[9];
  real_T (*c14_conflict_state_)[9];
  real_T *c14_token_value;
  real_T *c14_token_value_;
  real_T *c14_intruder_index_;
  real_T *c14_intruder_index;
} SFc14_Three_UAVsInstanceStruct;

#endif                                 /*typedef_SFc14_Three_UAVsInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c14_Three_UAVs_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c14_Three_UAVs_get_check_sum(mxArray *plhs[]);
extern void c14_Three_UAVs_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
