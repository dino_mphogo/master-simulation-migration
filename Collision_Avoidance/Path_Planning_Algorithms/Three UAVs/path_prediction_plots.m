uav_1_position_data = [];
uav_2_position_data = [];
uav_3_position_data = [];

for index = 1:size(simulated_positions,2)
    simulated = simulated_positions{index};
    uav_1 = [simulated{1}(1,:); simulated{2}(1,:); simulated{3}(1,:); simulated{4}(1,:); simulated{5}(1,:); simulated{6}(1,:)];
    uav_2 = [simulated{1}(2,:); simulated{2}(2,:); simulated{3}(2,:); simulated{4}(2,:); simulated{5}(2,:); simulated{6}(2,:)];
    uav_3 = [simulated{1}(3,:); simulated{2}(3,:); simulated{3}(3,:); simulated{4}(3,:); simulated{5}(3,:); simulated{6}(3,:)];
    uav_1_position_data = uav_1;
    uav_2_position_data = uav_2;
    uav_3_position_data = uav_3;


	
      data = [uav_1_position_data(:,1:3)  atan(gradient(uav_1_position_data(:,3))/min(gradient(abs(uav_1_position_data(:,1))))) ones(size(uav_1_position_data(:,1),1),1)*90 zeros(size(uav_1_position_data(:,1),1),1) ];
uav_1_data = data;
new_object('air_1.mat',data,... 
'model','f-16.mat','edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.3 .3 .3],'pathwidth',1,'scale',50.0); 
 

 data = [uav_2_position_data(:,1:3)  atan(gradient(uav_2_position_data(:,3))/min(gradient(abs(uav_2_position_data(:,1))))) ones(size(uav_2_position_data(:,1),1),1)*-90 zeros(size(uav_2_position_data(:,1),1),1)];
 uav_2_data = data;
new_object('air_2.mat',data,... 
'model','f-16.mat','edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.89 .27 .0],'pathwidth',1,'scale',50.0); 
 
 
  data = [uav_3_position_data(:,1:3)  atan(gradient(uav_3_position_data(:,3))/min(gradient(abs(uav_3_position_data(:,1))))) ones(size(uav_3_position_data(:,1),1),1)*-90 zeros(size(uav_3_position_data(:,1),1),1)];
 uav_3_data = data;
new_object('air_3.mat',data,... 
'model','f-16.mat','edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.89 .27 .0],'pathwidth',1,'scale',50.0); 
 

 
% generate the scene and save the result as png file 
flypath('air_1.mat','air_2.mat','air_3.mat',... 
'animate','off','step',1,... 
'font','Georgia','fontsize',10,... 
'view',[0 0],'window',[1000 800],... 
'xlim',[min([uav_1_position_data(:,1); uav_2_position_data(:,1); uav_3_position_data(:,1)])-500 max([uav_1_position_data(:,1); uav_2_position_data(:,1); uav_3_position_data(:,1)])+500],'ylim',[-500 3500],'zlim',[0 8000]);
      hold on
       [x,y,z] = sphere(100);

     %view(1,3)
      plot3(uav_1_position_data(:,1),uav_1_position_data(:,2),uav_1_position_data(:,3), 'r')
     scatter3(uav_1_position_data(:,1),uav_1_position_data(:,2),uav_1_position_data(:,3),'MarkerFaceColor',[1 0 0])
     plot3(uav_2_position_data(:,1), uav_2_position_data(:,2), uav_2_position_data(:,3), 'g')
      scatter3(uav_2_position_data(:,1),uav_2_position_data(:,2),uav_2_position_data(:,3),'MarkerFaceColor',[0 1 0])
     plot3(uav_3_position_data(:,1), uav_3_position_data(:,2), uav_3_position_data(:,3), 'b')
      scatter3(uav_3_position_data(:,1),uav_3_position_data(:,2),uav_3_position_data(:,3),'MarkerFaceColor',[0 0 1])
     filename = ['three_uavs_animation_', num2str(index) ,'_.png']; 
     eval(sprintf('print -dpng -r600 %s;',filename));

       close all
  	
end


