load('tree_uavs_conflict_status.mat')

uavs_data = three_uavs_conflict_status.data;
figure
grid on
hold on
uav_1 = uavs_data(1,1:3,:);
uav_1 = reshape(uav_1, [ 3 size(uav_1,3) ])';
conflict_counts =  uav_1(:,2);
conflict_counts = conflict_counts(2:end,:);
area(2:size(uav_1,1),conflict_counts)
title('UAV Conflicts-Time')
xlabel('Time(n)')
ylabel('Conflicts Count')
area_conflict = trapz(2:size(uav_1,1),conflict_counts);
legend(['UAV Conflict Area = ', num2str(area_conflict)])
filename = 'uav_1_conflict_counts.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off


figure
grid on
hold on
uav_2 = uavs_data(1,4:6,:);
uav_2 = reshape(uav_2, [ 3 size(uav_2,3) ])';
conflict_counts =  uav_2(:,2);
conflict_counts = conflict_counts(2:end,:);
area(2:size(uav_2,1),conflict_counts)
title('UAV Conflicts-Time')
xlabel('Time(n)')
ylabel('Conflicts Count')
area_conflict = trapz(2:size(uav_2,1),conflict_counts);
legend(['UAV Conflict Area = ', num2str(area_conflict)])
filename = 'uav_2_conflict_counts.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off


figure
grid on
hold on
uav_3 = uavs_data(1,7:9,:);
uav_3 = reshape(uav_3, [ 3 size(uav_3,3) ])';
conflict_counts =  uav_3(:,2);
conflict_counts = conflict_counts(2:end,:);
area(2:size(uav_3,1),conflict_counts)
title('UAV Conflicts-Time')
xlabel('Time(n)')
ylabel('Conflicts Count')
area_conflict = trapz(2:size(uav_3,1),conflict_counts);
legend(['UAV Conflict Area = ', num2str(area_conflict)])
filename = 'uav_3_conflict_counts.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off
