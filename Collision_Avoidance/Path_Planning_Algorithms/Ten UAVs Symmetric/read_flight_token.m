function token_outcome = read_flight_token(uav_id)
	global token_value
	
	if(ismember(uav_id, token_value))
		token_outcome = 1;
	else
		token_outcome = 0;
	end
	
end
