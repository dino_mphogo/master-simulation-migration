function [uavs_position, plan_trk] = propagate_aircraft_dynamics(all_uavs_position, plan_tracker_cpy, path_plan, uav_id, velocity_vect, x_dot, y_dot, t_divisions)
global uavs_nominal_altitude
 for uav_id_counter = 1:size(all_uavs_position,1)
                    if(uav_id_counter ~= uav_id)
                        if(plan_tracker_cpy(uav_id_counter) == 0)
                            all_uavs_position(uav_id_counter,1) =  all_uavs_position(uav_id_counter,1) + sign(velocity_vect(uav_id_counter,1))*x_dot*t_divisions;
                            all_uavs_position(uav_id_counter,2) =  all_uavs_position(uav_id_counter,2) + sign(velocity_vect(uav_id_counter,2))*y_dot*t_divisions;
                            all_uavs_position(uav_id_counter,3) =  all_uavs_position(uav_id_counter,3) + get_hdot(uavs_nominal_altitude(uav_id_counter),all_uavs_position(uav_id_counter,3), 16.4042)*t_divisions;
                        else
                            all_uavs_position(uav_id_counter,1) =  all_uavs_position(uav_id_counter,1) + sign(velocity_vect(uav_id_counter,1))*x_dot*t_divisions;
                            all_uavs_position(uav_id_counter,2) =  all_uavs_position(uav_id_counter,2) + sign(velocity_vect(uav_id_counter,2))*y_dot*t_divisions;
                            all_uavs_position(uav_id_counter,3) =  all_uavs_position(uav_id_counter,3) + path_plan(uav_id_counter,plan_tracker_cpy(uav_id_counter))*t_divisions;
                            plan_tracker_cpy(uav_id_counter) = plan_tracker_cpy(uav_id_counter) + 1;
                            if(plan_tracker_cpy(uav_id_counter) > size(path_plan,2))
                                plan_tracker_cpy(uav_id_counter) = 0;
                            end
                        end
                    end
                    
end
        plan_trk = plan_tracker_cpy;
        uavs_position = all_uavs_position; 
end
