function updated_uav_plans = populate_uavs_plans(uavs_positions,uavs_plan, plan_tracks, t)
    global uavs_nominal_altitude
	for uav_count = 1:size(plan_tracks, 2)
		if plan_tracks(uav_count) > 0
			new_input_length = plan_tracks(uav_count);
			new_uav_plan = uavs_plan(uav_count, plan_tracks(uav_count):size(uavs_plan,2));
            length = plan_tracks(uav_count);
			for path_count = length:size(uavs_plan,2)
				uavs_positions(uav_count, 3) = uavs_positions(uav_count, 3) + uavs_plan(path_count)*t;
			end
			% propergate existing plans
			for count = 1:size(uavs_plan,2) - size(new_uav_plan, 2)
				h_dot = get_hdot(uavs_nominal_altitude(uav_count), uavs_positions(uav_count, 3), 16.4042);
				uavs_positions(uav_count, 3) = uavs_positions(uav_count,3) + h_dot*t;
				new_uav_plan(size(new_uav_plan, 2)+1) = h_dot;
            end
		else
			new_uav_plan = [];
			for count = 1:size(uavs_plan,2)
				h_dot = get_hdot(uavs_nominal_altitude(uav_count), uavs_positions(uav_count, 3), 16.4042);
				uavs_positions(uav_count, 3) = uavs_positions(uav_count, 3) + h_dot*t;
				new_uav_plan(size(new_uav_plan,2)+1) = h_dot;
            end
        
        end
		uavs_plan(uav_count, :) = new_uav_plan;
    end
    updated_uav_plans = uavs_plan;
end
