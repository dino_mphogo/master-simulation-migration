load('ten_uavs_range_monte_carlo.mat')
load('ten_uavs_sim_time.mat')
sim_time = ten_uavs_sim_time.data;
uavs_data = ten_uavs_range_monte_carlo.data;
figure
grid on
hold on
uav_1 = uavs_data(1,:,:);
uav_1 = reshape(uav_1, [ size(uav_1,2) size(uav_1,3) ])';
uav_2 = uavs_data(2,:,:);
uav_2 = reshape(uav_2, [ size(uav_2,2) size(uav_2,3) ])';
uav_3 = uavs_data(3,:,:);
uav_3 = reshape(uav_3, [ size(uav_3,2) size(uav_3,3) ])';

uav_3 = uavs_data(3,:,:);
uav_3 = reshape(uav_3, [ size(uav_3,2) size(uav_3,3) ])';


uav_4 = uavs_data(4,:,:);
uav_4 = reshape(uav_4, [ size(uav_4,2) size(uav_4,3) ])';

uav_5 = uavs_data(5,:,:);
uav_5 = reshape(uav_5, [ size(uav_5,2) size(uav_5,3) ])';


range = 1:size(uav_1,1);
plot(sim_time,uav_1(range,2))
plot(sim_time,uav_1(range,3))
plot(sim_time,uav_1(range,4))
plot(sim_time,uav_1(range,5))
plot(sim_time,uav_1(range,6))
plot(sim_time,uav_1(range,7))
plot(sim_time,uav_1(range,8))
plot(sim_time,uav_1(range,9))
plot(sim_time,uav_1(range,10))


plot(sim_time,uav_2(range,3))
plot(sim_time,uav_2(range,4))
plot(sim_time,uav_2(range,5))
plot(sim_time,uav_2(range,6))
plot(sim_time,uav_2(range,7))
plot(sim_time,uav_2(range,8))
plot(sim_time,uav_2(range,9))
plot(sim_time,uav_2(range,10))


plot(sim_time,uav_3(range,4))
plot(sim_time,uav_3(range,5))
plot(sim_time,uav_3(range,6))
plot(sim_time,uav_3(range,7))
plot(sim_time,uav_3(range,8))
plot(sim_time,uav_3(range,9))
plot(sim_time,uav_3(range,10))



plot(sim_time,uav_4(range,5))
plot(sim_time,uav_4(range,6))
plot(sim_time,uav_4(range,7))
plot(sim_time,uav_4(range,8))
plot(sim_time,uav_4(range,9))
plot(sim_time,uav_4(range,10))





plot(sim_time,uav_5(range,6))
plot(sim_time,uav_5(range,7))
plot(sim_time,uav_5(range,8))
plot(sim_time,uav_5(range,9))
plot(sim_time,uav_5(range,10))


uav_6 = uavs_data(6,:,:);
uav_6 = reshape(uav_6, [ size(uav_6,2) size(uav_6,3) ])';

plot(sim_time,uav_6(range,7))
plot(sim_time,uav_6(range,8))
plot(sim_time,uav_6(range,9))
plot(sim_time,uav_6(range,10))


uav_7 = uavs_data(7,:,:);
uav_7 = reshape(uav_7, [ size(uav_7,2) size(uav_7,3) ])';


plot(sim_time,uav_7(range,8))
plot(sim_time,uav_7(range,9))
plot(sim_time,uav_7(range,10))


uav_8 = uavs_data(8,:,:);
uav_8 = reshape(uav_8, [ size(uav_8,2) size(uav_8,3) ])';


plot(sim_time,uav_8(range,9))
plot(sim_time,uav_8(range,10))



uav_9 = uavs_data(9,:,:);
uav_9 = reshape(uav_9, [ size(uav_9,2) size(uav_9,3) ])';


plot(sim_time,uav_9(range,10))
title('Conflict Separation Distance')
ylabel('||x_i - x_j||_2')
xlabel('Time(s)')
filename = 'uavs_host_intruders.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));

