load('ten_uavs_token_allocation_results.mat')
load('ten_uavs_conflict_nodes.mat')
load('ten_uavs_sim_time.mat')

figure
conflict_nodes  = sum(ten_uavs_conflict_nodes.data);
uav_ids = 1:size(conflict_nodes,2);
stem(uav_ids, conflict_nodes(1,uav_ids),'-r')
grid on
title('Sampled Conflict Nodes per UAV')
legend('conflict count')
xlabel('UAV IDs')
ylabel('Number of Conflict Nodes')
filename = 'uavs_sampled_conflict_counts.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off


sim_time = ten_uavs_sim_time.data;
tokens_data = ten_uavs_token_allocation_results.data;
tokens = tokens_data(:,1);

figure

conflict_nodes = tokens_data(:,2);
stem(tokens,conflict_nodes,'-r')
grid on
title('Token UAV Conflict Nodes')
legend('conflict nodes count')
xlabel('Token(n)')
ylabel('Number of Conflict Nodes')
filename = 'uavs_token_conflict_counts.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off

figure

terminal_costs = tokens_data(:,3);
stem(tokens,terminal_costs,'-r')
grid on
title('Token UAV Terminal Costs')
legend('UAV Terminal Costs')
xlabel('Token(n)')
ylabel('Terminal Costs')
filename = 'uavs_token_terminal_costs.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off


figure

transitional_costs = tokens_data(:,4);
stem(tokens,transitional_costs,'-r')
grid on
title('Token UAV Transition Costs')
legend('UAV Transition Costs')
xlabel('Token(n)')
ylabel('Trasition Costs')
filename = 'uavs_token_transition_costs.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off


figure

total_costs = transitional_costs + terminal_costs;
stem(tokens,total_costs,'-r')
grid on
title('Token UAV Total Cost(J)')
legend('UAV Total Cost')
xlabel('Token(n)')
ylabel('Trasition Costs')
filename = 'uavs_token_total_costs.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off
