function path_plan_track = read_plan_tracker(uav_id,plan_track)
	global plan_tracker
	global token_value
	global solution_path_length
	global conflict_states
	global path_plan
	current_plan_track = plan_track(uav_id);
	uav_conflict_state = conflict_states(uav_id,:);
	if(ismember(uav_id, token_value))
		%current_plan_track
		if(current_plan_track <= size(path_plan, 2))
			if(current_plan_track > 0 )
				plan_tracker(uav_id) = current_plan_track;
			else
				if(current_plan_track == -1 )
					plan_tracker(uav_id) = 0;
					solution_path_length(uav_id) = 0;
					token_value = token_value(token_value ~= uav_id);
					path_plan(uav_id,:) = 0;
				end
			end
		
		end
	end
	path_plan_track = plan_tracker;

end
