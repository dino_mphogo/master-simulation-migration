function interrupt_state = increasing_traffic_intensity(uav_id, current_conflict_count)
	% this function inturrupts conflict resolution path planning algorithm plan, 
	% if there is one currently executing when number of intruders in conflict 
	% increases, that is the traffic intensity.
	global number_of_intruders
	interrupt_state = 0;
	if (current_conflict_count > number_of_intruders(uav_id))
		interrupt_state = 1;
	end
	
end
