
clc;



% insert ballistic missile
data = zeros(100,6);
data(:,1) = linspace(4000,4000);
data(:,3) = linspace(5000,5000);
data(:,5) = 90;
uav_1_data = data;
 new_object('aircraft_1.mat',data,... 
'model','f-16.mat','scale',80,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1); 



% insert ballistic missile
data = zeros(100,6);
data(:,1) = linspace(4000,4000);
data(:,3) = linspace(7000,7000);
data(:,5) = 90;
uav_2_data = data;
 new_object('aircraft_2.mat',data,... 
'model','f-16.mat','scale',80,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1); 


data = zeros(100,6);
data(:,1) = linspace(2000,2000);
data(:,3) = linspace(8000,8000);
data(:,5) = 90;
uav_1_data = data;
 new_object('aircraft_6.mat',data,... 
'model','f-16.mat','scale',80,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1); 



% insert ballistic missile
data = zeros(100,6);
data(:,1) = linspace(2000,2000);
data(:,3) = linspace(4500,4500);
data(:,5) = 90;
uav_2_data = data;
 new_object('aircraft_7.mat',data,... 
'model','f-16.mat','scale',80,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1);


% insert ballistic missile
data = zeros(100,6);
data(:,1) = linspace(4000,4000);
data(:,3) = linspace(6000,6000);
data(:,5) = 90;
uav_2_data = data;
 new_object('aircraft_4.mat',data,... 
'model','f-16.mat','scale',80,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1);


% insert ballistic missile
data = zeros(100,6);
data(:,1) = linspace(14000,14000);
data(:,3) = linspace(6000,6000);
data(:,5) = -90;
uav_2_data = data;
 new_object('aircraft_3.mat',data,... 
'model','f-16.mat','scale',80,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1); 

 


data = zeros(100,6);
data(:,1) = linspace(16000,16000);
data(:,3) = linspace(8000,8000);
data(:,5) = -90;
uav_2_data = data;
 new_object('aircraft_5.mat',data,... 
'model','f-16.mat','scale',80,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1); 


% insert ballistic missile
data = zeros(100,6);
data(:,1) = linspace(16000,16000);
data(:,3) = linspace(4500,4500);
data(:,5) = -90;
uav_2_data = data;
 new_object('aircraft_8.mat',data,... 
'model','f-16.mat','scale',80,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1); 

% insert ballistic missile
data = zeros(100,6);
data(:,1) = linspace(14000,14000);
data(:,3) = linspace(5000,5000);
data(:,5) = -90;
uav_2_data = data;
 new_object('aircraft_9.mat',data,... 
'model','f-16.mat','scale',80,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1); 

% insert ballistic missile
data = zeros(100,6);
data(:,1) = linspace(14000,14000);
data(:,3) = linspace(7000,7000);
data(:,5) = -90;
uav_2_data = data;
 new_object('aircraft_10.mat',data,... 
'model','f-16.mat','scale',80,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.89 .0 .27],'pathwidth',1);

flypath('aircraft_1.mat', ...	
'animate','on','step',50,... 
'font','Georgia','fontsize',10,... 
'view',[0 0],'window',[700 700],... 
'xlim',[0 17000],'ylim',[-500 3500],'zlim',[0 10000]);
 





