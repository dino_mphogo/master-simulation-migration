% prepare environment 
clear all;
close all;
clc; 


data = zeros(100,6);
data(:,2) = linspace(1000,1000);
data(:,3) = linspace(1000,1000);
 new_object('aircraft_conf.mat',data,... 
'model','f-16.mat','scale',30,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.1 .0 .27],'pathwidth',1); 

data_ = zeros(100,6);
data_(:,2) = linspace(3000,3000);
data_(:,3) = linspace(1300,1300);
data_(:,5) = -135;
 new_object('aircraft_conf_1.mat',data_,... 
'model','f-16.mat','scale',30,... 
'edge',[0 0 0],'face',[0 0 0],'alpha',1,... 
'path','on','pathcolor',[.1 .0 .27],'pathwidth',1); 


flypath('aircraft_conf.mat', 'aircraft_conf_1.mat',... 
'animate','off','output','none','step',1000,... 
'font','Georgia','fontsize',10,... 
'view',[90 0],'xlim',[0 12000],'ylim',[0 4000],'zlim',[0 2500]); 

for index = 1:1000:size(data,1)
	
	%plot3(linspace(data(index,1), data(index,1),650*2+1 ), linspace(data(index,2), data(index,2), 650*2+1), data(index,3)-650:1:data(index,3)+650, '-r')
	%plot3(linspace(data(index,1), data(index,1),3077), data(index,2)-3076.12*0.5:1:data(index,2)+3076.12*0.5 ,linspace(data(index,3), data(index,3), 3077), '-r')
	%text(0,data(index,2),data(index,3)+300,['\Delta h = ', char(10), '650 Feet'],'FontName','Georgia','FontSize',10);
	%text(0,data(index,2)+350,data(index,3),['\Delta x = ',char(10),'1538 Feet'],'FontName','Georgia','FontSize',10);
end
%filename = 'aircraft_conflict_zone_thresholds.png';
%eval(sprintf('print -dpng -r600 %s;',filename));

