load('ten_uavs_sim_time.mat')
load('ten_uavs_token_allocation_results.mat')
load('ten_uavs_conflict_nodes.mat')
load('ten_uavs_sim_time.mat')
load uav_1_position_data.mat
load uav_2_position_data.mat
load uav_3_position_data.mat
load uav_4_position_data.mat
load uav_5_position_data.mat
load uav_1_flight_data.mat
load uav_2_flight_data.mat
load uav_3_flight_data.mat
load uav_4_flight_data.mat
load uav_5_flight_data.mat

load uav_6_position_data.mat
load uav_7_position_data.mat
load uav_8_position_data.mat
load uav_9_position_data.mat
load uav_10_position_data.mat
load uav_6_flight_data.mat
load uav_7_flight_data.mat
load uav_8_flight_data.mat
load uav_9_flight_data.mat
load uav_10_flight_data.mat

% Distribution of deviation from nominal path

altitude = [abs(uav_1_position_data.data(:,3) - uav_1_position_data.data(1,3)) abs(uav_2_position_data.data(:,3) - uav_2_position_data.data(1,3))  abs(uav_3_position_data.data(:,3) - uav_3_position_data.data(1,3))  abs(uav_4_position_data.data(:,3) - uav_4_position_data.data(1,3))  abs(uav_5_position_data.data(:,3) - uav_5_position_data.data(1,3))  abs(uav_6_position_data.data(:,3) - uav_6_position_data.data(1,3))  abs(uav_7_position_data.data(: ,3) - uav_7_position_data.data(1,3))  abs(uav_8_position_data.data(:,3) - uav_8_position_data.data(1,3))  abs(uav_9_position_data.data(:,3) - uav_9_position_data.data(1,3)) abs(uav_10_position_data.data(:,3) - uav_10_position_data.data(1,3)) ]; 
altitude = reshape(altitude, [size(altitude,1)*size(altitude,2) 1]);
altitude = altitude(find(altitude > 0));

tokens_data = ten_uavs_token_allocation_results.data;
tokens = tokens_data(:,1);





figure
conflict_nodes  = ten_uavs_conflict_nodes.data;
conflict_nodes = reshape(conflict_nodes, [size(conflict_nodes,1)*size(conflict_nodes,2) 1]);
conflict_nodes = conflict_nodes(find(conflict_nodes > 0));
histogram(conflict_nodes)
grid on
title('Sampled Conflict Nodes')
xlabel('Sampled Conflict Nodes')
ylabel('Frequency')
legend(['n = ', num2str(size(conflict_nodes, 1))])
filename = 'uavs_sampled_conflict_counts.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off

sim_time = ten_uavs_sim_time.data;
tokens_data = ten_uavs_token_allocation_results.data;
tokens = tokens_data(:,1);

figure

conflict_nodes = tokens_data(:,2);
%stem(tokens,conflict_nodes,'-r')
histogram(conflict_nodes)
grid on
title('Tokens Conflict Nodes')
legend(['n = ', num2str(size(conflict_nodes, 1))])
xlabel('Conflict Nodes')
ylabel('Frequency')
filename = 'uavs_token_conflict_counts.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off

figure

terminal_costs = tokens_data(:,3);
%stem(tokens,terminal_costs,'-r')
[f,xi] = ksdensity(terminal_costs); 
plot(xi,f);
%hold on
%histogram(terminal_costs)
grid on
title('Tokens Terminal Costs')
legend(['n = ', num2str(size(terminal_costs, 1))])
xlabel('Terminal Cost')
ylabel('Density')
filename = 'uavs_token_terminal_costs.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off


figure 
histogram(terminal_costs)
xlabel('Terminal Cost')
ylabel('Frequency')
legend(['n = ', num2str(size(terminal_costs, 1))])
grid on
filename = 'uavs_token_terminal_costs_1.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));



figure

transitional_costs = tokens_data(:,4);
[f,xi] = ksdensity(transitional_costs); 
plot(xi,f);
%hold on
%histogram(transitional_costs)
grid on
title('Tokens Transition Costs')
legend(['n = ', num2str(size(transitional_costs, 1))])
xlabel('Transition Costs')
ylabel('Density')
filename = 'uavs_token_transition_costs.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off


figure 
histogram(transitional_costs)
xlabel('Transitional Cost')
ylabel('Frequency')
legend(['n = ', num2str(size(transitional_costs, 1))])
grid on
filename = 'uavs_token_transition_costs_1.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));



