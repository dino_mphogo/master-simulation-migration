planning_uavs_indicators = csvread('historical_path_planning_uavs.txt');

hold on


load('uav_1_position_data.mat'); 
uav_1_position_data = uav_1_position_data.data(150000:250000,:);
data = [uav_1_position_data(:,1:3)  asin(uav_1_position_data(:,4)/max(uav_1_position_data(:,5))) ones(size(uav_1_position_data(:,1),1),1)*90 zeros(size(uav_1_position_data(:,1),1),1) ];
new_object('air_1.mat',data,... 
'model','f-16.mat', 'scale',50,'edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.3 .3 .3],'pathwidth',1); 
 
% insert friendly aircraft 
load('uav_2_position_data.mat'); 
uav_2_position_data = uav_2_position_data.data(150000:250000,:);
 data = [uav_2_position_data(:,1:3)  asin(uav_2_position_data(:,4)/max(uav_2_position_data(:,5))) ones(size(uav_2_position_data(:,1),1),1)*90 zeros(size(uav_2_position_data(:,1),1),1)];
new_object('air_2.mat',data,... 
'model','f-16.mat','scale',50,'edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.89 .27 .0],'pathwidth',1); 
 
 
 % insert friendly aircraft 
load('uav_3_position_data.mat'); 
uav_3_position_data = uav_3_position_data.data(150000:250000,:);
 data = [uav_3_position_data(:,1:3)  asin(uav_3_position_data(:,4)/max(uav_3_position_data(:,5))) ones(size(uav_3_position_data(:,1),1),1)*90 zeros(size(uav_3_position_data(:,1),1),1)];
new_object('air_3.mat',data,... 
'model','f-16.mat','scale',50,'edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.0 .5 .1],'pathwidth',1); 

 % insert friendly aircraft 
load('uav_4_position_data.mat'); 
uav_4_position_data = uav_4_position_data.data(150000:250000,:);
 data = [uav_4_position_data(:,1:3)  asin(uav_4_position_data(:,4)/max(uav_4_position_data(:,5))) ones(size(uav_4_position_data(:,1),1),1)*90 zeros(size(uav_4_position_data(:,1),1),1)];
new_object('air_4.mat',data,... 
'model','f-16.mat','scale',50,'edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.5 .5 .1],'pathwidth',1); 


 % insert friendly aircraft 
load('uav_5_position_data.mat'); 
uav_5_position_data = uav_5_position_data.data(150000:250000,:);
 data = [uav_5_position_data(:,1:3)  asin(uav_5_position_data(:,4)/max(uav_5_position_data(:,5))) ones(size(uav_5_position_data(:,1),1),1)*90 zeros(size(uav_5_position_data(:,1),1),1)];
new_object('air_5.mat',data,... 
'model','f-16.mat','scale',50,'edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.6 0 .1],'pathwidth',1); 


load('uav_6_position_data.mat'); 
uav_6_position_data = uav_6_position_data.data(150000:250000,:);
data = [uav_6_position_data(:,1:3)  asin(uav_6_position_data(:,4)/max(uav_6_position_data(:,5))) ones(size(uav_6_position_data(:,1),1),1)*90 zeros(size(uav_6_position_data(:,1),1),1) ];
new_object('air_6.mat',data,... 
'model','f-16.mat', 'scale',50,'edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.3 .3 .3],'pathwidth',1); 
 
% insert friendly aircraft 
load('uav_7_position_data.mat'); 
uav_7_position_data = uav_7_position_data.data(150000:250000,:);
 data = [uav_7_position_data(:,1:3)  asin(uav_7_position_data(:,4)/max(uav_7_position_data(:,5))) ones(size(uav_7_position_data(:,1),1),1)*90 zeros(size(uav_7_position_data(:,1),1),1)];
new_object('air_7.mat',data,... 
'model','f-16.mat','scale',50,'edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.89 .27 .0],'pathwidth',1); 
 
 
 % insert friendly aircraft 
load('uav_8_position_data.mat'); 
uav_8_position_data = uav_8_position_data.data(150000:250000,:);
 data = [uav_8_position_data(:,1:3)  asin(uav_8_position_data(:,4)/max(uav_8_position_data(:,5))) ones(size(uav_8_position_data(:,1),1),1)*90 zeros(size(uav_8_position_data(:,1),1),1)];
new_object('air_8.mat',data,... 
'model','f-16.mat','scale',50,'edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.0 .5 .1],'pathwidth',1); 

 % insert friendly aircraft 
load('uav_9_position_data.mat'); 
uav_9_position_data = uav_9_position_data.data(150000:250000,:);
 data = [uav_9_position_data(:,1:3)  asin(uav_9_position_data(:,4)/max(uav_9_position_data(:,5))) ones(size(uav_9_position_data(:,1),1),1)*90 zeros(size(uav_9_position_data(:,1),1),1)];
new_object('air_9.mat',data,... 
'model','f-16.mat','scale',50,'edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.5 .5 .1],'pathwidth',1); 


 % insert friendly aircraft 
load('uav_10_position_data.mat'); 
uav_10_position_data = uav_10_position_data.data(150000:250000,:);
 data = [uav_10_position_data(:,1:3)  asin(uav_10_position_data(:,4)/max(uav_10_position_data(:,5))) ones(size(uav_10_position_data(:,1),1),1)*90 zeros(size(uav_10_position_data(:,1),1),1)];
new_object('air_10.mat',data,... 
'model','f-16.mat','scale',50,'edge',[.2 .2 .2],'face',[.3 .3 .3],... 
'path','on','pathcolor',[.6 0 .1],'pathwidth',1); 

 
% generate the scene and save the result as png file 
flypath('air_1.mat','air_2.mat', 'air_3.mat','air_4.mat', 'air_5.mat','air_10.mat','air_6.mat', 'air_7.mat','air_8.mat', 'air_9.mat',... 
'animate','off','step',3000,... 
'font','Georgia','fontsize',10,... 
'view',[0 0],'window',[1000 800],... 
'xlim',[150000 480000],'ylim',[-500 3500],'zlim',[0 9000]);

for row_count = 2:size(planning_uavs_indicators,1)
	uav_id = planning_uavs_indicators(row_count,1);
	uav_conflict_nodes = csvread(['sampled_conflict_nodes_', num2str(uav_id), '_', num2str(planning_uavs_indicators(row_count,2)),'.txt']);
	uav_path = csvread(['path_plan_uav_',num2str(uav_id),'_',num2str(planning_uavs_indicators(row_count,2)) ,'.txt']);
	
	scatter3(uav_path(:,1),uav_path(:,2),uav_path(:,3),'b', 'filled');
end

filename = 'ten_uavs_animation.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
