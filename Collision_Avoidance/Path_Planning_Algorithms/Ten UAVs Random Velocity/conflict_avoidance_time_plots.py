import matplotlib.pyplot as plt
import numpy as np
from pylab import *
from scipy import stats
import pandas as pd
import scipy.special as sps
import pylab
from scipy.stats import gamma
import math
from scipy.stats import geom


system_effeciency = []
for line in open('path_planning_effeciency.txt', 'r'):
	line = float(line.replace('\n',''))
	if line > 0 and line < 100:
		system_effeciency.append(line)

		 
uavs_distance = []
for line in open('range_path_planning.txt', 'r'):
  line = line.replace("[", "")
  line = line.replace("]", "")
  line = line.replace("inf", "")
  line = line.replace("Nan", "")
  line = line.replace("Inf", "")
  line = line.replace("NaN", "")
  unwanted = ["inf","Nan", "Inf", "NaN"]
  for item in unwanted:
	if item in line:
		print item, " was found in the data"

  uavs_distance = line.split(';')

uavs_distance = np.array([map(float, item.split(' ')) for item in uavs_distance])

uavs_distance_plt = np.array([])
for uav in range(0, uavs_distance.shape[0]):
	uav_dist = uavs_distance[uav,:]
	grads =  np.gradient(uav_dist)
	positions = []
	for index in range(0,len(grads)-1):	
		if grads[index] * grads[index+1] < 0:
			positions.append(index)
	uavs_distance_plt = np.concatenate((uavs_distance_plt,np.array(uav_dist[positions])),axis = 0)

# for local minima

fig, ax11 = plt.subplots(figsize=(8,4))
uavs_distance = np.array(uavs_distance)
uavs_distance = uavs_distance.flatten()
uavs_distance = np.array(uavs_distance, dtype=float)
uavs_distance = uavs_distance[uavs_distance > 0]
dmod_threshold = uavs_distance[uavs_distance < 8000]
dmod_threshold = dmod_threshold [dmod_threshold   > 2500]
vertical_threshold = uavs_distance[uavs_distance < 1538.06]



#uavs_distance = np.sort(uavs_distance, axis= None)
#uavs_distance = uavs_distance[1:500000]

plt.boxplot(uavs_distance)
plt.title('Separation Distance (N = %d)' %len(uavs_distance))
plt.ylabel('Separation Distance(feet)')
plt.legend()
plt.grid(True)

fig, ax12 = plt.subplots(figsize=(8,4))
sample = stats.kde.gaussian_kde(vertical_threshold)
x = np.arange(0,max(vertical_threshold),0.1)
plt.plot(x,sample(x),label='Separation Distance')
plt.hist(vertical_threshold, normed=True, color='#0504aa')
plt.title('Separation Distance $X_{Thr}$ (N = %d)' %len(vertical_threshold))
plt.xlabel('Separation Distance(feet)')
plt.ylabel('Density')
plt.legend()
plt.grid(True)

fig, ax13 = plt.subplots(figsize=(8,4))
sample = stats.kde.gaussian_kde(dmod_threshold)
x = np.arange(0,max(dmod_threshold),0.1)
plt.plot(x,sample(x),label='Separation Distance')
plt.hist(dmod_threshold, normed=True, color='#0504aa')
plt.title('Separation Distance DMOD(N = %d)' %len(dmod_threshold))
plt.xlabel('Separation Distance(feet)')
plt.ylabel('Density')
plt.legend()
plt.grid(True)


'''
uavs_list = []
uavs_conflict_times = []
uavs_terminal_cost = []
uavs_transional_cost = []
intruders_count = []
tokens_list  = {}
conflict_nodes_count = []
cost_function_growth = {}
last_sample_time = 0
for line in open('path_planning_statistics.txt', 'r'):
  line = line.replace("\n", "")
  line = line.replace("inf", "")
  line = line.replace("Nan", "")
  line = line.replace("Inf", "")
  line = line.replace("NaN", "")
  #print line
  unwanted = ["inf","Nan", "Inf", "NaN"]
  for item in unwanted:
	if item in line:
		print item, " was found in the data"
  conflict = line.split(' ')
  if conflict[0] != '':
  	conflict_time = conflict[4]
  	# print conflict_time
  	conflict_nodes_count.append(int(conflict[1]))
  	uavs_terminal_cost.append(float(conflict[2]))
  	uavs_transional_cost.append(float(conflict[3]))
  	uavs_conflict_times.append(conflict[4].split(':'))
  	if conflict[4].split(':')[0] not in tokens_list.keys():
  		tokens_list[conflict[4].split(':')[0]] =  [conflict[0]]
  	else:
  		tokens_list[conflict[4].split(':')[0]] = tokens_list[conflict[4].split(':')[0]] + [conflict[0]]
  	if conflict[4] not in cost_function_growth.keys():
  		cost_function_growth[conflict[4]] = float(conflict[2])
		
  	else:
  		cost_function_growth[conflict[4]] = max(cost_function_growth[conflict[4]],float(conflict[2]))

  	intruders_count.append(int(conflict[5]))



conflict_t  = []
for line in open("conflict_time_path_planning.txt", 'r'):
	line = line.replace("[","")
	line = line.replace("]","")
 	line = line.replace("inf", "")
  	line = line.replace("Nan", "")
  	line = line.replace("Inf", "")
  	line = line.replace("NaN", "")
	line = line.split(";")
	unwanted = ["inf","Nan", "Inf", "NaN"]
	for item in unwanted:
		if item in line:
			print item, " was found in the data"
	conflict_t = line

	
conflict_status =  []
conflict_times_matrix = []
for line in open('conflict_path_planning.txt', 'r'):
  line = line.replace("[", "")
  line = line.replace("]", "")
  line = line.replace("inf", "")
  line = line.replace("Nan", "")
  line = line.replace("Inf", "")
  line = line.replace("NaN", "")
  unwanted = ["inf","Nan", "Inf", "NaN"]
  for item in unwanted:
	if item in line:
		print item, " was found in the data"
  
  #print line
  conflict = line.split(';')
  for uav_line in conflict:
  	conflict_ = uav_line.split(' ')
  	if conflict_[0] != '':
  		try:
  			first_index = conflict_.index('1')-1
  			last_index = len(conflict_) -  conflict_[::-1].index('1')-1
  			conflict_status.append(conflict_[first_index:last_index])
  		
  		except:
  			continue
  			#print "No conflict was detected!"


times_dict = {}
for c_time in uavs_conflict_times:
	if times_dict.get(c_time[0]) != None: 
		times_dict[c_time[0]] = times_dict.get(c_time[0]) + ["".join(c_time[1:])]
	else:
		times_dict[c_time[0]] =  ["".join(c_time[1:])]


def unique(list1): 
  
    # intilize a null list 
    unique_list = [] 
      
    # traverse for all elements 
    for x in list1: 
        # check if exists in unique_list or not 
        if x not in unique_list: 
            unique_list.append(x)
    return unique_list

def hour_data_split(hour):
	hour =  map(int,hour)
	hour = sort(hour)
	step_size = 20
	separate_hours = []
	hour_temp = []
	for index  in range(len(hour)):
		hour_temp.append(hour[index])
		if index < len(hour) - 1:
			if abs(hour[index] - hour[index+1]) > step_size:
				separate_hours.append(hour_temp)
				hour_temp = []
			
		else:
			separate_hours.append(hour_temp)
	return separate_hours

	
#hour_list = map(int, times_dict.keys())
hour_list = sort(times_dict.keys())
hours = []		
hour_temp = []
tokens_pos  = []
# print tokens_list
for index in range(len(hour_list)):
	hour_new = hour_data_split(times_dict.get(str(hour_list[index])))
	if len(hour_new) > 1:
		for hour in hour_new:
			hour_ = unique(hour)
			# print 'new ', len(hour_)
			if len(hour_) > 1:
				hours.append(hour_)
	else:
		hour_ = unique(hour_new[0])
		if len(hour_) > 1:
			hours.append(hour_)


def tokens_processing(Tokens):
	tokens_dict = {}
	count = 0
	window_size = 3
	index  = 0
	for index in range(2, len(Tokens)):
		if Tokens[index-2:index].count(Tokens[index]) > 1 and index != len(Tokens)-1 :
			count += 1
		else:
			if Tokens[index]  not in tokens_dict.keys():
				if count > 1:
					tokens_dict[Tokens[index]] = [count]
			else:
				if count > 1:
					tokens_dict[Tokens[index]]  = tokens_dict[Tokens[index]]  + [count]
			count = 1
	
	return tokens_dict
'''	
# tokens_dicts = tokens_processing(tokens_list)

'''

time_list = np.unique(tokens_list.keys())
fig, ax = plt.subplots(figsize=(8,4))
n = 0
X = []
geometric  = []
for conf_time in time_list:
	uav_list =  tokens_list[conf_time]
	p = 1.0/len(np.unique(uav_list))
	uav_geometric = []
	uav_X = []
	for uav in uav_list:
		x = uav_list.count(uav)
		X.append(x)
		uav_X.append(x)
		uav_geometric.append(((1 - p)**(x -1))*p)
		geometric.append(((1 - p)**(x -1))*p)
		#geometric = []
	
	plt.plot(uav_X, uav_geometric)
plt.scatter(X, geometric)
plt.title('Path Replanning Distribution N = %d' %len(X))
plt.ylabel('Probability')
plt.xlabel('Number of Replanning')
plt.grid(True)
'''

		
'''		
	if index == len(hour_list) - 1:
		hour_temp = hour_temp + times_dict.get(str(hour_list[index]))
		if len(hour_temp) > 0:
			hours.append(hour_temp)
	else:
		if abs(int(hour_list[index]) - int(hour_list[index+1])) < 1:
			# times_dict.get(str(hour_list[index]))
			hour_temp = hour_temp + times_dict.get(hour_list[index])
			
		else:
			if len(hour_temp) > 0:
				hours.append(hour_temp)
			hour_temp = times_dict.get(str(hour_list[index]))
'''

#hours = [map(int, hour) for hour in hours]
'''
fig, ax = plt.subplots(figsize=(8,4))
# Poission distribution of number of collision in 60 seconds
number_of_collisions = []
n = np.arange(0,20)
gradients = []
for hour in hours:
	hour_ = np.array(hour) - min(hour)
	x = np.linspace(0, max(hour_), len(hour_))
	y = hour_
	slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
	plt.plot(n, stats.poisson.pmf(n, sum(np.gradient(hour_))/len(hour_)))
	gradients.append(sum(np.gradient(hour_))/len(hour_))
plt.title('Poisson Distribution of Conflicts $\lambda$ = %d' % (sum(gradients)/len(gradients)) )
plt.ylabel('Probability of a Collision')
plt.xlabel('Number of Conflict')
plt.grid(True)


n = 1
intercepts = []
gradients = []
std_errs = []
for hour in hours:
	hour_ = np.array(hour) - min(hour)
	x = np.linspace(0, max(hour_), len(hour_))
	y = hour_
	slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
	intercepts.append(intercept)
	gradients.append(slope)
	std_errs.append(std_err)
	#plt.plot(x, y, 'o', label='sample %d' % n )
	#plt.plot(x, intercept + slope*x, 'r')
	n += 1

plt.title('Conflict Time Intervals  N = %d' %n)
plt.ylabel('Conflict Time Interval(s)')
plt.xlabel('Samples')
plt.grid(True)



unwanted = ['nan', 'inf']
std_errs  = [str(item) for item in std_errs]
intercepts = [str(item) for item in intercepts]
gradients = [str(item) for item in gradients]
for item in unwanted:
	print item
	if item in intercepts:
		while intercepts.count(item):
			intercepts.remove(item)
		
	if item in gradients:
		while gradients.count(item):
			gradients.remove(item)
		
	if item in std_errs:
		while std_errs.count(item):
			std_errs.remove(item)
	
std_errs  = map(float,std_errs)
intercepts = map(float, intercepts)
gradients = map(float, gradients)
'''

def remove_infs(input_list):
	new_list = []
	for index in range(0, len(input_list)):
		line_input = map(str, input_list[index])
		while line_input.count("inf") > 0:
			line_input.remove("inf")
		while line_input.count("nan") > 0:
			line_input.remove("nan")
		new_list = new_list + line_input
	return new_list


#conflict_timeline = np.array(map(float, remove_infs(hours)))
# conflict_timeline = conflict_timeline.flatten()
# conflict_timeline = conflict_timeline[conflict_timeline > 0]
'''
fig, ax13 = plt.subplots(figsize=(8,4))
plt.hist(conflict_timeline)
plt.title('Conflict Time Intervals Density')
plt.ylabel('Density')
plt.xlabel('Time(s)')
plt.grid(True)
#plt.show()


fig, ax1 = plt.subplots(figsize=(8,4))
itr = 1

for hour in hours:
	hour_ = np.array(hour) - min(hour)
	sample = stats.kde.gaussian_kde(hour_)
	x = np.arange(0,max(hour_),0.1)
	ax1.plot(x, sample(x),label='sample %d' % itr)
	itr += 1

plt.hist(hours)
plt.title('Conflict Time Intervals Density')
plt.ylabel('Density')
plt.xlabel('Time(s)')
plt.grid(True)





fig, ax2 = plt.subplots(figsize=(8,4))
intercepts = np.array(intercepts)
intercepts_mean = intercepts.mean()
sample_intercepts = stats.kde.gaussian_kde(intercepts)
x = np.arange(0,max(intercepts),0.1)

ax2.plot(x, sample_intercepts(x),label='conflict avoidance times')
plt.title('Conflict Time Intervals (N = %d, Mean = %d' %(n, intercepts_mean) )
plt.ylabel('Density')
plt.xlabel('Conflict Time Interval(s)')
plt.legend()
plt.grid(True)


fig, ax3 = plt.subplots(figsize=(8,4))
gradients =  np.array(gradients)
gradients_mean = gradients.mean()
sample_gradients = stats.kde.gaussian_kde(gradients)
x = np.arange(0,max(gradients),0.1)
ax3.plot(x, sample_gradients(x),label='conflict occurance times')
plt.title('Conflict Occurance Intervals (N = %d, Mean = %d)' %(n, gradients_mean))
plt.ylabel('Density')
plt.xlabel('Conflict Occurrance Rate(1/s)')
plt.legend()
plt.grid(True)

fig, ax4 = plt.subplots(figsize=(8,4))
sample_erros = stats.kde.gaussian_kde(std_errs)
x = np.arange(0,max(std_errs),0.1)
x1 = np.linspace(0, max(std_errs), len(std_errs))
ax4.plot(x,sample_erros(x) ,label='Conflict Time Standard Error')
plt.title('Conflict Times Standard Error (N = %d)' %n)
plt.ylabel('Density')
plt.xlabel('Conflict Times Standard Error(s)')
plt.legend()
plt.grid(True)




fig, ax5 = plt.subplots(figsize=(8,4))
sample_erros = stats.kde.gaussian_kde(std_errs)
x = np.arange(0,max(std_errs),0.1)
x1 = np.linspace(0, max(intercepts), len(intercepts))
#ax5.plot(x1,std_errs ,label='Conflict Time ')
ax5.errorbar(x1, intercepts, yerr=0.1)
plt.title('Conflict Times Standard Error (N = %d)' %n)
plt.ylabel('Density')
plt.xlabel('Samples(N)')
plt.legend()
plt.grid(True)
'''


'''
fig, ax8 = plt.subplots(figsize=(8,4))
sample = stats.kde.gaussian_kde(uavs_terminal_cost)
x = np.arange(0,max(uavs_terminal_cost),0.1)
plt.plot(x,sample(x),label='Terminal Cost')
plt.hist(uavs_terminal_cost, normed=True, color='#0504aa')
plt.title('Terminal Costs (N = %d)' %len(uavs_terminal_cost))
plt.ylabel('Density')
plt.xlabel('Terminal Costs')
plt.legend()
plt.grid(True)


fig, ax9 = plt.subplots(figsize=(8,4))
sample = stats.kde.gaussian_kde(uavs_transional_cost)
x = np.arange(0,max(uavs_transional_cost),0.1)
plt.plot(x,sample(x),label='Transition Cost ')
plt.hist(uavs_transional_cost, normed=True,  color='#0504aa',)
plt.title('Transition Costs (N = %d)' %len(uavs_transional_cost))
plt.ylabel('Density')
plt.xlabel('Transition Costs')
plt.legend()
plt.grid(True)

plt.show()

'''


'''


fig, ax13 = plt.subplots(figsize=(8,4))
sample = stats.kde.gaussian_kde(conflict_nodes_count)
x = np.arange(0,max(conflict_nodes_count),0.1)
#plt.plot(x,sample(x),label='Conflict Nodes')
plt.hist(conflict_nodes_count)
plt.title('Conflict Nodes (N = %d)' %len(conflict_nodes_count))
plt.ylabel('Number of Conflict Nodes(n)')
#plt.ylabel('Density')
plt.legend()
plt.grid(True)
'''

'''
fig, ax14 = plt.subplots(figsize=(8,4))
plt.plot(conflict_nodes_count, color='#0504aa')
plt.title('Conflict Nodes (N = %d)' %len(conflict_nodes_count))
plt.ylabel('Number of Conflict Nodes(n)')
plt.xlabel('Samples(n)')
plt.legend()
plt.grid(True)

cost_growth_func = [cost_function_growth[key] for key in cost_function_growth.keys()]
fig, ax15 = plt.subplots(figsize=(8,4))
plt.hist(cost_growth_func, color='#0504aa')
plt.title('Terminal Costs (N = %d)' %len(cost_growth_func))
plt.xlabel('Teminal Costs')
plt.ylabel('Frequency')
plt.legend()
plt.grid(True)


fig, ax9 = plt.subplots(figsize=(8,4))
plt.hist(np.gradient(uavs_transional_cost), color='#0504aa')
plt.title('Transition Costs Rate Distribution(N = %d)' %len(uavs_transional_cost))
plt.ylabel('Frequency')
plt.xlabel('Transition Costs')
plt.legend()
plt.grid(True)

cost_growth_rate = np.gradient([cost_function_growth[key] for key in cost_function_growth.keys()])
fig, ax15 = plt.subplots(figsize=(8,4))
plt.hist(cost_growth_rate, color='#0504aa')
plt.title('Terminal Cost Rate Distribution(N = %d)' %len(cost_growth_rate))
plt.xlabel('Teminal Costs Growth')
plt.ylabel('Frequency')
plt.legend()
plt.grid(True)



fig, ax15 = plt.subplots(figsize=(8,4))
plt.hist(np.gradient(intruders_count), color='#0504aa')
plt.title('Conflict Intruders Distribution(N = %d)' %len(conflict_nodes_count))
plt.xlabel('Number of Conflict Intruders(n)')
plt.ylabel('Frequency')
plt.legend()
plt.grid(True)
plt.show()
'''

'''
fig, ax15 = plt.subplots(figsize=(8,4))
plt.boxplot(system_effeciency)
plt.title('Conflict resolutions Effeciency(N = %d)' %len(system_effeciency))
#plt.xlabel('Planning Sample(n)')
plt.ylabel('Conflict Resolutions')
plt.legend()
plt.grid(True)
plt.show()


fig, ax16 = plt.subplots(figsize=(8,4))
for hour in hours:
	hour = hour - min(hour)
	plt.plot(hour, '-o')
plt.title('Conflict Intervals Times (N = %d)' %len(hours))
plt.xlabel('Planning Sample(n)')
plt.ylabel('Time(s)')
plt.legend()
plt.grid(True)

# let us try Binomial Distribution to answer the question
# What is the probability of a conflict within 60 seconds after a token allocation



fig, ax17 = plt.subplots(figsize=(8,4))
X = []
n = 60
p = 0.50
binomial = []
for hour in hours:
	hour = list(hour - min(hour))
	x = len(hour)

	binomial.append(stats.binom.pmf(x,n,p))
	X.append(x)
plt.plot(sort(X), sort(binomial))	
#plt.hist(np.gradient(hour))
plt.title('Conflicts Binomial Distribution (N = %d)' %len(hours))
plt.xlabel('Number of Planning')
plt.ylabel('Probability of a collision')
plt.legend()
plt.grid(True)

'''



plt.show()
