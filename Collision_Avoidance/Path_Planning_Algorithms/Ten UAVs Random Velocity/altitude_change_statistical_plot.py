import matplotlib.pyplot as plt
import numpy as np
from pylab import *
from scipy import stats
import scipy.special as sps
import pylab


def cluster_positions(altitude):
	positions = []
	pos = []
	for index in range(0, len(altitude)):
		if abs(altitude[index]) > 1 and abs(altitude[index-1]) < 1:
			if len(pos) != 2:
				pos.append(index)
			else:
				positions.append(pos)
				pos = []
				pos.append(index)
		if abs(altitude[index]) < 1 and len(pos) == 1:
			pos.append(index)
			positions.append(pos)
			pos = []
			
	return positions
	
	
def each_uav_altitude(altitude):
	for index in range(10):
		a = altitude[:][index]
		a = a[a > 1]
		plt.figure()
		sample = stats.kde.gaussian_kde(a)
		x = np.arange(0,max(a),0.1)
		plt.plot(x,sample(x),label='Altitude Deviations UAV %d' % (index + 1))
		plt.hist(a, normed=True,  color='#0504aa')
		plt.title('Altitude Deviations (N = %d)' %len(a))
		plt.ylabel('Density')
		plt.xlabel('Deviations(feet)')
		plt.legend()
		plt.grid(True)
		plt.savefig('uav_%d_altidute_deviations.png' % (index+1))
	plt.show()	
	exit()
		
		
def process_array(input_array):
	new_positions = [[],[],[],[],[],[],[],[],[],[]]
	for items in input_array:
		for indx in range(0,10):
			new_positions[indx].append(items[indx])
	return new_positions
def clusters(altitude_deviations_tcas):
	altitude_clusters_tcas = []
	for uavs in range(0, altitude_deviations_tcas.shape[0]):
		uav_positions = altitude_deviations_tcas[uavs]
		indices = cluster_positions(uav_positions)
		for index_pair in indices:
			x = index_pair[0]
			y = index_pair[1]
			altitude_clusters_tcas.append(max(abs(uav_positions[x:y])))
	return np.array(altitude_clusters_tcas)
	
count = 0
altitude_data = []
for line in open('altitude_change_path_planning.txt', 'r'):
  line = line.replace("[", "")
  line = line.replace("]", "")
  line = line.replace("Nan", "")
  line = line.replace("Inf", "")
  altitude = line.split(';')
  if altitude[0] != '':
  	n = 100000
  	altitude_data = np.array([map(float, item.split(' ')) for item in altitude])
  

'''  		
  	if len(altitude) > n:
  		altitude_data.append(altitude[0:n])
  	else:
  		 altitude_data.append(altitude)
  count += 1
'''

each_uav_altitude(np.array(process_array(altitude_data)))
uavs_altitudes = clusters(np.array(process_array(altitude_data)))

'''
if "" in altitude_data:
	while altitude_data.count(""):
		altitude_data.remove("")
'''
#altitude_data =  np.array(map(double, altitude_data))
#altitude_data = np.trim_zeros(altitude_data)
#altitude_data = np.around(altitude_data,3)
#altitude_data = altitude_data[altitude_data > 10]


fig, ax1 = plt.subplots(figsize=(8,4))
ax1.boxplot(uavs_altitudes)
plt.title('Altitude Deviations (N = %d)' %len(uavs_altitudes))
plt.ylabel('Altitude(feet)')
plt.legend()
plt.grid(True)


fig, ax2 = plt.subplots(figsize=(8,4))
sample = stats.kde.gaussian_kde(uavs_altitudes)
x = np.arange(0,max(uavs_altitudes),0.1)
plt.plot(x,sample(x),label='Altitude Deviations')
ax2.hist(uavs_altitudes, normed=True,  color='#0504aa')
plt.title('Altitude Deviations (N = %d)' %len(uavs_altitudes))
plt.ylabel('Density')
plt.xlabel('Altitude(feet)')
plt.legend()
plt.grid(True)



'''
fig, ax3 = plt.subplots(figsize=(8,4))
x = pylab.linspace(0, len(altitude_data), len(altitude_data))
ax3.hist(gamma(altitude_data), color='#0504aa')
plt.title('Altitude Deviations (N = %d)' %len(altitude_data))
plt.ylabel('Density')
plt.xlabel('Altitude(feet)')
plt.legend()
plt.grid(True)
#pylab.plot(x, gamma(x),  label='$\Gamma(x)$')



fig, ax5 = plt.subplots(figsize=(8,4))
sample = stats.kde.gaussian_kde(gamma(altitude_data))
a =  gamma(altitude_data)
x = np.arange(0,max(gamma(altitude_data)),0.1)
ax5.plot(x,sample(x),label='Altitude Deviations')
plt.title('Altitude Deviations (N = %d)' %len(gamma(altitude_data)))
plt.ylabel('Density')
plt.xlabel('Altitude(feet)')
plt.legend()
plt.grid(True)
'''
plt.show()

