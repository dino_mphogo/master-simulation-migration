[uav_ids, conflict_nodes, terminal_costs,transitional_cost, conflict_time, number_of_intruders] = textread('path_planning_statistics.txt', '%d %d %f %f %s %d', 5000);

figure
histogram(conflict_nodes)
grid on
title('Conflict Nodes Distribution')
xlabel('Number of Conflict Nodes')
ylabel('Frequency')
legend(['n = ', num2str(size(conflict_nodes, 1))])
filename = 'uavs_monte_carlo_conflict_counts.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off


figure
histogram(terminal_costs)
grid on
title('Terminal Costs Distributions')
xlabel('Terminal Costs')
ylabel('Frequency')
legend(['n = ', num2str(size(terminal_costs, 1))])
filename = 'uavs_monte_carlo_terminal_costs.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off



figure
histogram(transitional_cost)
grid on
title('Transitional Costs Distributions')
xlabel('Transitional Costs')
ylabel('Frequency')
legend(['n = ', num2str(size(transitional_cost, 1))])
filename = 'uavs_monte_carlo_transitional_costs.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off





figure
histogram(number_of_intruders)
grid on
title('Intruders Distributions')
xlabel('Intruders Counts')
ylabel('Frequency')
legend(['n = ', num2str(size(number_of_intruders, 1))])
filename = 'uavs_monte_carlo_intruders_count.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off
