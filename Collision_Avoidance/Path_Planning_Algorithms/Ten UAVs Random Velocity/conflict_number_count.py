import matplotlib.pyplot as plt
import numpy as np
from pylab import *
from scipy import stats
import pandas as pd
import scipy.special as sps
import pylab
from scipy.stats import gamma
import math
from scipy.stats import geom

uav_conflict_nodes_count = [[],[],[],[],[],[],[],[],[],[]]
row_data = []
for lines in open('search_tree_conflict_nodes.txt', 'r'):
	line_new = lines.replace("[", "")
  	line_new = line_new.split("]")
  	line_new = [item.replace("]", "") for item in line_new]
	for item in line_new:
		item_new = item.split(" ")
		row_data.append(item_new)
		if len(item_new) == len(uav_conflict_nodes_count):
  			for index in range(0, len(item_new)):
				uav_conflict_nodes_count[index].append(item_new[index])
'''

for item in uav_conflict_nodes_count:
	item = np.array(item)
	item = item[item > 0]
	if len(item) > 0:
		plt.boxplot(item)
plt.title('Conflict Nodes')
plt.ylabel('Tree Conflict of Nodes')
plt.xlabel('UAV ID')
plt.grid(True)
'''
uavs_data  = []
uavs_data_hist = []
uavs_data_max = []
uavs_data_min = []
fig, ax1 = plt.subplots(figsize=(8,4))
for uav in uav_conflict_nodes_count:
	uav_data = np.array(map(float, uav))
	uavs_data.append(list(uav_data[uav_data > 0]))
	uavs_data_hist = uavs_data_hist + list(uav_data[uav_data > 0])


for row_dt in row_data:
	if len(row_dt) == 10:
		uav_data_line = np.array(map(float, row_dt))
		if len(uav_data_line[uav_data_line > 0]) > 0:
			uavs_data_max.append(max(uav_data_line[uav_data_line > 0]))
		if len(uav_data_line[uav_data_line > 0]) > 0:
			uavs_data_min.append(min(uav_data_line[uav_data_line > 0]))
	else:
		print row_dt
plt.boxplot(uavs_data)
plt.title('Conflict Nodes Distribution')
plt.ylabel('Number of Conflict of Nodes')
plt.xlabel('UAV ID')
plt.grid(True)


f, axarr = plt.subplots(1, 2)
axarr[0].plot(uavs_data_max)
axarr[0].set_title('Maximum Conflict Nodes(N = %d)' %len(uavs_data_max))
axarr[0].grid(True)
axarr[1].plot(uavs_data_min)
axarr[1].set_title('Minimum Conflict Nodes(N = %d)' %len(uavs_data_min))
axarr[1].grid(True)
for ax in axarr.flat:
    ax.set(ylabel='Number of Conflict of Nodes', xlabel='')
'''  
fig, ax2 = plt.subplots(figsize=(8,4))
plt.boxplot(uavs_data_max)
plt.title('Maximum Conflict Nodes Distribution')
plt.ylabel('Number of Conflict of Nodes')
plt.grid(True)

fig, ax4 = plt.subplots(figsize=(8,4))
plt.boxplot(uavs_data_min)
plt.title('Minimum Conflict Nodes')
plt.ylabel('Number of Conflict of Nodes')

plt.grid(True)
'''
'''
fig, ax3 = plt.subplots(figsize=(8,4))
plt.plot(np.gradient(uavs_data_min))
plt.title('Conflict Nodes Rate')
plt.ylabel('Number of Conflict of Nodes')
plt.xlabel('Samples(n)')
plt.grid(True)

fig, ax5 = plt.subplots(figsize=(8,4))
plt.plot(np.gradient(uavs_data_max))
plt.title('Conflict Nodes Distribution')
plt.ylabel('Number of Conflict of Nodes')
plt.xlabel('Samples(n)')
plt.grid(True)
'''
f, axa = plt.subplots(1, 2)
axa[0].hist(uavs_data_min)
axa[0].set_title('Altitude Deviations TCAS(N = %d)' %len(uavs_data_min))
axa[0].grid(True)
axa[1].hist(uavs_data_max)
axa[1].set_title('Altitude Deviations CIF(N = %d)' %len(uavs_data_max))
axa[1].grid(True)
for ax in axa.flat:
    ax.set(ylabel='Nodes growth Rate', xlabel='Samples(n)')

plt.show()		

