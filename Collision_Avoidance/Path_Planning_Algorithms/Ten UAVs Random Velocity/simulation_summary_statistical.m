load('ten_uavs_range_monte_carlo.mat')
load('ten_uavs_sim_time.mat')
sim_time = ten_uavs_sim_time.data;
uavs_data = ten_uavs_range_monte_carlo.data;
load('ten_uavs_token_allocation_results.mat')
load('ten_uavs_conflict_nodes.mat')
load('ten_uavs_sim_time.mat')
load uav_1_position_data.mat
load uav_2_position_data.mat
load uav_3_position_data.mat
load uav_4_position_data.mat
load uav_5_position_data.mat
load uav_1_flight_data.mat
load uav_2_flight_data.mat
load uav_3_flight_data.mat
load uav_4_flight_data.mat
load uav_5_flight_data.mat

load uav_6_position_data.mat
load uav_7_position_data.mat
load uav_8_position_data.mat
load uav_9_position_data.mat
load uav_10_position_data.mat
load uav_6_flight_data.mat
load uav_7_flight_data.mat
load uav_8_flight_data.mat
load uav_9_flight_data.mat
load uav_10_flight_data.mat

% Distribution of deviation from nominal path

altitude = [abs(uav_1_position_data.data(:,3) - uav_1_position_data.data(1,3)) abs(uav_2_position_data.data(:,3) - uav_2_position_data.data(1,3))  abs(uav_3_position_data.data(:,3) - uav_3_position_data.data(1,3))  abs(uav_4_position_data.data(:,3) - uav_4_position_data.data(1,3))  abs(uav_5_position_data.data(:,3) - uav_5_position_data.data(1,3))  abs(uav_6_position_data.data(:,3) - uav_6_position_data.data(1,3))  abs(uav_7_position_data.data(:,3) - uav_7_position_data.data(1,3))  abs(uav_8_position_data.data(:,3) - uav_8_position_data.data(1,3))  abs(uav_9_position_data.data(:,3) - uav_9_position_data.data(1,3)) abs(uav_10_position_data.data(:,3) - uav_10_position_data.data(1,3)) ]'; 

%figure 
%scatter(sim_time,altitude)
% UAV 1 



uav_1_data = uavs_data(1,:,:);
uav_1_data  = reshape(uav_1_data, [size(uav_1_data, 2) size(uav_1_data ,3)])';
uav_1_data(:,1) = [];
uav_1_data(1,:) = [];

%uav_1_data = reshape(uav_1_data, [size(uav_1_data, 1)*size(uav_1_data ,2) 1]);

fig_1 = figure ; 

boxplot(uav_1_data)
xlabel('Intruder UAV ID')
ylabel('Separation Distance(feet)')
title('UAV 1 Separation Distance')
grid on
saveas(fig_1,['uav_1_separation_distance_stats.png']);




% UAV 2
uav_2_data = uavs_data(2,:,:);
uav_2_data  = reshape(uav_2_data, [size(uav_2_data, 2) size(uav_2_data ,3)])';
uav_2_data(:,2) = [];
uav_2_data(1,:) = [];

%uav_2_data = reshape(uav_2_data, [size(uav_2_data, 1)*size(uav_2_data ,2) 1]);

fig_1 = figure ;

boxplot(uav_2_data)
xlabel('Intruder UAV ID')
ylabel('Separation Distance(feet)')
title('UAV 2 Separation Distance')
grid on
saveas(fig_1,['uav_2_separation_distance_stats.png']);



% UAV 3
uav_3_data = uavs_data(3,:,:);
uav_3_data  = reshape(uav_3_data, [size(uav_3_data, 2) size(uav_3_data ,3)])';
uav_3_data(:,3) = [];
uav_3_data(1,:) = [];

%uav_3_data = reshape(uav_3_data, [size(uav_3_data, 1)*size(uav_3_data ,2) 1]);
fig_1 = figure ; 
boxplot(uav_3_data)
xlabel('Intruder UAV ID')
ylabel('Separation Distance(feet)')
title('UAV 3 Separation Distance')
grid on
saveas(fig_1,['uav_3_separation_distance_stats.png']);



% UAV 4
uav_4_data = uavs_data(4,:,:);
uav_4_data  = reshape(uav_4_data, [size(uav_4_data, 2) size(uav_4_data ,3)])';
uav_4_data(:,4) = [];
uav_4_data(1,:) = [];
%uav_4_data = reshape(uav_4_data, [size(uav_4_data, 1)*size(uav_4_data ,2) 1]);
fig_1 = figure ;
boxplot(uav_4_data)
xlabel('Intruder UAV ID')
ylabel('Separation Distance(feet)')
title('UAV 4 Separation Distance')
grid on
saveas(fig_1,['uav_4_separation_distance_stats.png']);



% UAV 5
uav_5_data = uavs_data(5,:,:);
uav_5_data  = reshape(uav_5_data, [size(uav_5_data, 2) size(uav_5_data ,3)])';
uav_5_data(:,5) = [];
uav_5_data(1,:) = [];
%uav_5_data = reshape(uav_5_data, [size(uav_5_data, 1)*size(uav_5_data ,2) 1]);
fig_1 = figure ;
boxplot(uav_5_data)
xlabel('Intruder UAV ID')
ylabel('Separation Distance(feet)')
title('UAV 5 Separation Distance')
grid on
saveas(fig_1,['uav_5_separation_distance_stats.png']);



% UAV 6
uav_6_data = uavs_data(6,:,:);
uav_6_data  = reshape(uav_6_data, [size(uav_6_data, 2) size(uav_6_data ,3)])';
uav_6_data(:,6) = [];
uav_6_data(1,:) = [];
%uav_6_data = reshape(uav_6_data, [size(uav_6_data, 1)*size(uav_6_data ,2) 1]);
fig_1 = figure ;
boxplot(uav_6_data)
xlabel('Intruder UAV ID')
ylabel('Separation Distance(feet)')
title('UAV 6 Separation Distance')
grid on
saveas(fig_1,['uav_6_separation_distance_stats.png']);



% UAV 7
uav_7_data = uavs_data(7,:,:);
uav_7_data  = reshape(uav_7_data, [size(uav_7_data, 2) size(uav_7_data ,3)])';
uav_7_data(:,7) = [];
uav_7_data(1,:) = [];
%uav_7_data = reshape(uav_7_data, [size(uav_7_data, 1)*size(uav_7_data ,2) 1]);
fig_1 = figure ;
boxplot(uav_7_data)
xlabel('Intruder UAV ID')
ylabel('Separation Distance(feet)')
title('UAV 7 Separation Distance')
grid on
saveas(fig_1,['uav_7_separation_distance_stats.png']);



% UAV 8
uav_8_data = uavs_data(8,:,:);
uav_8_data  = reshape(uav_8_data, [size(uav_8_data, 2) size(uav_8_data ,3)])';
uav_8_data(:,8) = [];
uav_8_data(1,:) = [];
%uav_8_data = reshape(uav_8_data, [size(uav_8_data, 1)*size(uav_8_data ,2) 1]);
fig_1 = figure ;
boxplot(uav_8_data)
xlabel('Intruder UAV ID')
ylabel('Separation Distance(feet)')
title('UAV 8 Separation Distance')
grid on
saveas(fig_1,['uav_8_separation_distance_stats.png']);
grid on



% UAV 9
uav_9_data = uavs_data(9,:,:);
uav_9_data  = reshape(uav_9_data, [size(uav_9_data, 2) size(uav_9_data ,3)])';
uav_9_data(:,9) = [];
uav_9_data(1,:) = [];
%uav_9_data = reshape(uav_9_data, [size(uav_9_data, 1)*size(uav_9_data ,2) 1]);
fig_1 = figure ;
boxplot(uav_9_data)
xlabel('Intruder UAV ID')
ylabel('Separation Distance(feet)')
title('UAV 9 Separation Distance')
grid on
saveas(fig_1,['uav_9_separation_distance_stats.png']);

 
% UAV 10
uav_10_data = uavs_data(10,:,:);
uav_10_data  = reshape(uav_10_data, [size(uav_10_data, 2) size(uav_10_data ,3)])';
uav_10_data(:,10) = [];
uav_10_data(1,:) = [];
%uav_10_data = reshape(uav_10_data, [size(uav_10_data, 1)*size(uav_10_data ,2) 1]);
fig_1 = figure ;
boxplot(uav_10_data)
xlabel('Intruder UAV ID')
ylabel('Separation Distance(feet)')
title('UAV 10 Separation Distance')
grid on
saveas(fig_1,['uav_10_separation_distance_stats.png']);




