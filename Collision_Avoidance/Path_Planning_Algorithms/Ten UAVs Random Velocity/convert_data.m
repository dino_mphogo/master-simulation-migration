load('ten_uavs_conflict_status_carlo.mat')
load('ten_uavs_sim_time.mat')
load('ten_uavs_token_allocation_results.mat')
load('ten_uavs_conflict_nodes.mat')
load('ten_uavs_range_monte_carlo.mat')
load uav_1_position_data.mat
load uav_2_position_data.mat
load uav_3_position_data.mat
load uav_4_position_data.mat
load uav_5_position_data.mat
load uav_6_position_data.mat
load uav_7_position_data.mat
load uav_8_position_data.mat
load uav_9_position_data.mat
load uav_10_position_data.mat
range_data = ten_uavs_range_monte_carlo.data;
sim_time = ten_uavs_sim_time.data(2:end,:);
uavs_data = ten_uavs_conflict_status_carlo.data;
conflict_status_linear = [];
range_data_linear = [];

for index = 1:size(uavs_data,1)
	uav_conflict = uavs_data(index,:,:);
	uav_range = range_data(index,:,:);
	uav_conflict  = reshape(uav_conflict , [size(uav_conflict ,2) size(uav_conflict,3) ]);
	uav_range  = reshape(uav_range, [size(uav_range ,2) size(uav_range,3) ]);
	temp_conflict = [];
	temp_range = [];
	for index_1 = 2:size(uavs_data,1)-1
		temp_conflict = [temp_conflict  uav_conflict(index_1, index_1:end)];
		short_dist = uav_range(index_1, index_1:end);
		short_dist = short_dist(short_dist < 10000);
		temp_range = [temp_range uav_range(index_1, index_1:end) ];
	end
	conflict_status_linear = [conflict_status_linear; temp_conflict];
	range_data_linear = [range_data_linear; temp_range];
end
stat_file = fopen('conflict_path_planning.txt','a');
fprintf(stat_file, [mat2str(conflict_status_linear),'\n']);
fclose(stat_file);


stat_file = fopen('range_path_planning.txt','a');
fprintf(stat_file, [mat2str(range_data_linear),'\n']);
fclose(stat_file);

stat_file = fopen('conflict_time_path_planning.txt','a');
fprintf(stat_file, [mat2str(ten_uavs_sim_time.data)]);
fclose(stat_file);

altitude = [abs(uav_1_position_data.data(:,3) - uav_1_position_data.data(1,3)) abs(uav_2_position_data.data(:,3) - uav_2_position_data.data(1,3)) abs(uav_3_position_data.data(:,3) - uav_3_position_data.data(1,3)) abs(uav_4_position_data.data(:,3) - uav_4_position_data.data(1,3)) abs(uav_5_position_data.data(:,3) - uav_5_position_data.data(1,3)) abs(uav_6_position_data.data(:,3) - uav_6_position_data.data(1,3)) abs(uav_7_position_data.data(:,3) - uav_7_position_data.data(1,3)) abs(uav_8_position_data.data(:,3) - uav_8_position_data.data(1,3)) abs(uav_9_position_data.data(:,3) - uav_9_position_data.data(1,3)) abs(uav_10_position_data.data(:,3) - uav_10_position_data.data(1,3))];

%altitude = reshape(altitude, [size(altitude,1)*size(altitude,2) 1]);
%altitude = altitude(find(altitude > 1));

stat_file = fopen('altitude_change_path_planning.txt','a');
fprintf(stat_file, [mat2str(altitude)]);
fclose(stat_file);   
