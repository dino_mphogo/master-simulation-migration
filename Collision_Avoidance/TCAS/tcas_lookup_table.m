function resolution = tcas_lookup_table(delta_h, tau, sensetivity_level)
TA_tau = [20 25 30 40 45 48 48];
RA_tau = [ -1 15 20 25 30 35 35];

% DMOD constants
TA_dmod = [0.30*1852 0.33*1852 0.48*1852 0.75*1852 1*1852 1.3*1852 1.3*1852]
RA_dmod = [-1 0.20*1852 0.35*1852 0.55*1852 0.80*1852 1.1*1852 1.1*1852];

% ZTHR
TA_zthr = [850*0.3048 850*0.3048 850*0.3048 850*0.3048 850*0.3048 850*0.3048 1200*0.3048 ];
RA_zthr = [-1 600*0.3048 600*0.3048 600*0.3048 600*0.3048 700*0.3048 800*0.3048];

% ALIM
RA_alim = [-1 300*0.3048 300*0.3048 300*0.3048 350*0.3048 400*0.3048 600*0.3048 700*0.3048];

% allowed rates
altitude_rates = [0 1500*0.3048/60 1500*0.3048/60 2000*0.3048/60 2500*0.3048/60 3000*0.3048/60];
tau_ra = RA_tau(sensetive_level);
tau_ta = RA_tau(sensetive_level); 

vertical_threshold = RA_dmod(sensentivity_level);
horizontal_threshold = RA_zthr(sensetivity_level);

r = 0:tau_ra/size(altitude_rates,2):tau_ta
h = 0::TA_z_threhold

if(tau > 0)
	resolution = delt_h/tau;
	
else
	resolution = 0; % levell off
end

