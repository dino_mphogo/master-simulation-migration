function tau = get_tau(x_delta,x_dot,y_delta,y_dot)
r = sqrt(x_delta^2+y_delta^);
r_dot = (x_delta*x_dot + y_delta*)/r;
tau = r/r_dot
if(closure_rate ~= 0)
    if(abs(range_value) < 0.48*1852 || abs(closure_rate) < 1)
        tau = -1;
    else
        tau = range_value/(closure_rate);
    end
else
    tau = -1;
end

