function uavs_states_ = group_uavs(uavs_states, uav_state)

uav_id = size(uavs_states,1)+1;
uav_data.uav_state_data = uav_state;
uavs_states.uav_id = uav_data;
if(size(uavs_states,1) > 0)
	uavs_states.num_uavs = uavs_states.num_uavs + 1;

else
	uavs_states.num_uavs = 1;	
end
uavs_states_ = uavs_states;
