function [advisory_direction resolution_rate] = resolve_conflict(host_aircraft_altitude, state_horizontal,state_altitude, old_altitude_command)
delta_x = state_horizontal(1);
dot_x = state_horizontal(2);
delta_altitude = state_altitude(1);
dot_altitude = state_altitude(2);
if(delta_altitude < 0)
    own_altitude_THLD = 1000*0.3048;
    if(host_aircraft_altitude <= own_altitude_THLD)
        advisory_direction = 'Crossing';
        resolution_rate = 0;
       
    else
        % host aircraft is below an intruder aircraft
        
            if(dot_altitude == 0 )
                % check for vertical threshold
                if(delta_altitude >= -850*0.3048)
                    advisory_direction = 'Increase descend';
                    resolution_rate = -2500*0.3048/60;
                    
                else
                    advisory_direction = 'Reduce descent';
                    resolution_rate = -500*0.3048/60;
                    
                end
            else
                if(dot_x > 0)
                    if(delta_altitude >= -850*0.3048)
                        advisory_direction = 'Descend. Descend';
                        resolution_rate = -1500*0.3048/60;
                     
                    else
                        advisory_direction = 'Monitor vertical speed';
                        resolution_rate = 0;
                      
                    end
                    
                else
                    if(delta_altitude < -850*0.3048)
                        advisory_direction = 'Monitor vertical speed';
                        resolution_rate = 0;
                      
                    else
                        advisory_direction = 'Increase descent';
                        resolution_rate = -2500*0.3048/60;
                    
                    end
                    
                end

            
            
        end
    end
   
    
else
    % host aircraft is above or level with an intruder aircraft
 
        if(dot_altitude == 0)
            % check for vertical threshold
            if(delta_altitude <= 850*0.3048)
                advisory_direction = 'Increase climb';
                resolution_rate = 2500*0.3048/60;
          
            else
                advisory_direction = 'Reduce climb';
                resolution_rate = 500*0.3048/60;
                
            end
        else
            if(dot_x > 0)
                if(delta_altitude <= 850*0.3048)
                    advisory_direction = 'Climb; climb';
                    resolution_rate = 1500*0.3048/60;
                 
                else
                    advisory_direction = 'Monitor vertical speed';
                    resolution_rate = 0;
                  
                end
                
            else
                if(delta_altitude > 850*0.3048)
                    advisory_direction = 'Monitor vertical speed';
                    resolution_rate = 0;
                 
                else
                    advisory_direction = 'Increase climb';
                    resolution_rate = 2500*0.3048/60;
              
                end
                
            end

    end
end

