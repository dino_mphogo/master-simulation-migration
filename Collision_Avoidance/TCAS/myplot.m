load uav_1_position_data.mat
load uav_2_position_data.mat
load uav_3_position_data.mat
load uav_4_position_data.mat
load uav_5_position_data.mat
load uav_6_position_data.mat
load uav_7_position_data.mat
load uav_8_position_data.mat
load uav_9_position_data.mat
load uav_10_position_data.mat
figure
hold on
grid on
for i=1:size(uav_1_position_data.data,1)
	plot(uav_1_position_data.data(i,1),uav_1_position_data.data(i,3),'r.')
	plot(uav_2_position_data.data(i,1),uav_2_position_data.data(i,3),'g.')
	plot(uav_3_position_data.data(i,1),uav_3_position_data.data(i,3),'b.')
	plot(uav_4_position_data.data(i,1),uav_4_position_data.data(i,3),'c.')
	plot(uav_5_position_data.data(i,1),uav_5_position_data.data(i,3),'k.')
	plot(uav_6_position_data.data(i,1),uav_6_position_data.data(i,3),'m.')
	plot(uav_7_position_data.data(i,1),uav_7_position_data.data(i,3),'y.')
	plot(uav_8_position_data.data(i,1),uav_8_position_data.data(i,3),'r-')
	plot(uav_9_position_data.data(i,1),uav_9_position_data.data(i,3),'b-')
	plot(uav_10_position_data.data(i,1),uav_10_position_data.data(i,3),'g-.')
	pause(0.0001);
end
