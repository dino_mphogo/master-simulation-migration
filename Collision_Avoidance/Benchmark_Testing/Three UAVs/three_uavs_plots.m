load uav_1_position_data.mat
load uav_2_position_data.mat
load uav_3_position_data.mat
load uav_1_flight_data.mat
load uav_2_flight_data.mat
load uav_3_flight_data.mat
figure 
%for i = 1:size(uav_1_position_data.data(:,1),1)
	subplot(2,2,1);
	hold on
	grid on
	title('UAV 1, UAV 2, UAV 3 positions')
	plot(uav_1_position_data.data(:,1),uav_1_position_data.data(:,3),'r.')
	plot(uav_2_position_data.data(:,1),uav_2_position_data.data(:,3),'b.')
	plot(uav_3_position_data.data(:,1),uav_3_position_data.data(:,3),'k.')
	xlabel('X position(m)');
	ylabel('Altitude(m)');
	legend('UAV 1 position','UAV 2 position', 'UAV 3 position');
	
	subplot(2,2,2);
	hold on
	grid on
	title('Altitude vs time steps');
	plot(1:size(uav_1_flight_data.data(:,1),1),uav_1_flight_data.data(:,1),'r-')
	plot(1:size(uav_2_flight_data.data(:,1),1),uav_2_flight_data.data(:,1),'b-')
	plot(1:size(uav_3_flight_data.data(:,1),1),uav_3_flight_data.data(:,1),'k-')
	xlabel('time step');
	ylabel('Altitude(m)');
	legend('UAV 1','UAV 2', 'UAV 3');

	subplot(2,2,4);
	hold on
	grid on
	title('Climb rate vs simulation time');
	plot(1:size(uav_1_flight_data.data(:,3),1),uav_1_flight_data.data(:,3),'r-')
	plot(1:size(uav_2_flight_data.data(:,3),1),uav_2_flight_data.data(:,3),'b-')
	plot(1:size(uav_3_flight_data.data(:,3),1),uav_3_flight_data.data(:,3),'k-')
	xlabel('time step');
	ylabel('Climb rate(m/s)');
	legend('UAV 1','UAV 2','UAV 3');
	pause(0.003);
	
	subplot(2,2,3);
	hold on
	grid on
	title('North position rate vs simulation time');
	plot(1:size(uav_1_flight_data.data(:,2),1),uav_1_flight_data.data(:,2),'r-')
	plot(1:size(uav_2_flight_data.data(:,2),1),uav_2_flight_data.data(:,2),'b-')
	plot(1:size(uav_3_flight_data.data(:,2),1),uav_3_flight_data.data(:,2),'k-')
	xlabel('time step');
	ylabel('North position rate(m/s)');
	legend('UAV 1','UAV 2', 'UAV 3');
	%pause(0.00005);
%end
