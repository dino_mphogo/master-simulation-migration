load('uav_1_altitude_rate_limited.mat')
load('uav_2_altitude_rate_limited.mat')
load('uav_3_altitude_rate_limited.mat')
load('three_uavs_sim_time.mat')
load('altitude_delt.mat')

sim_time = three_uavs_sim_time.data;
uav_1 = uav_1_altitude_rate_limited.data;
uav_2 = uav_2_altitude_rate_limited.data;
uav_3 = uav_3_altitude_rate_limited.data;
alt_delt = altitude_delt_data.data;
uav_1_alt = reshape(altitude_delt_data.data(1,:,:),[size(altitude_delt_data.data, 2) size(altitude_delt_data.data,3)])';
uav_2_alt = reshape(altitude_delt_data.data(2,:,:),[size(altitude_delt_data.data, 2) size(altitude_delt_data.data,3)])';
uav_3_alt = reshape(altitude_delt_data.data(3,:,:),[size(altitude_delt_data.data, 2) size(altitude_delt_data.data,3)])';

figure

plot(sim_time,-uav_1(:,2),'-b')
grid on
xlabel('time(s)')
ylabel('Altitude Rate(feet/s)')
legend('Altitude Rate UAV 1')
hold off

figure

plot(sim_time,-uav_2(:,2),'-b')
grid on
xlabel('time(s)')
ylabel('Altitude Rate(feet/s)')
legend('Altitude Rate UAV 2')
hold off

figure


plot(sim_time,-uav_3(:,2),'-b')
grid on
xlabel('time(s)')
ylabel('Altitude Rate(feet/s)')
legend('Altitude Rate UAV 3')
hold off
