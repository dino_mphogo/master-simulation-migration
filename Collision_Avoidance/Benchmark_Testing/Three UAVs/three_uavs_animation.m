% prepare environment 



 
% insert enemy aircraft 
load('uav_1_position_data.mat')
data = [uav_1_position_data.data(:,1:3)  asin(uav_1_position_data.data(:,4)/max(uav_1_position_data.data(:,5))) ones(size(uav_1_position_data.data(:,1),1),1)*90 zeros(size(uav_1_position_data.data(:,1),1),1) ];
uav_1_data = data;
new_object('aircraft_1.mat',data,... 
'model','f-16.mat','scale',50,... 
'edge',[0 0 0],'face',[.0 0.8 0],'alpha',1,... 
'path','on','pathcolor',[.0 0.8 0],'pathwidth',1); 
 
 load('uav_2_position_data.mat')
 data = [uav_2_position_data.data(:,1:3)  asin(uav_2_position_data.data(:,4)/max(uav_2_position_data.data(:,5))) ones(size(uav_2_position_data.data(:,1),1),1)*-90 zeros(size(uav_2_position_data.data(:,1),1),1)];
 uav_2_data = data;
 new_object('aircraft_2.mat',data,... 
'model','f-16.mat','scale',50,... 
'edge',[0 0 0],'face',[.89 .0 .27],'alpha',1,... 
'path','on','pathcolor',[0 0 0.27],'pathwidth',1); 

 load('uav_3_position_data.mat')
 data = [uav_3_position_data.data(:,1:3)  asin(uav_3_position_data.data(:,4)/max(uav_3_position_data.data(:,5))) ones(size(uav_3_position_data.data(:,1),1),1)*-90 zeros(size(uav_3_position_data.data(:,1),1),1)];
 uav_3_data = data;
 new_object('aircraft_3.mat',data,... 
'model','f-16.mat','scale',50,... 
'edge',[0 0 0],'face',[.89 .0 .27],'alpha',1,... 
'path','on','pathcolor',[0.9 0 0 ],'pathwidth',1);



% generate the scene and save the result as animated gif file 
flypath('aircraft_1.mat','aircraft_2.mat','aircraft_3.mat',...	
'animate','off','step',1500,... 
'font','Georgia','fontsize',10,... 
'view',[0 0],'window',[700 700],... 
'output','three_aircraft_animation.png','dpi',600,...
'xlim',[0 11000],'ylim',[-500 3500],'zlim',[0 6000]);

%[x,y,z] = sphere(100); 
%for index = 1:1500:size(data,1)
%	surf(uav_1_data(index,1)+6076.12*0.35*x,uav_1_data(index,2)+6076.12*0.35*y,uav_1_data(index,3)+650*z,... 
%	'EdgeColor','none','FaceColor',[.89 .0 .27],'FaceAlpha',.2); 

%end




