load uav_1_flight_path_data.mat;
load uav_2_flight_path_data.mat;
load uav_3_flight_path_data.mat;

load uav_1_tau_data.mat
load uav_2_tau_data.mat
load uav_3_tau_data.mat
load three_uavs_sim_time.mat;
sim_time = three_uavs_sim_time.data;

figure
grid on
hold on
uav_1_tau = reshape(uav_1_tau_data.data, [size(uav_1_tau_data.data, 2) size(uav_1_tau_data.data, 3)])';
uav_2_tau = reshape(uav_2_tau_data.data, [size(uav_2_tau_data.data, 2) size(uav_2_tau_data.data, 3)])';
uav_3_tau = reshape(uav_3_tau_data.data, [size(uav_3_tau_data.data, 2) size(uav_3_tau_data.data, 3)])';
title('Time to collisions(tau)');
plot(sim_time,uav_1_tau(:,2));
plot(sim_time,uav_1_tau(:,3));
plot(sim_time,uav_2_tau(:,3));
xlabel('Time(s)');
ylabel('Time to Collision(s)');
legend('UAV 1', 'UAV 2', 'UAV 3');
filename = 'uavs_tau_functions.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off



figure
grid on
hold on
title('Flight Path Angles');
plot(sim_time,uav_1_flight_path_data.data);
plot(sim_time,uav_2_flight_path_data.data);
plot(sim_time,uav_3_flight_path_data.data);
xlabel('Time(s)');
ylabel('Flight Path Angle(Degrees)');
legend('UAV 1', 'UAV 2', 'UAV 3');
filename = 'flight_path_angles.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
