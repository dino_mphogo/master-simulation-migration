function [uav_3_waypoints_, uav_launch_angle] = uav_3_waypoints()
start_point = [9000 594/0.3048 4700];
x_0 = start_point(1);
y_0 = start_point(2);
z_0 = start_point(3);
waypoint_0 = [x_0 y_0 z_0];
waypoint_1 = [-100000 y_0 z_0];
uav_launch_angle = 0; % picks the random waypoint angle
uav_3_waypoints_ = [waypoint_0; waypoint_1];
