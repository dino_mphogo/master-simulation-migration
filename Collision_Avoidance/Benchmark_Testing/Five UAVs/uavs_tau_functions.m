load uav_1_flight_path_data.mat;
load uav_2_flight_path_data.mat;
load uav_3_flight_path_data.mat;
load uav_4_flight_path_data.mat;
load uav_5_flight_path_data.mat;

load uav_1_tau_data.mat
load uav_2_tau_data.mat
load uav_3_tau_data.mat
load uav_4_tau_data.mat
load uav_5_tau_data.mat
load five_uavs_sim_time.mat;
sim_time = five_uavs_sim_time.data;


uav_1_tau = reshape(uav_1_tau_data.data, [size(uav_1_tau_data.data, 2) size(uav_1_tau_data.data, 3)])';
uav_2_tau = reshape(uav_2_tau_data.data, [size(uav_2_tau_data.data, 2) size(uav_2_tau_data.data, 3)])';
uav_3_tau = reshape(uav_3_tau_data.data, [size(uav_3_tau_data.data, 2) size(uav_3_tau_data.data, 3)])';
uav_4_tau = reshape(uav_4_tau_data.data, [size(uav_4_tau_data.data, 2) size(uav_4_tau_data.data, 3)])';
uav_5_tau = reshape(uav_5_tau_data.data, [size(uav_5_tau_data.data, 2) size(uav_5_tau_data.data, 3)])';



figure 

taus = abs([uav_1_tau; uav_2_tau; uav_3_tau; uav_4_tau; uav_5_tau]);
taus(1,:) = [];
taus = taus(find(taus < 100));
taus = taus(find(taus > 0));
histogram(taus)
xlabel('Time to Collision')
ylabel('Frequency')
legend('\tau < 100s')
title('Time to Collision')
grid on
filename = 'uavs_tau_functions_statistical.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));


