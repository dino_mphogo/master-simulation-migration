function [] = filter_conflicts_ras(conflicts, range, tau_values)

    filtered_data = range(find(conflicts > 0));
    if size(filtered_data, 1) > 0
    	   stat_file = fopen('separation_distance_ras.txt','a');
           fprintf(stat_file, [mat2str(filtered_data), '\n']);
           fclose(stat_file);
           
           filtered_tau = tau_values(find(conflicts > 0));
           stat_file = fopen('separation_time_ras.txt','a');
           fprintf(stat_file, [mat2str(filtered_tau), '\n']);
           fclose(stat_file);
    end
 

end
