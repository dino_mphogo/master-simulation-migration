function  [] = statistical_data_save_tcas(altitude_change, tau_vect, range_dist)

stat_file = fopen('conflict_resolution_statistics_tcas.txt','a');
    fprintf(stat_file, [mat2str(tau_vect), ' ' ,mat2str(range_dist), ' \n']);

    fclose(stat_file);

 stat_file = fopen('altitude_change_statistics_tcas.txt','a');
   fprintf(stat_file, [mat2str(altitude_change), ' \n ']);
    fclose(stat_file);


end
