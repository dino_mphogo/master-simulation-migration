load('uav_1_position_data.mat')
load('uav_2_position_data.mat')
load('uav_3_position_data.mat')
load('uav_4_position_data.mat')
load('uav_5_position_data.mat')



altitude = [abs(uav_1_position_data.data(:,3) - uav_1_position_data.data(1,3)) abs(uav_2_position_data.data(:,3) - uav_2_position_data.data(1,3))  abs(uav_3_position_data.data(:,3) - uav_3_position_data.data(1,3))  abs(uav_4_position_data.data(:,3) - uav_4_position_data.data(1,3))  abs(uav_5_position_data.data(:,3) - uav_5_position_data.data(1,3))]; 
altitude = reshape(altitude, [size(altitude,1)*size(altitude,2) 1]);
altitude = altitude(find(altitude > 0));

figure 
histogram(altitude)
xlabel('Altitude Deviations(feet)')
ylabel('Frequency')
title('UAVs Altitude Deviations')
legend('\Delta h')
grid on
filename = 'uavs_tau_altitude_deviations_statistical.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));




