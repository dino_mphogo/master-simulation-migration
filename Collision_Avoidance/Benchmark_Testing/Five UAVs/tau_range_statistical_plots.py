import matplotlib.pyplot as plt
import numpy as np
from pylab import *
from scipy import stats
from scipy.signal import argrelextrema
def list_filter(line):
  line_new = line.replace("[", "")
  line = line_new.replace("]", "")
  line_new = line.replace("\n", "")
  line = line_new.replace("-1", "0")
  line = line_new.replace("NaN", "0")
  line = line_new.replace("Nan", "0")
  line = line_new.replace("inf", "0")
  line = line_new.replace("Inf", "0")
  altitudes = line.split(';')
  if "Nan" in line:
  	print "NAn present!"
  if "NaN" in line:
  	print "NAN present!"
  if "Inf" in line:
  	print "inf present!"
  if "InF" in line:
  	print "inf present!"
  if altitudes[0] != '':
  	altitudes_dev = [float(item) for item in altitudes]
  return altitudes_dev


min_distances_cif = []
for line in open('separation_distance_cif.txt', 'r'):
  line_new = line.replace("\n", "")
  if '[' in line:
  	line_new = line_new.replace("[", "")
  	line = line_new.replace("]", "")
  	distances = line.split(';')
  	for item in distances:
  		min_distances_cif.append(float(item))
  else:
  	min_distances_cif.append(float(line_new))

min_times_cif = []
for line in open('separation_time_cif.txt', 'r'):
  line_new = line.replace("\n", "")
  line_new = line_new.replace("inf", "10000")
  line_new = line.replace("Inf", "10000")
  if '[' in line:
  	line_new = line_new.replace("[", "")
  	line = line_new.replace("]", "")
  	times = line.split(';')
  	for item in times:
  		min_times_cif.append(float(item))
  else:
  	min_times_cif.append(float(line_new))

min_times_cif = np.array(min_times_cif)
min_distances_cif = np.array(min_distances_cif)


min_distances_ras = []
for line in open('separation_distance_ras.txt', 'r'):
  line_new = line.replace("\n", "")
  if '[' in line:
  	line_new = line_new.replace("[", "")
  	line = line_new.replace("]", "")
  	line = line.replace(" ", "")
  	distances = line.split(';')
  	for item in distances:
  		min_distances_ras.append(float(item))
  else:
  	min_distances_ras.append(float(line_new))


min_times_ras = []
for line in open('separation_time_ras.txt', 'r'):
  line_new = line.replace("\n", "")
  line_new = line_new.replace("inf", "10000")
  line_new = line_new.replace("Inf", "10000")

  if '[' in line:
  	line_new = line_new.replace("[", "")
  	line = line_new.replace("]", "")
  	line = line.replace(" ", "")
  	times = line.split(';')
  	for item in times:
  		min_times_ras.append(float(item))
  else:
  	min_times_ras.append(float(line_new))	

min_distances_ras = np.array(min_distances_ras)
min_times_ras = np.array(min_times_ras)



f, axarr = plt.subplots(1, 2)
axarr[0].boxplot(min_distances_cif[min_distances_cif < 650])
axarr[0].set_title('Separation Distribution CIF(N = %d)' %len(min_distances_cif[min_distances_cif < 650]))
axarr[0].set_ylim([0,max(min_distances_cif[min_distances_cif < 650].max(), min_distances_ras[min_distances_ras < 650].max())])
axarr[0].grid(True)
axarr[1].boxplot(min_distances_ras[min_distances_ras < 650])
axarr[1].set_title('Separation Distribution RAS(N = %d)' %len(min_distances_ras[min_distances_ras < 650]))
axarr[1].set_ylim([0,max(min_distances_cif[min_distances_cif < 650].max(), min_distances_ras[min_distances_ras < 650].max())])
axarr[1].grid(True)
for ax in axarr.flat:
    ax.set(ylabel='Separation Distance(feet)', xlabel='')


fig, ax11 = plt.subplots(figsize=(8,4))

plt.hist(min_times_cif[min_distances_cif < 650], bins=2000,  normed=True, color='#0504aa')
plt.title('CIF time separation with distance < 650 ft (N = %d)' %len(min_times_cif[min_distances_cif < 650]))
plt.ylabel('Density')
plt.xlabel('Separation Time(s)')
ax11.grid(True)



fig, ax8 = plt.subplots(figsize=(8,4))
plt.hist(min_times_ras[min_distances_ras < 650], bins=2000, normed=True, color='#0504aa')
plt.title('RAS time separation with distance < 650 ft (N = %d)' %len(min_times_ras[min_distances_ras < 650]))
plt.ylabel('Density')
plt.xlabel('Separation Time(s)')
ax8.grid(True)


'''
fig, ax12 = plt.subplots(figsize=(8,4))

plt.hist(min_distances_cif[min_times_cif < 60], bins=100,  normed=True, color='#0504aa')
plt.title('CIF distance separation with time < 60 s (N = %d)' %len(min_distances_cif[min_times_cif < 60]))
plt.ylabel('Density')
plt.xlabel('Separation Distance(feet)')
ax12.grid(True)



fig, ax9 = plt.subplots(figsize=(8,4))
plt.hist(min_distances_ras[min_times_ras < 60], bins=100,  normed=True, color='#0504aa')
plt.title('RAS distance separation with time < 60 s (N = %d)' %len(min_distances_ras[min_times_ras < 60]))
plt.ylabel('Density')
plt.xlabel('Separation Distance(feet)')
ax9.grid(True)


'''

    
    
    
f, axarr2 = plt.subplots(1, 2)
axarr2[0].boxplot(min_distances_cif[min_times_cif < 60])
axarr2[0].set_title('CIF Separation Distance for time < 60s (N = %d)' %len(min_distances_cif[min_times_cif < 60]))
axarr2[0].set_ylim([0,max(min_distances_cif[min_times_cif < 60].max(), min_distances_ras[min_times_ras < 60].max())])
axarr2[0].grid(True)
axarr2[1].boxplot(min_distances_ras[min_times_ras < 60])
axarr2[1].set_title('RAS Separation Distance for time < 60s (N = %d)' %len(min_distances_ras[min_times_ras < 60]))
axarr2[1].set_ylim([0,max(min_distances_cif[min_times_cif < 60].max(), min_distances_ras[min_times_ras < 60].max())])
axarr2[1].grid(True)
for ax in axarr2.flat:
    ax.set(ylabel='Separation Distance(feet)', xlabel='')    

'''
# TCAS analysis
taus_tcas, range_dist_tcas, nmac_tcas, near_conflict_tcas = [],[],[],[]
tau_distance_pair_tcas = []
number_simulations_tcas = 0
tau_pair_tcas, distance_pair_tcas = [],[]
previus_pair = []
tau_distance_pair = []
tcas_success_rates = []
for line in open('conflict_resolution_statistics_tcas.txt', 'r'):
  line_split = line.split(' ')
  tau = line_split[0]
  dist = line_split[1]
  dists = list_filter(dist)
  dists_new = abs(np.array(dists))
  tau_new = list_filter(tau)
  tau_new = np.array(tau_new)
  tau_new = abs(tau_new)
  #tau_new = tau_new[dists_new < 650]
  #dists_new = dists_new[dists_new < 650]
  if len(zip(tau_new[dists_new < 650], dists_new[dists_new < 650])) > 0 and previus_pair != zip(tau_new[dists_new < 650], dists_new[dists_new < 650]):
	tau_pair_tcas  = np.concatenate([tau_pair_tcas, tau_new[dists_new < 650]])
  	distance_pair_tcas = np.concatenate([distance_pair_tcas, dists_new[dists_new < 650]])
  	previus_pair =  zip(tau_new[dists_new < 650], dists_new[dists_new < 650])
	tau_distance_pair = tau_distance_pair + list(zip(dists_new[dists_new < 650], tau_new[dists_new < 650]))
	

  tau_new = tau_new[tau_new < 60]
  tau_new = tau_new[tau_new > 1]
  if len(tau_new) > 0:
  	taus_tcas.append(min(tau_new))
  if len(dists_new) > 0:
  	range_dist_tcas.append(min(dists_new))
  	nmac_dist = np.abs(np.array(dists_new))
  	tcas_success_rates.append((len(nmac_dist) - len(nmac_dist[nmac_dist < 650]))/(len(nmac_dist) + 0.0)*100)
  	if len(nmac_dist[nmac_dist < 5000]) > 0:
  		near_conflict_tcas.append(len(nmac_dist[nmac_dist < 5000]))
  		nmac_tcas.append(len(nmac_dist[nmac_dist < 650]/2))

tau_distribution_tcas = np.array(taus_tcas)
#tau_distribution_tcas  = np.trim_zeros(tau_distribution_tcas.flatten())
distance_distribution_tcas = np.array(range_dist_tcas)

#distance_distribution_tcas  = np.trim_zeros(np.absolute(distance_distribution_tcas.flatten()))
#distance_distribution_tcas = distance_distribution_tcas[distance_distribution_tcas < 4000]
#distance_distribution_tcas = distance_distribution_tcas[distance_distribution_tcas > 1]


# CIF analysis
taus_closest, range_dist_closest, nmac_closest, near_conflict_closest = [],[],[],[]
n = 0
number_simulations_closest = 0
tau_pair_closest, distance_pair_closest = [],[]
previus_pair = []
cif_success_rates = []
for line in open('conflict_resolution_statistics_closest.txt', 'r'):
  line_split = line.split(' ')
  tau = line_split[0]
  dist = line_split[1]
  dists_new = list_filter(dist)
  dists_new = abs(np.array(dists_new))
  tau_new = list_filter(tau)
  tau_new = np.array(tau_new)
  tau_new = abs(tau_new)
  
  if len(zip(tau_new[dists_new < 650], dists_new[dists_new < 650])) > 0 and previus_pair != zip(tau_new[dists_new < 650], dists_new[dists_new < 650]):
	tau_pair_closest  = np.concatenate([tau_pair_closest, tau_new[dists_new < 650]])
  	distance_pair_closest = np.concatenate([distance_pair_closest, dists_new[dists_new < 650]])
  	previus_pair =  zip(tau_new[dists_new < 650], dists_new[dists_new < 650])	
  n =  len(dists_new)
  tau_new = tau_new[tau_new < 60]
  tau_new = tau_new[tau_new > 1]
  if len(tau_new) > 0:
  	taus_closest.append(min(tau_new))
  if len(dists_new) > 0:
  	# print dists_new
  	range_dist_closest.append(min(dists_new))
  	
  	nmac_dist = np.abs(np.array(dists_new))
  	cif_success_rates.append((len(nmac_dist) - len(nmac_dist[nmac_dist < 650]))/(len(nmac_dist) + 0.0)*100)
  	if len(nmac_dist[nmac_dist < 5000]) > 0:
  		near_conflict_closest.append(len(nmac_dist[nmac_dist < 5000]))
  		nmac_closest.append(len(nmac_dist[nmac_dist < 650]/2))

# let us look what 
#tau_pair_tcas = tau_pair_tcas.flatten()
#tau_pair_closest =  tau_pair_closest.flatten()
#distance_pair_closest =  distance_pair_closest.flatten()
#distance_pair_tcas =  distance_pair_tcas.flatten()


fig, ax10 = plt.subplots(figsize=(8,4))

plt.hist(tcas_success_rates)

plt.title('RAS Success Rate (N = %d)' %len(tcas_success_rates))
plt.ylabel('Frequency')
plt.xlabel('Success Rate(%)')
ax10.grid(True)


fig, ax11 = plt.subplots(figsize=(8,4))

plt.hist(cif_success_rates)
plt.title('CIF Success Rate (N = %d)' %len(cif_success_rates))
plt.ylabel('Frequency')
plt.xlabel('Success Rate(%)')
ax10.grid(True)



fig, ax8 = plt.subplots(figsize=(8,4))
plt.hist(distance_pair_closest, bins=100, color='#0504aa')
plt.title('NMAC CIF (N = %d)' %len(distance_pair_closest))
plt.ylabel('NMAC(650 feet)')
plt.xlabel('Separation Distance(feet)')
ax8.grid(True)

tau_distribution = np.array(taus_closest)
tau_distribution_closest  = np.trim_zeros(tau_distribution.flatten())

distance_distribution = np.array(range_dist_closest)
distance_distribution_closest  = np.trim_zeros(np.absolute(distance_distribution.flatten()))
#distance_distribution_closest = distance_distribution[distance_distribution < 4000]
distance_distribution_closest = distance_distribution_closest[distance_distribution_closest > 1]
'''
'''
print "total close encounter CIF ", sum(near_conflict_closest), " NMAC = ",  sum(nmac_closest)
#print "total  encounter CIF ", min(range_dist_closest), " altitude deviations = ",  max(range_dist_closest)

print "Total close encounter TCAS", sum(near_conflict_tcas), " NMAC = ", sum(nmac_tcas) 
#range_dist_tcas
'''
'''
fig, ax1 = plt.subplots(figsize=(8,4))

slice_obj = slice(1,500000)
#distance_distribution_tcas = distance_distribution_tcas.tolist()
#distance_distribution_tcas =  distance_distribution_tcas[slice_obj]
plt.boxplot(distance_distribution_tcas)
ax1.set_title('Separation Distance Distribution TCAS')
ax1.grid(True)

fig, ax2 = plt.subplots(figsize=(8,4))

#distance_distribution_closest = distance_distribution_closest.tolist()
#distance_distribution_closest = distance_distribution_closest[slice_obj]
plt.boxplot(distance_distribution_closest)
ax2.set_title('Separation Distance Distribution CIF')
ax2.grid(True)
ax2.set(ylabel='Separation Distance(feet)', xlabel='')
# Hide x labels and tick labels for top plots and y ticks for right plots.


fig, ax3 = plt.subplots(figsize=(8,4))

plt.hist(distance_distribution_tcas, bins=100, normed=True, color='#0504aa')
plt.title('Separation Distance Distribution TCAS (N = %d)' %len(distance_distribution_tcas))
plt.ylabel('Density')
plt.xlabel('Separation Distance(feet)')
ax3.grid(True)


fig, ax4 = plt.subplots(figsize=(8,4))

plt.hist(distance_distribution_closest, bins=100, normed=True, color='#0504aa')
plt.title('Seperation Distance Distribution CIF (N = %d)' %len(distance_distribution_closest))
plt.ylabel('Density')
plt.xlabel('Separation Distance(feet)')
ax4.grid(True)



fig, ax5 = plt.subplots(figsize=(8,4))

plt.hist(tau_distribution_tcas, bins=100, normed=True, color='#0504aa')
plt.title('Time-to-collision Distribution TCAS (N = %d)' %len(tau_distribution_tcas))
plt.ylabel('Density')
plt.xlabel('Time(s)')
ax5.grid(True)

fig, ax6 = plt.subplots(figsize=(8,4))

plt.hist(tau_distribution_closest, bins=100,  normed=True, color='#0504aa')
plt.title('Time-to-collision Distribution CIF (N = %d)' %len(tau_distribution_closest))
plt.ylabel('Density')
plt.xlabel('Time(s)')
ax6.grid(True)


fig, ax7 = plt.subplots(figsize=(8,4))
plt.hist(np.gradient(distance_distribution_tcas), label='TCAS')
plt.hist(np.gradient(distance_distribution_closest),  label='Pairwise CIF')
plt.title('Seperation Distance Distribution CIF (N = %d)' %len(distance_distribution_closest))
plt.xlabel('Samples(n)')
plt.ylabel('Separation Distance(feet)')
ax7.grid(True)

f, axarr1 = plt.subplots(1, 2)

slice_obj = slice(1,500000)
#tau_distribution_tcas = tau_distribution_tcas.tolist()
#tau_distribution_closest = tau_distribution_closest.tolist()
#tau_distribution_tcas = tau_distribution_tcas[slice_obj]
# tau_distribution_closest = tau_distribution_closest[slice_obj]
axarr1[0].hist(x=distance_distribution_tcas, bins='auto', color='#0504aa', normed=1)
axarr1[0].set_title('Time to collision Distribution TCAS')
axarr1[0].grid(True)
axarr1[1].hist(x=distance_distribution_closest, bins='auto', color='#0504aa', normed=1)
axarr1[1].set_title('Time to collision Distribution CIF')
axarr1[1].grid(True)
for ax in axarr1.flat:
    ax.set(xlabel='Time(feet)', ylabel='Density')
# Hide x labels and tick labels for top plots and y ticks for right plots.

fig, ax3 = plt.subplots(1, 2)
sample_taus_tcas = stats.kde.gaussian_kde(tau_distribution_tcas)
sample_taus_closest = stats.kde.gaussian_kde(tau_distribution_closest)
x_tcas = np.arange(0,max(tau_distribution_tcas),0.1)
x_closest = np.arange(0,max(tau_distribution_closest),0.1)
ax3[0].plot(x_tcas,sample_taus_tcas(x_tcas))
ax3[0].set_title('Time to collision Distribution TCAS')
ax3[0].grid(True)
ax3[1].plot(x_closest,sample_taus_closest(x_closest))
ax3[1].set_title('Time to collision Distribution CIF')
ax3[1].grid(True)
for ax in ax3.flat:
    ax.set(xlabel='Time(feet)', ylabel='Density')
'''


plt.show()

