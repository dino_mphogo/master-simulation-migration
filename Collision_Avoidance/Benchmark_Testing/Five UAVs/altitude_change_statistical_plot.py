import matplotlib.pyplot as plt
import numpy as np
from pylab import *
from scipy import stats
from scipy.signal import argrelextrema

altitude_deviations_tcas = []
altitude_clusters_tcas = []
def cluster_positions(altitude):
	positions = []
	pos = []
	for index in range(0, len(altitude)):
		if abs(altitude[index]) > 1 and abs(altitude[index-1]) < 1:
			if len(pos) != 2:
				pos.append(index)
			else:
				positions.append(pos)
				pos = []
				pos.append(index)
		if abs(altitude[index]) < 1 and len(pos) == 1:
			pos.append(index)
			positions.append(pos)
			pos = []
			
	return positions


def clusters(altitude_deviations_tcas):
	altitude_clusters_tcas = []
	for uav in range(0, altitude_deviations_tcas.shape[0]):
		uav_positions = altitude_deviations_tcas[uav,:]
		indices = cluster_positions(uav_positions)
		for index_pair in indices:
			x = index_pair[0]
			y = index_pair[1]
			altitude_clusters_tcas.append(max(abs(uav_positions[x:y])))
	return altitude_clusters_tcas	

def tcas_uavs_altitude_deviations(altitude, method):
	for index in range(5):
		a = altitude[:,index]
		plt.figure()
		sample = stats.kde.gaussian_kde(a)
		x = np.arange(0,max(a),0.1)
		#plt.boxplot(a)
		plt.plot(x,sample(x),label='Altitude Deviations UAV %d' % (index + 1))
		plt.hist(a, normed=True,  color='#0504aa')
		plt.title('Altitude Deviations (N = %d)'%len(a))
		plt.ylabel('Devition(feet)')
		plt.xlabel('Deviations(feet)')
		plt.legend()
		plt.grid(True)
		plt.savefig(str('uav_%d_altitude_deviations_box_' + method +'.png') % (index+1))
	

# RAS analysis				
for line in open('altitude_change_statistics_tcas.txt', 'r'):
  line_new = line.replace("[", "")
  line = line_new.replace("]", "")
  line_new = line.replace("\n", "")
  line = line_new.replace(" ", "")
  altitudes = line.split(';')
  if altitudes[0] != '':
  	altitudes_dev = [float(item) for item in altitudes]
  	altitude_deviations_tcas.append(altitudes_dev)


altitude_deviations_tcas  = np.array(altitude_deviations_tcas)
#altitude_deviations_tcas = altitude_deviations_tcas[argrelextrema(altitude_deviations_tcas, np.greater)[0]]
altitude_deviations_tcas  = np.abs(altitude_deviations_tcas)
tcas_uavs_altitude_deviations(altitude_deviations_tcas ,'ras')
path_rates_tcas= []
for uav in range(0, altitude_deviations_tcas.shape[0]):
	uav_positions = altitude_deviations_tcas[uav,:]
	indices = cluster_positions(uav_positions)
	for index_pair in indices:
		x = index_pair[0]
		y = index_pair[1]
		altitude_clusters_tcas.append(max(abs(uav_positions[x:y])))
		if len(uav_positions[x:y]) > 1:
			path_rates_tcas.append(max(abs(np.gradient(uav_positions[x:y]))))





# CIF analysis
altitude_deviations_closest = []
altitude_clusters_closest = []
for line in open('altitude_change_statistics_closest.txt', 'r'):
  line_new = line.replace("[", "")
  line = line_new.replace("]", "")
  line_new = line.replace("\n", "")
  line = line_new.replace(" ", "")
  altitudes = line.split(';')
  if altitudes[0] != '':
  	altitudes_dev = [float(item) for item in altitudes]
  	altitude_deviations_closest.append(altitudes_dev)



altitude_deviations_closest  = np.array(altitude_deviations_closest)
#altitude_deviations_closest = altitude_deviations_closest[argrelextrema(altitude_deviations_closest, np.greater)[0]]
altitude_deviations_closest  = np.abs(altitude_deviations_closest)
tcas_uavs_altitude_deviations(altitude_deviations_closest,'cif')
path_rates_closest = []
for uav in range(0, altitude_deviations_closest.shape[0]):
	uav_positions = altitude_deviations_closest[uav,:]
	indices = cluster_positions(uav_positions)
	for index_pair in indices:
		x = index_pair[0]
		y = index_pair[1]
		altitude_clusters_closest.append(max(abs(uav_positions[x:y])))
		if len(uav_positions[x:y]) > 1:
			path_rates_closest.append(max(abs(np.gradient(uav_positions[x:y]))))
		
		
path_rates_closest = np.array(path_rates_closest)
path_rates_closest = path_rates_closest.flatten()
path_rates_tcas = np.array(path_rates_tcas)
path_rates_tcas = path_rates_tcas.flatten()	
altitude_clusters_tcas = np.array(altitude_clusters_tcas)
altitude_clusters_tcas = altitude_clusters_tcas.flatten()
altitude_clusters_closest = np.array(altitude_clusters_closest)
altitude_clusters_closest = altitude_clusters_closest.flatten()






'''
f, axarr = plt.subplots(1, 2)
axarr[0].boxplot(x=altitude_clusters_tcas)
axarr[0].set_title('Altitude Deviations TCAS(N = %d)' %len(altitude_clusters_tcas))
axarr[0].grid(True)
axarr[1].boxplot(altitude_clusters_closest)
axarr[1].set_title('Altitude Deviations CIF (N = %d)' %len(altitude_clusters_closest))
axarr[1].grid(True)
for ax in axarr.flat:
    ax.set(ylabel='Altitude(feet)', xlabel='')
  



fig, ax3 = plt.subplots(figsize=(8,4))

plt.hist(path_rates_tcas,  normed=True, color='#0504aa')
plt.title('Altitude Rates Distribution TCAS(N = %d)' %len(path_rates_tcas))
plt.ylabel('Density')
plt.xlabel('Altitude Rates(feet/s)')
ax3.grid(True)

fig, ax4 = plt.subplots(figsize=(8,4))

plt.hist(path_rates_closest,  normed=True, color='#0504aa')
plt.title('Altitude Rates Distribution CIF(N = %d)' %len(path_rates_closest))
plt.ylabel('Density')
plt.xlabel('Altitude Rates(feet/s)')
ax4.grid(True)

'''

  
'''
f, axarr1 = plt.subplots(1, 2)
x_tcas = np.linspace(0, max(altitude_deviations_tcas), len(altitude_deviations_tcas))
x_closest = np.linspace(0, max(altitude_deviations_closest), len(altitude_deviations_tcas))

slope_tcas, intercept_tcas, r_value_tcas, p_value_tcas, std_err_tcas = stats.linregress(x_tcas, altitude_deviations_tcas)
slope_closest, intercept_closest, r_value_closest, p_value_closest, std_err_closest = stats.linregress(x_closest, altitude_deviations_closest)

axarr1[0].plot(x_tcas, altitude_deviations_tcas, 'o', label='samples data')
axarr1[0].plot(x_tcas, intercept_tcas + slope_tcas*x_tcas, 'r', label='line fit')
axarr1[0].set_title('Altitude Deviations Regression TCAS')
axarr1[0].grid(True)
axarr1[1].plot(x_closest, altitude_deviations_closest, 'o', label='samples data')
axarr1[1].plot(x_closest, intercept_closest + slope_closest*x_closest, 'r', label='line fit')
axarr1[1].set_title('Altitude Deviations Regression CIF')
axarr1[1].grid(True)
for ax in axarr1.flat:
    ax.set(xlabel='Altitude(feet)', ylabel='Probability')
'''
plt.show()
