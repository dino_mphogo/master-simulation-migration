load uav_1_navigation_data.mat;
load uav_2_navigation_data.mat;
load two_uavs_simulation_time.mat;
load uav_1_flight_path_data.mat;
load uav_2_flight_path_data.mat;

sim_time = two_uavs_simulation_time.data;
rows = size(uav_1_navigation_data.data,2);
columns = size(uav_1_navigation_data.data,3);
uav_1_data = reshape(uav_1_navigation_data.data(1,:,:), [rows, columns])';
uav_2_data = reshape(uav_2_navigation_data.data(1,:,:), [rows, columns])';
figure

title('Time to Collision \tau');
plot(sim_time,uav_1_data(:,2),'b-')
grid on
xlabel('Time(s)');
ylabel('\tau (seconds)');
legend('Time to Collision \tau');
filename = 'tau_function.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));



hold off
figure

title('Range(r)');
plot(sim_time,uav_1_data(:,4),'b-')
grid on
xlabel('Time(s)');
ylabel('Range r (feet)');
legend('Range r');
filename = 'range_function.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));



hold off
figure
title('Range Rate \dot{r}');
plot(sim_time,uav_1_data(:,6),'b-');
grid on
xlabel('Time(s)');
ylabel('Range Rate(feet/sec)');
legend({'Range Rate \dot r'}, 'Interpreter','latex');
filename = 'range_dot_function.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));






figure
title('Closure Rate (-\dot{r})');
plot(sim_time,-uav_1_data(:,6),'b-');
grid on
xlabel('Time(s)');
ylabel('Rate(feet/sec)');
legend({'Closure Rate -\dot(r)'}, 'Interpreter','latex');

filename = 'closure_rate_function.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));
hold off


figure

hold on
title('Flight Path Angles');
plot(sim_time,uav_1_flight_path_data.data);
plot(sim_time,uav_2_flight_path_data.data);
grid on
xlabel('Time(s)');
ylabel('Flight Path Angle(Degreees)');
legend('UAV 1', 'UAV 2');
filename = 'flight_path_angles.png'; 
eval(sprintf('print -dpng -r600 %s;',filename));







