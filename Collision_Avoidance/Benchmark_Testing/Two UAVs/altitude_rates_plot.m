load('uav_1_altitude_rate_limited.mat')
load('uav_2_altitude_rate_limited.mat')
load('two_uavs_simulation_time.mat')
load('altitude_delt.mat')

sim_time = two_uavs_simulation_time.data;
uav_1 = uav_1_altitude_rate_limited.data;
uav_2 = uav_2_altitude_rate_limited.data;
alt_delt = altitude_delt_data.data;
uav_1_alt = reshape(altitude_delt_data.data(1,:,:),[size(altitude_delt_data.data, 2) size(altitude_delt_data.data,3)])';
uav_2_alt = reshape(altitude_delt_data.data(2,:,:),[size(altitude_delt_data.data, 2) size(altitude_delt_data.data,3)])';



figure
plot(sim_time,uav_1(:,1),'-r')
hold on
grid on
plot(sim_time,uav_1(:,2),'-b')
xlabel('time(s)')
ylabel('Altitude Rate(feet/s)')
legend('Altitude rate Limited', 'Altitude Rate')
hold off

figure
plot(sim_time,uav_2(:,1),'-r')
hold on
grid on
plot(sim_time,uav_2(:,2),'-b')
xlabel('time(s)')
ylabel('Altitude Rate(feet/s)')
legend('Altitude rate Limited', 'Altitude Rate')
hold off


