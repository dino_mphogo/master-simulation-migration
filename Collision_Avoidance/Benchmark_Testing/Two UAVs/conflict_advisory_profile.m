load('two_uavs_simulation_time.mat')
load('uav_1_position_data.mat')
load('uav_2_position_data.mat')
load('uav_tau_data.mat')

tau_data = uav_tau_data.data;
uav_1_data = uav_1_position_data.data(:,3);
uav_1_data = uav_1_data - uav_1_data(1);
uav_2_data = uav_2_position_data.data(:,3);
uav_2_data = uav_2_data - uav_2_data(1);
uav_time = two_uavs_simulation_time.data;

subplot(2,1,1);
hold on
g = area( uav_time, uav_1_data);
g.EdgeColor = 'red';
g.FaceColor = 'red';
n  = area(uav_time, fliplr(uav_2_data));
n.EdgeColor = 'blue';
n.FaceColor = 'blue';
grid on
legend('Climb Manoeuvre', 'Descent Manoeuvre');
xlabel('Time(s)')
ylabel('\Delta h')

hold off
subplot(2,1,2);
plot(uav_time, tau_data);
grid on
legend({'$\tau_{ra} \le \tau \le \tau_{ta}$'}, 'Interpreter','latex')
xlabel('Time(s)')
ylabel('Conflict Status')
ylim([0 2.5])

