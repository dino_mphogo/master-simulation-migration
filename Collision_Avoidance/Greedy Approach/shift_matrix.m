function shifted_waypoints = shift_matrix(old_waypoints,waypoint, index)
old_waypoints_ = old_waypoints(1:index,:);
old_waypoints_(index+1,:) = waypoint;
shifted_waypoints = [old_waypoints_; old_waypoints(index+1:end,:)];
