function tau = get_tau(range_value,closure_rate)
if(closure_rate ~= 0)
    if(abs(range_value) < 0.48*1852)
        tau = -1;
    else
        tau = range_value/(closure_rate);
    end
else
    tau = -1;
end

