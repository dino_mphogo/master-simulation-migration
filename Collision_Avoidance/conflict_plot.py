import matplotlib.pyplot as plt
import numpy as np
from pylab import *
'''
with open("path_planning_statistics.txt") as f:
    data = f.read()
    

data = data.split('\n')
# print(data)
z = np.array([[np.array(row.split(' ')) for row in data]])
z  = np.delete(z, 1,1) 
print z
#uav_id, conflict_nodes_count, terminal_costs,transitional_cost, conflict_time, number_of_intruders = np.loadtxt('path_planning_statistics.txt', delimiter=' ', unpack=True)

'''



uav_ids, conflict_nodes_count, terminal_costs,transitional_cost, conflict_time, number_of_intruders = [],[],[],[],[],[]
for line in open('path_planning_statistics.txt', 'r'):
  values = [str(item) for item in line.split(' ')]
  uav_ids.append(int(values[0]))
  conflict_nodes_count.append(int(values[1]))
  terminal_costs.append(float(values[2]))
  transitional_cost.append(float(values[3]))
  number_of_intruders.append(float(values[5].split('\n')[0]))
  conflict_time.append(values[4])
  #X.append(values[0])
  #Y.append(values[1])

''' 
fig, axs = plt.subplots(1, 4, sharey=True, tight_layout=True)

# We can set the number of bins with the `bins` kwarg





n, bins, patches = axs[0].hist(x=conflict_nodes_count, bins='auto', color='#0504aa',
                            alpha=0.7, rwidth=0.85)
                            
n, bins, patches = axs[1].hist(x=terminal_costs, bins='auto', color='#0504aa',
                            alpha=0.7, rwidth=0.85)
                            
                            
n, bins, patches = axs[2].hist(x=transitional_cost, bins='auto', color='#0504aa',
                            alpha=0.7, rwidth=0.85) 
                            
n, bins, patches = axs[3].hist(x=number_of_intruders, bins='auto', color='#0504aa',
                            alpha=0.7, rwidth=0.85)                                                        

plt.grid(axis='y', alpha=0.75)
plt.xlabel('Conflict Nodes')
plt.ylabel('Frequency')
plt.title('Conflict Resolutions Distribution')
plt.text(23, 45, r'$\mu=15, b=3$')
maxfreq = n.max()
# Set a clean upper y-axis limit.
plt.ylim(ymax=np.ceil(maxfreq / 10) * 10 if maxfreq % 10 else maxfreq + 10)          

plt.show()
'''

f, axarr = plt.subplots(2, 2)
axarr[0, 0].hist(x=conflict_nodes_count, bins='auto', color='#0504aa',
                            alpha=0.7, rwidth=0.85)
axarr[0, 0].set_title('Conflict Nodes')
axarr[0, 1].hist(x=terminal_costs, bins='auto', color='#0504aa',
                            alpha=0.7, rwidth=0.85)
axarr[0, 1].set_title('Terminal Costs')
axarr[1, 0].hist(x=transitional_cost, bins='auto', color='#0504aa',
                            alpha=0.7, rwidth=0.85) 
axarr[1, 0].set_title('Altitude Deviations')
axarr[1, 1].hist(x=number_of_intruders, bins='auto', color='#0504aa',
                            alpha=0.7, rwidth=0.85) 
axarr[1, 1].set_title('Number of Intruders')
for ax in axarr.flat:
    ax.set(xlabel='', ylabel='Frequency')
# Hide x labels and tick labels for top plots and y ticks for right plots.

plt.show()
